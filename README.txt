Open Math Info

* Plan
** Description
   OpenMath provides secondary math education with a means of teaching
   their students to learn computer science principles alongside
   mathematics.
*** Description of the application domain:
    The application domain is any school district, domestic and
    international. We hope to penetrate the international by
    making all documentation multilingual and editiable by the
    community.
*** Usefulness
    Value for the user is created through the students who learn
    from the product. The students will more easily learn basic
    computer science principles and also learn to use tools
    related to the sciencees and engieering. All of this is
    wrapped within the hand-in system, so that teachers can easily
    stay organized and measure student's performance. This will
    also expose math teachers to implementing LaTex in there
    everyday curriculum.
***  Realness.
     Our data includes problems and lessons. While alot of it will
     be created by us, we plan to aggregate video's from sites like
     khan acadamy and such. We also plan on later to work on user
     generated problems and lessons.
*** Description of the functionality that you plan to offer.  
    We plan to store grade information and how well students
    do on various problems. For the demo we will demonstrate tools
    for analyzing this data. Two spefic tools are:
    View which problems the student is doing poorly on
    from the teacher perspective
    As a student based on the problem you do poorly on
    generate suggested lecture readings and videos

** DONE Milestone 1
   DEADLINE: <2012-03-15 Thu> CLOSED: [2012-03-15 Thu 20:34]
*** DONE Courses: Eric
    CLOSED: [2012-04-15 Sun 20:27]
    - Design basic problem page
    - Design "homework" modules
*** DONE Sage file upload: Marcell
    CLOSED: [2012-04-15 Sun 20:27]
    - Interface with sage
    - Scaffolding for file upload and grading
*** DONE Seed data scripts: Sean
    CLOSED: [2012-04-15 Sun 20:27]
*** DONE Site Map:
    CLOSED: [2012-03-12 Mon 00:22]
*** DONE Basic user interface implementation
    CLOSED: [2012-04-15 Sun 20:27]
** TODO Milestone 2
   DEADLINE: <2012-04-18 Wed>
*** Lesson problem linking
**** TODO Connect to videos on the internet
**** DONE Connect to wikpedia articles on elementary mathematics
     CLOSED: [2012-04-18 Wed 00:17]
*** Grading system
**** DONE Set up dir structure
     CLOSED: [2012-04-18 Wed 00:17]
**** DONE Autograder
**** DONE Script evaluation
**** DONE Script evalution in Django
     CLOSED: [2012-04-18 Wed 00:17]
**** DONE User grade linking
     CLOSED: [2012-04-18 Wed 00:17]
*** Teacher problem development interface    
**** TODO Give to teachers and other to begin developing modules (data aggregation begins)
** Milestone 3
*** DONE "handin" system integrated with grading
    CLOSED: [2012-04-18 Wed 18:19]
*** TODO Problem administration
*** TODO Build a Course page
**** Uses Course parser - give category
**** Autoload Courses into database
**** Manually input videos in fields
** Milestone 4
   - Extra features begin
   - Graph fun
   - Begin implementing "level up system"
   - user portal upgraded
   - implement "quizzes"
** Database design
*** Login information
**** user: admin
**** pass: omath
*** DONE Schema
    CLOSED: [2012-04-15 Sun 20:32]
*** DONE E/R 
    CLOSED: [2012-04-15 Sun 20:32]
