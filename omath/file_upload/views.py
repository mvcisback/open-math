from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from omath.file_upload.forms import UploadFileForm
from omath.course.models import Course
from django.template import RequestContext

def handle_uploaded_file(f,title,delete,request):
    a = False
    courses = Course.objects.filter(name=title)
    if f.name.endswith('.html'):
        s = "templates/"
    elif f.name.endswith('.py') or f.name.endswith('.sage'):
        print request.user.username
        s = "users/" + request.user.username + "/"
    if courses:
        a=courses[0]
        if delete:
            a.delete()
            return
        else:
            a.name = title
            a.path = f.name
    else:
        a = Course(name=title, path=f.name)
    if a:
        a.save()
        destination = open(s+f.name, 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
            destination.close()

def upload_file(request):
    c = {}
    if request.path == "/upload_sucess/":
        html = "upload_sucess.html"
    else:
        html = "upload.html"
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            handle_uploaded_file(request.FILES['file'],
                                 cd['title'], cd['delete'], request)
            return HttpResponseRedirect('/upload_sucess/')
    else:
        form = UploadFileForm()
    c['form'] = form
    c['flag'] = True
    c.update(csrf(request))
    return render_to_response(html, c, context_instance=RequestContext(request))
