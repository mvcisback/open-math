BEGIN;
CREATE TABLE "course_course" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "path" varchar(300) NOT NULL
)
;
CREATE TABLE "course_chapter" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "course_id" integer NOT NULL REFERENCES "course_course" ("id")
)
;
CREATE TABLE "course_lesson" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "video_url" varchar(200) NOT NULL,
    "content" text NOT NULL,
    "chapter_id" integer NOT NULL REFERENCES "course_chapter" ("id")
)
;
CREATE TABLE "course_problem" (
    "id" integer NOT NULL PRIMARY KEY,
    "question" varchar(400) NOT NULL,
    "answer" varchar(100) NOT NULL,
    "chapter_id" integer NOT NULL REFERENCES "course_chapter" ("id")
)
;
CREATE INDEX "course_chapter_b7271b" ON "course_chapter" ("course_id");
CREATE INDEX "course_lesson_6c8b9f8c" ON "course_lesson" ("chapter_id");
CREATE INDEX "course_problem_6c8b9f8c" ON "course_problem" ("chapter_id");
COMMIT;
