# Create your views here.
import os
from django.shortcuts import render_to_response
from omath.course.models import Course, Chapter, Lesson, Problem

"""
Wrapper to add course
"""
def load_all(request):
    get_into_omath()
    os.chdir('lessons')
    for d in os.listdir('.'):
        if not os.path.isdir(d):
            continue
        add_course(d)
    os.chdir('..')
    return render_to_response("README.html")

def get_course(name):
    # Get Course
    course = Course.objects.filter(name=name)
    if course:
        course = course[0]
    else:
        #create new course
        course = Course(name=name)
        course.save()
    return course

def get_into_omath():
    os.chdir('/home/marcell/workspace/school/cs411/open-math/omath')
    return

"""
loops through dirs and runs f on each dir
"""
def objectize_dirs(f,obj):
    # loop through folders in course, aka chapters
    for d in os.listdir("."):
        if not os.path.isdir(d):
            continue
        f(d,obj)
    return

"""
Addes course based on dirs
"""
def add_course(name):
    course = get_course(name)
    # Just hard coded atm
    path = os.getcwd()
    os.chdir(path+'/'+name + '/')
    objectize_dirs(add_chapter,course)
    # clean up dir
    os.chdir('..')

def get_chap(chap_name,course):
    chap = Chapter.objects.filter(name=chap_name)
    if chap:
        chap = chap[0]
    else:
        chap = Chapter(name=chap_name, course=course)
        chap.save()
    return chap

"""
Addes a chapter based off folders in the dir
"""
def add_chapter(chap_name, course):
    chap = get_chap(chap_name, course)
    os.chdir(chap_name)
    #loop through each html file, aka lesson
    for d in os.listdir('.'):
        if not d.endswith('.html'):
            continue
        add_lesson(d,chap,course)
    os.chdir('..')


import unicodedata
"""
Adds a lesson based off of html extension
"""
def add_lesson(lesson, chap, course):
    name = lesson.replace('.html','')
    l = Lesson.objects.filter(name=name, chapter=chap)
    if l:
        return
    l = Lesson(name = name,chapter =chap)
    l.save()

