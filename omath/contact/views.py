from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from omath.contact.forms import ContactForm
from django.template import RequestContext
from django.http import HttpResponseRedirect

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            return HttpResponseRedirect('/')
    else:
        form = ContactForm()
    c = {'form': form}
    c.update(csrf(request))
    return render_to_response('contact_form.html',c, context_instance=RequestContext(request))
