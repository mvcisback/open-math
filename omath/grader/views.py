from django.shortcuts import render_to_response
from omath.course.models import Course, Chapter, Lesson, Problem
from omath.grader.autograder2 import grader
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

import os

def view_problem(request):
    html = 'problem2.html'
    
    return render_to_response(html)

def eval_grades(request):
    if 'q' in request.GET:
        q = request.GET['q']
    else:
        q = 1
    path = os.getcwd()
    if path.endswith('users'):
        main_dir = '.'
    else:
        main_dir = 'users'
    objs = User.objects.all()
    users = []
    for user in objs:
        users.append(user.username)
    prefix = 'prob_'
    print q
    problem = Problem.objects.filter(id=q)
    if not problem:
        return HttpResponseRedirect('/profile/')
    
    problem_id = problem[0].id
    s = prefix + str(problem_id)
    impl_files = [s+'.py']
    test_script = 'python -B ' + s + '_test.py'
    out = s+'.out'
    print 'grading'
    grader(implFiles = impl_files, mainDir = main_dir, 
        testScript = test_script, solution = out, users = users, forceRegrade=1, problem_id=problem_id)
    return HttpResponseRedirect('/profile/')




