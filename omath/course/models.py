from django.db import models
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from django.utils.encoding import force_unicode
fs = FileSystemStorage(location='/media/lessons')

#
# Course
#   Chapter
#     Lesson
#     Problem
#
#

class Course(models.Model):
    name = models.CharField(max_length=100)
    path = models.CharField(max_length=300)
#this unicode function is for the default display of the object when you call .objects.all()
#like trying to print some object in java
    def __unicode__(self):
        return self.name

class Chapter(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Course)
    def __unicode__(self):
        return self.name

class Lesson(models.Model):
    name = models.CharField(max_length=400)
    video_url = models.URLField(verify_exists=True, blank=True)
    f = models.FileField(upload_to='/media/lessons', blank=True, storage=fs)
    chapter = models.ForeignKey(Chapter)
    def __unicode__(self):
        return force_unicode(self.name)
    
class Problem(models.Model):
    question = models.CharField(max_length=400)
    answer = models.CharField(max_length=100)
    chapter = models.ForeignKey(Chapter)
    def __unicode__(self):
        return self.question

class ProblemInstance(models.Model):
    user = models.ForeignKey(User)
    problem = models.ForeignKey(Problem)
    score = models.FloatField()
