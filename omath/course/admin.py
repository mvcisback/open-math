from course.models import Course, Chapter, Lesson, Problem
from django.contrib import admin

#these classes are used for option in the admin
#    fields - for the changing order of the fields in the form
#    list_display - change what elements show in the listing
class ChapterAdmin(admin.ModelAdmin):
    fields = ['name', 'course']
    list_display = ['name', 'course']

class LessonAdmin(admin.ModelAdmin):
#    fields = ['name', 'chapter', 'video_url']
    list_display = ['name', 'chapter', 'video_url']
    

class ProblemAdmin(admin.ModelAdmin):
#    fields = ['question', 'answer', 'chapter']
    list_display = ['question', 'answer', 'chapter']

admin.site.register(Course)
admin.site.register(Chapter, ChapterAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Problem, ProblemAdmin)

