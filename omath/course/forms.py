from django import forms
from omath.course.models import Chapter

class AddCourseForm(forms.Form):
    name = forms.CharField(max_length=50)

class DeleteCourseForm(forms.Form):
    id = forms.IntegerField()

class UpdateCourseForm(forms.Form):
    name = forms.CharField(max_length=50)

class ProblemSubmission(forms.Form):
	input = forms.CharField(label='', required=False, widget=forms.Textarea(attrs={'class' : 'span7'}))
	file = forms.FileField(required=False)

class ChangeProblem(forms.Form):
	question = forms.CharField(max_length=400, required=False, widget=forms.Textarea(attrs={'class' : 'span7'}))
	answer = forms.CharField(max_length=50, required=False)
	chapter = forms.ModelChoiceField(Chapter.objects)
