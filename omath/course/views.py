# Create your views here.
from django.shortcuts import render_to_response
from omath.course.models import Course, Chapter, Lesson, Problem
from omath.course.forms import ProblemSubmission
from omath.file_upload.forms import UploadFileForm
from django.template import RequestContext
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from omath.course.models import *
from omath.course.forms import ChangeProblem
import os

def get_courses(request):
    user = request.user
    courses = Course.objects.all()
    return render_to_response('dashboard.html', {'courses':courses}, context_instance=RequestContext(request))

def get_chapters(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:   
            errors.append('No chapters available for this course.')
        else:
            chapters = Chapter.objects.filter(course=q)
            return render_to_response('course.html',
                                      {'chapters':chapters, 'course':q},context_instance=RequestContext(request))
    return render_to_response('course.html', {'chapters':chapters, 'errors': errors}, context_instance=RequestContext(request))

def get_lessons(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:   
            errors.append('No chapters available for this course.')
        else:
            lessons = Lesson.objects.filter(chapter=q)
            problems = Problem.objects.filter(chapter=q)
            c = {'lessons':lessons, 'problems':problems, 'chapter':q}
            return render_to_response('chapter.html', c, context_instance=RequestContext(request) )
    return render_to_response('chapter.html', {'lessons':lessons, 'problems':problems, 'errors': errors}, context_instance=RequestContext(request))

def show_lesson(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:   
            errors.append('The lesson is broken or deleted.')
        else:
            lesson = Lesson.objects.get(id__exact=q)
            chapter = lesson.chapter
            course = chapter.course
            path = course.name + '/' + chapter.name + '/' + lesson.name + '.html'
            print path
            return render_to_response('lesson.html',
                                      {'lesson':lesson, 'path':path}, context_instance=RequestContext(request))
    return render_to_response('lesson.html', {'errors': errors}, context_instance=RequestContext(request))   

def handle_uploaded_file(f,input, request, p):
    if os.getcwd().endswith('users'):
        print os.getcwd()
        s = ''
    else:
        s = 'users/'
    if f:
        if not (f.name.endswith('.py') or f.name.endswith('.sage')):
            return
        s += request.user.username + "/"
        destination = open(s+str(p.id) + '.py', 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
            destination.close()
    elif input:
        s +=  request.user.username + '/'
        print os.getcwd()
        f = open(s+'prob_'+str(p.id)+'.py','w')
        input = ('def test(x):\n' + input).replace('\n','\n   ')
        f.write(input)
        f.close()
    # Create Problem Instance
        
def show_problem(request):
    errors = []
    c = {}
    q = request.GET['q']
    p = Problem.objects.get(id=q)
    user = request.user 
    if user in User.objects.all():
        prob_inst = ProblemInstance.objects.filter(user = user, problem = p)
        # if no such prob_inst exists make one
        if not prob_inst:
            a = ProblemInstance(user=user,problem=p,score=0)
            a.save()
            print 'made problem inst'
    
    if request.method == 'POST':
        form = ProblemSubmission(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            
            if cd['file']:
                handle_uploaded_file(request.FILES['file'],
                                     cd['input'], request,p)
            else:
                handle_uploaded_file(None,cd['input'], request,p)
            return HttpResponseRedirect('/grade/?q='+q)

    elif 'q' in request.GET:
        q = request.GET['q']
        if not q:   
            errors.append('The problem failed to load or has been deleted.')
        elif request.user.is_authenticated():
            problem = Problem.objects.get(id__exact=q)
            lessons = Lesson.objects.filter(chapter=problem.chapter)
            form =ProblemSubmission(request.POST)
            c = {'problem':problem, 'lessons':lessons, 'form':form}
            return render_to_response('problem.html', c, context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect('/login')
    return render_to_response('problem.html', {}, context_instance=RequestContext(request))   

def course_search(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Please enter search terms')
        else:
            courses = Course.objects.filter(name__contains=q)
            lessons = Lesson.objects.filter(name__contains=q)
            chapters = Chapter.objects.filter(name__contains=q)
            return render_to_response('search_results.html',
                                      {'courses':courses, 'query':q, 'lessons':lessons, 'chapters':chapters}, context_instance=RequestContext(request))
    return render_to_response('search_form.html', {'errors': errors}, context_instance=RequestContext(request))
    
    
def show_profile(request):
    user = request.user
    if not user.is_authenticated():
        return HttpResponseRedirect("/login")
    else:
        probs = ProblemInstance.objects.filter(user=user)
        c = {'probs':probs}
        return render_to_response('profile.html', c, context_instance=RequestContext(request))

def add_problem(request):
    if request.method == 'POST':
        formset = ChangeProblem(request.POST, request.FILES)
        if formset.is_valid():
            cd = formset.cleaned_data
            selected = handle_problem(request, None , cd['question'], cd['answer'], cd['chapter'])
            return HttpResponseRedirect('/problem/?q='+str(selected.id))
    else:
        formset = ChangeProblem()
    return render_to_response("manage_problem.html", {"formset": formset}, context_instance=RequestContext(request))

def handle_problem(request, q ,question, answer, chapter):
    selected = Problem.objects.filter(id__exact=q)
    if not selected:
        selected = Problem(question=question, answer=answer, chapter=chapter)
        selected.save()
        print 'new...'
    else:
        selected = selected[0]
        if question:
            selected.question = question
        if answer:
            selected.answer = answer
        if chapter:
            selected.chapter = chapter
        selected.save()
        print 'updating...'
    return selected

def remove_problem(request):
    print "remove"
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            return HttpResponseRedirect('/dashboard/')
        delete = Problem.objects.filter(id__exact=q)
        if delete:
            delete.delete()
    return HttpResponseRedirect('/dashboard/')

def update_problem(request):
    if 'q' not in request.GET:
        return HttpResponseRedirect('/dashboard/')
    q = request.GET['q']
    if request.method == 'POST':
        formset = ChangeProblem(request.POST, request.FILES)
        if formset.is_valid():
            cd = formset.cleaned_data
            selected = handle_problem(request, q , cd['question'], cd['answer'], cd['chapter'])
            return HttpResponseRedirect('/problem/?q='+str(selected.id))
    else:
        formset = ChangeProblem()
    return render_to_response("manage_problem.html", {"formset": formset}, context_instance=RequestContext(request))
