from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from django.conf import settings
from omath.course.models import *


class Command(NoArgsCommand):
	def handle_noargs(self, **options):
	
		#Insert Courses
		course_names = ["Precalculus", "Calculus I", "Calculus II"] #Names of courses
		courses = [] #Course objects will eventually be held here
		for course_name in reversed(course_names):
			try:
				test = Course.objects.get(name=course_name) #See if course already exists
				courses.append(test) #Add it to the list of courses
			except Course.DoesNotExist: #If it doesn't exist, make it, add it to the list of courses and then save it in the DB
				c = Course(name = course_name)
				courses.append(c)
				c.save()
		courses.reverse() #Sent through the for loop in reverse for neat order in Admin
		print courses
		
		#Initialize Calc II Chapters
		calc_two_chapter_names = ["Integration Techniques", "Parametric Equations", "Polar Coordinates", "Sequences and Series", "Vectors", "Three Dimensional Space"]
		calc_two_chapters = []
		for chap_name in reversed(calc_two_chapter_names):
			try:
				test = Chapter.objects.get(name = chap_name, course = courses[2])
				calc_two_chapters.append(test)
			except Chapter.DoesNotExist:
				c = Chapter(name = chap_name, course = courses[2])
				calc_two_chapters.append(c)
				c.save()
		calc_two_chapters.reverse()
		print calc_two_chapters
		
		#Initialize Calc I Chapters
		calc_one_chapter_names = ["Limits", "Derivatives", "Applications of Derivatives", "Integrals", "Applications of Integrals"]
		calc_one_chapters = []
		for chap_name in reversed(calc_one_chapter_names):
			try:
				test = Chapter.objects.get(name = chap_name, course = courses[1])
				calc_one_chapters.append(test)
			except Chapter.DoesNotExist:
				c = Chapter(name = chap_name, course = courses[1])
				calc_one_chapters.append(c)
				c.save()
		calc_one_chapters.reverse()
		print calc_one_chapters
		
		#Initialize Precalc Chapters
		precalc_chapter_names = ["Functions", "Polynomial and Rational Functions", "Exponential and Log Functions", "Trigonometry", "Systems and Inequalities", "Matrices and Determinants", "Discrete Math", "Conics and Polar Coordinates"]
		precalc_chapters = []
		for chap_name in reversed(precalc_chapter_names):
			try:
				test = Chapter.objects.get(name = chap_name, course = courses[0])
				precalc_chapters.append(test)
			except Chapter.DoesNotExist:
				c = Chapter(name = chap_name, course = courses[0])
				precalc_chapters.append(c)
				c.save()
		precalc_chapters.reverse()
		print precalc_chapters
		
	
		
		