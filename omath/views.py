from django.shortcuts import render_to_response
from omath.grader.autograder2 import *
from django.template import RequestContext
from omath.course.models import *
import os

def load_dash_item(request,template='error.html'):
    c = {'user':request.user}
    return render_to_response(template, c, context_instance=RequestContext(request))

def test_autograder(request):
    path = os.getcwd()
    print path
    if path.endswith('users'):
        main_dir = '.'
    else:
        main_dir = 'users'
    impl_files = ['1.py']
    test_script = 'python2 -B 1_test.py'
    template = 'README.html'
    users = ['marcell8','marcell10']
    grader(implFiles = impl_files, mainDir = main_dir,
           testScript = test_script, solution = '1.out',
           users = users, forceRegrade=1)
    return render_to_response(template, context_instance=RequestContext(request))

def avg_score(request):
    problems = Problem.objects.all()
    avg_list = []
    for problem in problems:
        instances = ProblemInstance.objects.filter(problem=problem)
        count = 0
        for inst in instances:
            count += inst.score
        total = len(instances)
        avg = count/float(total)
        avg_list.append((problem.id,avg))
    print avg_list
    
    return render_to_response('avg.html', {'avgs':avg_list}, context_instance=RequestContext(request))
