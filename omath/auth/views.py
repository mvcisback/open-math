from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.forms.models import modelformset_factory
from omath.auth.forms import *

import os

def login_user(request):
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "You're successfully logged in!"
                return HttpResponseRedirect("/dashboard")
            else:
                state = "Your account is not active, please contact the site admin."
        else:
            state = "Your username and/or password were incorrect."
    c = {'state':state, 'username': username}
    return render_to_response('auth.html', c, context_instance=RequestContext(request))

def logout_user(request):
    logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/")

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            os.chdir('users')
            os.mkdir(username)
            f = open(username+'/__init__.py','w')
            os.chdir('..')
            # make the code in the directory includable for test scripts
            f.close()
            new_user = form.save()
            return HttpResponseRedirect("/login/")
    else:
        form = UserCreationForm()
    c = {'form': form}
    return render_to_response("registration/register.html", c, context_instance=RequestContext(request))


def user_settings(request):
    if request.method == 'POST':
        formset = ChangeUserForm(request.POST, request.FILES)
        if formset.is_valid():
            cd = formset.cleaned_data
            handle_user_settings(request, cd['delete'], cd['firstname'], cd['lastname'], cd['email'])
            return HttpResponseRedirect('/profile/')
            # do something.
    else:
        formset = ChangeUserForm()
    return render_to_response("settings.html", {"formset": formset}, context_instance=RequestContext(request))

def handle_user_settings(request, delete, fname, lname, email):
    selected = User.objects.filter(username=request.user)
    if not selected:
        return HttpResponseRedirect('/')
    selected = selected[0]
    if delete == True:
        if selected:
            selected.delete()
            return HttpResponseRedirect('/home/')
    else:
        if fname:
            selected.first_name = fname
        if lname:
            selected.last_name = lname
        if email:
            selected.email = email
        selected.save()