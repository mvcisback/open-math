from django import forms

class ChangeUserForm(forms.Form):
    firstname = forms.CharField(max_length=50, required=False)
    lastname = forms.CharField(max_length=50, required=False)
    email = forms.EmailField(required=False)
    delete = forms.BooleanField(required=False)
