from django.conf.urls.defaults import patterns, include, url
from omath.file_upload.views import upload_file

from django.views.static import * 
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from omath.views import load_dash_item, avg_score 
from omath.contact.views import contact
from omath.course.views import *
from omath.grader.views import *
from omath.auth.views import user_settings
from omath.build_lesson.views import *
from django.contrib.auth.views import login, logout

admin.autodiscover()


# General urls
urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^upload/|^upload_sucess/$', upload_file),
                       url(r'^grade/$', eval_grades),
                       url(r'^search/$', course_search),
                       url(r'^contact/$', contact),
                       url(r'^login/$', 'auth.views.login_user'),
					   url(r'^logout/$', 'auth.views.logout_user'),
                       url(r'^register/$', 'auth.views.register'),
                       url(r'^accounts/login/$',  login),
                       url(r'^accounts/logout/$', logout),
                       url(r'^accounts/profile/$', load_dash_item, {'template':'dashboard.html'}),
                       url(r'^profile/$', show_profile),
                       url(r'^settings/$', user_settings), 
                       url(r'^add_problem/$', add_problem),
                       url(r'^remove_problem/$', remove_problem),
                       url(r'^update_problem/$', update_problem),                                              
                       # url(r'^add_all_lessons/$, )
                       # url(r'^test/$', test_wiki),


                       # Required to make static serving work 
                      (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)

# Dashboard urls
urlpatterns += patterns('',

                       url(r'^$|^home/$', load_dash_item,
                           {'template':'home.html'}),     # Root page
                       url(r'^about/$', load_dash_item,
                           {'template':'about.html'}),     # About
                       url(r'^test_wiki/$', load_dash_item,
                           {'template':'Elementary_Algebra/Cube root.html'}),
                       url(r'^load_lessons/$', load_all)
)


from omath.course.models import *
# Course Specific stuff
urlpatterns += patterns('',
                       url(r'^dashboard/$', get_courses),
                       url(r'^avg/$', avg_score),
                       url(r'^chapter/$', get_lessons),
                       url(r'^course/$', get_chapters),
                       url(r'^lesson/$', show_lesson),
                       url(r'^problem/$', show_problem),
                       #url(r'^$|^courses/$', load_course_elements,
                           #{'template': 'dashboard.html', 'models':Course}),

                       #url(r'^$|^chapters/$', load_course_elements,
                           #{'template': 'chapter.html', 'models':Chapter}),
                       #url(r'^$|^lessons/$', load_course_elements,
                           #{'template': 'lesson.html', 'models':Lesson}),
                       #url(r'^$|^problems/$', load_course_elements,
                           #{'template': 'problem.html', 'models':Problem}),
)

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
    (r'^media/(?P<path>.*)$', 
        'serve', {
        'document_root': '/media',
        'show_indexes': True }),)
