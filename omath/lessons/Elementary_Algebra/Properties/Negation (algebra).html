<p><b>Negation</b> is the <a href="/wiki/Operation_(mathematics)" title="Operation (mathematics)">mathematical operation</a> that reverses the <a href="/wiki/Sign_(mathematics)" title="Sign (mathematics)">sign</a> of a number. Thus the negation of a <a href="/wiki/Positive_number" title="Positive number" class="mw-redirect">positive number</a> is negative, and the negation of a <a href="/wiki/Negative_number" title="Negative number">negative number</a> is positive. The negation of <a href="/wiki/0_(number)" title="0 (number)">zero</a> is zero. The negation of a number <span class="texhtml"><i>a</i></span>, usually denoted <span class="texhtml"><i>a</i></span>, is often also called the <i>opposite</i> of <span class="texhtml"><i>a</i></span>.</p>
<p>More generally, negation may refer to any operation that replaces an object with its <a href="/wiki/Additive_inverse" title="Additive inverse">additive inverse</a>. For example, the negation of a <a href="/wiki/Euclidean_vector" title="Euclidean vector">vector</a> is another vector with the same magnitude and the exact opposite direction.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Relation_to_other_operations"><span class="tocnumber">1</span> <span class="toctext">Relation to other operations</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Double_negation"><span class="tocnumber">2</span> <span class="toctext">Double negation</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Additive_inverse"><span class="tocnumber">3</span> <span class="toctext">Additive inverse</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Other_properties"><span class="tocnumber">4</span> <span class="toctext">Other properties</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Generalizations"><span class="tocnumber">5</span> <span class="toctext">Generalizations</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#See_also"><span class="tocnumber">6</span> <span class="toctext">See also</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=1" title="Edit section: Relation to other operations"></a>]</span> <span class="mw-headline" id="Relation_to_other_operations">Relation to other operations</span></h2>
<p>Negation is closely related to <a href="/wiki/Subtraction" title="Subtraction">subtraction</a>, which can be viewed as a combination of <a href="/wiki/Addition" title="Addition">addition</a> and negation:</p>
<dl>
<dd><span class="texhtml"><i>a</i>  <i>b</i> = <i>a</i> + (<i>b</i>).</span></dd>
</dl>
<p>Similarly, negation can be thought of as subtraction from zero:</p>
<dl>
<dd><span class="texhtml"><i>a</i> = 0  <i>a</i>.</span></dd>
</dl>
<p>Negation can also be thought of as <a href="/wiki/Multiplication" title="Multiplication">multiplication</a> by negative one:</p>
<dl>
<dd><span class="texhtml">(1)  <i>a</i> = <i>a</i>.</span></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=2" title="Edit section: Double negation"></a>]</span> <span class="mw-headline" id="Double_negation">Double negation</span></h2>
<p>The negation of a <a href="/wiki/Negative_number" title="Negative number">negative number</a> is positive. For example,</p>
<dl>
<dd><span class="texhtml">(3) = +3.</span></dd>
</dl>
<p>In general, double negation has no net effect on a number. That is,</p>
<dl>
<dd><span class="texhtml">(<i>a</i>) = <i>a</i></span></dd>
</dl>
<p>for any <a href="/wiki/Real_number" title="Real number">real number</a> <i>a</i>.</p>
<p>This sometimes leads to somewhat confusing notation. For example, the <a href="/wiki/Absolute_value" title="Absolute value">absolute value</a> of a real number <span class="texhtml"><i>a</i></span> is usually defined by the formula:</p>
<dl>
<dd><img class="tex" alt="|a| = \begin{cases} a, &amp; \text{if }  a \ge 0  \\ -a,  &amp; \text{if } a &lt; 0. \end{cases} " src="//upload.wikimedia.org/wikipedia/en/math/6/f/b/6fb7b7ff1d00ea7dca6a42c5bfac2287.png" /></dd>
</dl>
<p>When <span class="texhtml"><i>a</i></span> is negative, the absolute value of <span class="texhtml"><i>a</i></span> is defined to be <span class="texhtml"><i>a</i></span>, which is always positive, being the negation of a negative number.</p>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=3" title="Edit section: Additive inverse"></a>]</span> <span class="mw-headline" id="Additive_inverse">Additive inverse</span></h2>
<p>The negation of a real number is an <a href="/wiki/Additive_inverse" title="Additive inverse">additive inverse</a> for that number. That is,</p>
<dl>
<dd><span class="texhtml"><i>a</i> + (<i>a</i>) = 0</span></dd>
</dl>
<p>for any real number <span class="texhtml"><i>a</i></span>. (Note that <span class="texhtml">0</span> is the <a href="/wiki/Additive_identity" title="Additive identity">additive identity</a>.)</p>
<p>In <a href="/wiki/Abstract_algebra" title="Abstract algebra">abstract algebra</a>, certain <a href="/wiki/Binary_operation" title="Binary operation">binary operations</a> in <a href="/wiki/Algebraic_structure" title="Algebraic structure">algebraic structures</a> are commonly written as <a href="/wiki/Addition" title="Addition">addition</a>. In this case, the <a href="/wiki/Identity_element" title="Identity element">identity element</a> with respect to the binary operation is usually thought of as <span class="texhtml">0</span>, and the <a href="/wiki/Inverse_element" title="Inverse element">inverse</a> of any element is thought of as its negation.</p>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=4" title="Edit section: Other properties"></a>]</span> <span class="mw-headline" id="Other_properties">Other properties</span></h2>
<p>In addition to the identities listed above, negation has the following algebraic properties:</p>
<dl>
<dd><span class="texhtml">(<i>a</i> + <i>b</i>) = (<i>a</i>) + (<i>b</i>)</span></dd>
<dd><span class="texhtml"><i>a</i>  (<i>b</i>) = <i>a</i> + <i>b</i></span></dd>
<dd><span class="texhtml">(<i>a</i>)  <i>b</i> = <i>a</i>  (<i>b</i>) = (<i>a</i>  <i>b</i>)</span></dd>
<dd><span class="texhtml">(<i>a</i>)  (<i>b</i>) = <i>a</i>  <i>b</i></span></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=5" title="Edit section: Generalizations"></a>]</span> <span class="mw-headline" id="Generalizations">Generalizations</span></h2>
<p>Negation may also be defined for <a href="/wiki/Complex_number" title="Complex number">complex numbers</a> by the formula</p>
<dl>
<dd><span class="texhtml">(<i>a</i> + <i>bi</i>) = (<i>a</i>) + (<i>b</i>)<i>i</i></span></dd>
</dl>
<p>On the <a href="/wiki/Complex_plane" title="Complex plane">complex plane</a>, this operation <a href="/wiki/Rotation" title="Rotation">rotates</a> a complex number 180 <a href="/wiki/Degree_(angle)" title="Degree (angle)">degrees</a> around the <a href="/wiki/Origin_(mathematics)" title="Origin (mathematics)">origin</a>.</p>
<p>Similarly, the negation of a <a href="/wiki/Euclidean_vector" title="Euclidean vector">Euclidean vector</a> can be obtained by rotating the vector 180 degrees. Thus the negation of a vector has the same magnitude as the original, but the exact opposite direction. (Vectors in exactly opposite directions are sometimes referred to as <b>antiparallel</b>.) In terms of <a href="/wiki/Vector_components" title="Vector components" class="mw-redirect">vector components</a></p>
<dl>
<dd><span class="texhtml">(<i>x</i>, <i>y</i>, <i>z</i>) = (<i>x</i>, <i>y</i>, <i>z</i>)</span></dd>
</dl>
<p>or more generally</p>
<dl>
<dd><span class="texhtml">(<i>x</i><sub>1</sub>, ..., <i>x<sub>n</sub></i>) = (<i>x</i><sub>1</sub>, ..., <i>x<sub>n</sub></i>)</span></dd>
</dl>
<p>Negation of vectors has the same effect as <a href="/wiki/Scalar_multiplication" title="Scalar multiplication">scalar multiplication</a> by <span class="texhtml">1</span>.</p>
<p>In <a href="/wiki/Abstract_algebra" title="Abstract algebra">abstract algebra</a>, negation may refer to any operation that takes the <a href="/wiki/Additive_inverse" title="Additive inverse">additive inverse</a> of an element of an <a href="/wiki/Abelian_group" title="Abelian group">abelian group</a> (or any invertible element of an additive <a href="/wiki/Magma_(algebra)" title="Magma (algebra)">magma</a>).</p>
<h2><span class="section">[<a href="/w/index.php?title=Negation_(algebra)&amp;action=&amp;section=6" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Negative_and_non-negative_numbers" title="Negative and non-negative numbers" class="mw-redirect">Negative and non-negative numbers</a></li>
<li><a href="/wiki/Sign_(mathematics)" title="Sign (mathematics)">Sign (mathematics)</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 383/1000000
Post-expand include size: 1357/2048000 bytes
Template argument size: 623/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:29334824-0!0!0!!en!*!* and timestamp 20120301044519 -->
