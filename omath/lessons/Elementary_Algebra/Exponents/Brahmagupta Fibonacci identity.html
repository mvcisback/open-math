<p>In <a href="/wiki/Algebra" title="Algebra">algebra</a>, <b>Brahmagupta's identity</b>, also called <b>Fibonacci's identity</b>, implies that the product of two sums each of two squares is itself a sum of two squares. In other words, the set of all sums of two squares is <a href="/wiki/Closure_(mathematics)" title="Closure (mathematics)">closed</a> under multiplication. Specifically:</p>
<dl>
<dd><img class="tex" alt="\begin{align}
\left(a^2 + b^2\right)\left(c^2 + d^2\right) &amp; {}= \left(ac-bd\right)^2 + \left(ad+bc\right)^2 \  \qquad\qquad(1) \\
                                             &amp; {}= \left(ac+bd\right)^2 + \left(ad-bc\right)^2.\qquad\qquad(2)
\end{align}" src="//upload.wikimedia.org/wikipedia/en/math/7/6/e/76eb540033111b8ad04e73e5dd4b21d2.png" /></dd>
</dl>
<p>For example,</p>
<dl>
<dd><img class="tex" alt="(1^2 + 4^2)(2^2 + 7^2) = 26^2 + 15^2 = 30^2 + 1^2.\," src="//upload.wikimedia.org/wikipedia/en/math/9/e/c/9ec4898b9708b03ebace60a47df8d68c.png" /></dd>
</dl>
<p>The identity is a special case (<i>n</i>&#160;=&#160;1) of <a href="/wiki/Lagrange%27s_identity" title="Lagrange's identity">Lagrange's identity</a>, and is first found in Diophantus. Brahmagupta proved and used a more general identity, equivalent to</p>
<dl>
<dd><img class="tex" alt="\begin{align}
\left(a^2 + nb^2\right)\left(c^2 + nd^2\right) &amp; {}= \left(ac-nbd\right)^2 + n\left(ad+bc\right)^2 \  \qquad\qquad(3) \\
                                               &amp; {}= \left(ac+nbd\right)^2 + n\left(ad-bc\right)^2,\qquad\qquad(4)
\end{align}" src="//upload.wikimedia.org/wikipedia/en/math/1/3/c/13c979a526cd6f13b556b7dbc2d11a76.png" /></dd>
</dl>
<p>showing that the set of all numbers of the form <img class="tex" alt="x^2 + ny^2" src="//upload.wikimedia.org/wikipedia/en/math/1/b/2/1b2f9b45cb00f8442675547f2ce052b9.png" /> is closed under multiplication.</p>
<p>Both (1) and (2) can be verified by <a href="/wiki/Polynomial_expansion" title="Polynomial expansion">expanding</a> each side of the equation. Also, (2) can be obtained from (1), or (1) from (2), by changing <i>b</i> to&#160;<i>b</i>.</p>
<p>This identity holds in both the <a href="/wiki/Integer" title="Integer">ring of integers</a> and the <a href="/wiki/Rational_number" title="Rational number">ring of rational numbers</a>, and more generally in any <a href="/wiki/Commutative_ring" title="Commutative ring">commutative ring</a>.</p>
<p>In the <a href="/wiki/Integer" title="Integer">integer</a> case this identity finds applications in <a href="/wiki/Number_theory" title="Number theory">number theory</a> for example when used in conjunction with one of <a href="/wiki/Fermat%27s_theorem_on_sums_of_two_squares" title="Fermat's theorem on sums of two squares">Fermat's theorems</a> it proves that the product of a square and any number of primes of the form 4<i>n</i>&#160;+&#160;1 is also a sum of two squares.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#History"><span class="tocnumber">1</span> <span class="toctext">History</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Related_identities"><span class="tocnumber">2</span> <span class="toctext">Related identities</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Relation_to_complex_numbers"><span class="tocnumber">3</span> <span class="toctext">Relation to complex numbers</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Interpretation_via_norms"><span class="tocnumber">4</span> <span class="toctext">Interpretation via norms</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Application_to_Pell.27s_equation"><span class="tocnumber">5</span> <span class="toctext">Application to Pell's equation</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#See_also"><span class="tocnumber">6</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#References"><span class="tocnumber">7</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#External_links"><span class="tocnumber">8</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=1" title="Edit section: History"></a>]</span> <span class="mw-headline" id="History">History</span></h2>
<p>The identity is first found in <a href="/wiki/Diophantus" title="Diophantus">Diophantus</a>'s <i>Arithmetica</i> (III, 19). The identity was rediscovered by <a href="/wiki/Brahmagupta" title="Brahmagupta">Brahmagupta</a> (598668), an <a href="/wiki/Indian_mathematicians" title="Indian mathematicians" class="mw-redirect">Indian mathematician</a> and <a href="/wiki/Indian_astronomy" title="Indian astronomy">astronomer</a>, who generalized it and used it in his study of what is now erroneously called <a href="/wiki/Pell%27s_equation" title="Pell's equation">Pell's equation</a>. His <i><a href="/wiki/Brahmasphutasiddhanta" title="Brahmasphutasiddhanta" class="mw-redirect">Brahmasphutasiddhanta</a></i> was translated from <a href="/wiki/Sanskrit" title="Sanskrit">Sanskrit</a> into <a href="/wiki/Arabic_language" title="Arabic language">Arabic</a> by <a href="/wiki/Mohammad_al-Fazari" title="Mohammad al-Fazari" class="mw-redirect">Mohammad al-Fazari</a>, and was subsequently translated into <a href="/wiki/Latin" title="Latin">Latin</a> in 1126.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup> The identity later appeared in <a href="/wiki/Fibonacci" title="Fibonacci">Fibonacci</a>'s <i><a href="/wiki/The_Book_of_Squares" title="The Book of Squares">Book of Squares</a></i> in 1225.</p>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=2" title="Edit section: Related identities"></a>]</span> <span class="mw-headline" id="Related_identities">Related identities</span></h2>
<p><a href="/wiki/Euler%27s_four-square_identity" title="Euler's four-square identity">Euler's four-square identity</a> is an analogous identity involving four squares instead of two that is related to <a href="/wiki/Quaternions" title="Quaternions" class="mw-redirect">quaternions</a>. There is a similar <a href="/wiki/Degen%27s_eight-square_identity" title="Degen's eight-square identity">eight-square identity</a> derived from the <a href="/wiki/Octonions" title="Octonions" class="mw-redirect">Cayley numbers</a> which has connections to <a href="/wiki/Bott_periodicity" title="Bott periodicity" class="mw-redirect">Bott periodicity</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=3" title="Edit section: Relation to complex numbers"></a>]</span> <span class="mw-headline" id="Relation_to_complex_numbers">Relation to complex numbers</span></h2>
<p>If <i>a</i>, <i>b</i>, <i>c</i>, and <i>d</i> are <a href="/wiki/Real_number" title="Real number">real numbers</a>, this identity is equivalent to the multiplication property for absolute values of <a href="/wiki/Complex_numbers" title="Complex numbers" class="mw-redirect">complex numbers</a> namely that:</p>
<dl>
<dd><img class="tex" alt="  | a+bi |  | c+di | = | (a+bi)(c+di) | \," src="//upload.wikimedia.org/wikipedia/en/math/4/3/0/430633477c4eec47220921414c54987f.png" /></dd>
</dl>
<p>since</p>
<dl>
<dd><img class="tex" alt="  | a+bi |  | c+di | = | (ac-bd)+i(ad+bc) |,\," src="//upload.wikimedia.org/wikipedia/en/math/e/9/1/e91de8a4986df0ed367ac0dd5851c9a2.png" /></dd>
</dl>
<p>by squaring both sides</p>
<dl>
<dd><img class="tex" alt="  | a+bi |^2  | c+di |^2 = | (ac-bd)+i(ad+bc) |^2,\," src="//upload.wikimedia.org/wikipedia/en/math/f/9/6/f96da79fd0f53ab5f90678b7d8b0733c.png" /></dd>
</dl>
<p>and by the definition of absolute value,</p>
<dl>
<dd><img class="tex" alt="  (a^2+b^2)(c^2+d^2)= (ac-bd)^2+(ad+bc)^2. \," src="//upload.wikimedia.org/wikipedia/en/math/5/4/7/54780df03c91db81c354cd4f88f522b5.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=4" title="Edit section: Interpretation via norms"></a>]</span> <span class="mw-headline" id="Interpretation_via_norms">Interpretation via norms</span></h2>
<p>In the case that the variables <i>a</i>, <i>b</i>, <i>c</i>, and <i>d</i> are <a href="/wiki/Rational_number" title="Rational number">rational numbers</a>, the identity may be interpreted as the statement that the <a href="/wiki/Field_norm" title="Field norm">norm</a> in the <a href="/wiki/Field_(mathematics)" title="Field (mathematics)">field</a> <b>Q</b>(<i>i</i>) is <i>multiplicative</i>. That is, we have</p>
<dl>
<dd><img class="tex" alt="N(a+bi) = a^2 + b^2 \text{ and }N(c+di) = c^2 + d^2, \," src="//upload.wikimedia.org/wikipedia/en/math/a/8/1/a811bc7693e5bdf207f5bef3dc3d8cbd.png" /></dd>
</dl>
<p>and also</p>
<dl>
<dd><img class="tex" alt="N((a+bi)(c+di)) = N((ac-bd)+i(ad+bc)) = (ac-bd)^2 + (ad+bc)^2. \," src="//upload.wikimedia.org/wikipedia/en/math/1/d/a/1dac6cfc038f9114013d6e5aa6013024.png" /></dd>
</dl>
<p>Therefore the identity is saying that</p>
<dl>
<dd><img class="tex" alt="N((a+bi)(c+di)) = N(a+bi) \cdot N(c+di). \," src="//upload.wikimedia.org/wikipedia/en/math/0/c/3/0c31aa4bf2d8338c47a48a0fd2e1abc5.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=5" title="Edit section: Application to Pell's equation"></a>]</span> <span class="mw-headline" id="Application_to_Pell.27s_equation">Application to Pell's equation</span></h2>
<p>In its original context, Brahmagupta applied his discovery to the solution of <a href="/wiki/Pell%27s_equation" title="Pell's equation">Pell's equation</a>, namely <i>x</i><sup>2</sup>&#160;&#160;<i>Ny</i><sup>2</sup>&#160;=&#160;1. Using the identity in the more general form</p>
<dl>
<dd><img class="tex" alt="(x_1^2 - Ny_1^2)(x_2^2 - Ny_2^2) = (x_1x_2 + Ny_1y_2)^2 - N(x_1y_2 + x_2y_1)^2, \, " src="//upload.wikimedia.org/wikipedia/en/math/9/7/c/97cd219165ea487e4fdc37e270aa8f48.png" /></dd>
</dl>
<p>he was able to "compose" triples (<i>x</i><sub>1</sub>,&#160;<i>y</i><sub>1</sub>,&#160;<i>k</i><sub>1</sub>) and (<i>x</i><sub>2</sub>,&#160;<i>y</i><sub>2</sub>,&#160;<i>k</i><sub>2</sub>) that were solutions of <i>x</i><sup>2</sup>&#160;&#160;<i>Ny</i><sup>2</sup>&#160;=&#160;<i>k</i>, to generate the new triple</p>
<dl>
<dd><img class="tex" alt="(x_1x_2 + Ny_1y_2 \,,\, x_1y_2 + x_2y_1 \,,\, k_1k_2)." src="//upload.wikimedia.org/wikipedia/en/math/e/0/e/e0e3b48abec42cec967faffbaabccf47.png" /></dd>
</dl>
<p>Not only did this give a way to generate infinitely many solutions to <i>x</i><sup>2</sup>&#160;&#160;<i>Ny</i><sup>2</sup>&#160;=&#160;1 starting with one solution, but also, by dividing such a composition by <i>k</i><sub>1</sub><i>k</i><sub>2</sub>, integer or "nearly integer" solutions could often be obtained. The general method for solving the Pell equation given by <a href="/wiki/Bhaskara_II" title="Bhaskara II" class="mw-redirect">Bhaskara II</a> in 1150, namely the <a href="/wiki/Chakravala_method" title="Chakravala method">chakravala (cyclic) method</a>, was also based on this identity.<sup id="cite_ref-stillwell_1-0" class="reference"><a href="#cite_note-stillwell-1"><span>[</span>2<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=6" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Brahmagupta_matrix" title="Brahmagupta matrix">Brahmagupta matrix</a></li>
<li><a href="/wiki/Indian_mathematics" title="Indian mathematics">Indian mathematics</a></li>
<li><a href="/wiki/List_of_Indian_mathematicians" title="List of Indian mathematicians">List of Indian mathematicians</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=7" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">George G. Joseph (2000). <i>The Crest of the Peacock</i>, p. 306. <a href="/wiki/Princeton_University_Press" title="Princeton University Press">Princeton University Press</a>. <a href="/wiki/Special:BookSources/0691006598" class="internal mw-magiclink-isbn">ISBN 0691006598</a>.</span></li>
<li id="cite_note-stillwell-1"><b><a href="#cite_ref-stillwell_1-0">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFJohn_Stillwell2002"><a href="/wiki/John_Stillwell" title="John Stillwell">John Stillwell</a> (2002), <a rel="nofollow" class="external text" href="http://books.google.com/books?id=WNjRrqTm62QC&amp;pg=PA72"><i>Mathematics and its history</i></a> (2 ed.), Springer, pp.&#160;7276, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9780387953366" title="Special:BookSources/9780387953366">9780387953366</a><span class="printonly">, <a rel="nofollow" class="external free" href="http://books.google.com/books?id=WNjRrqTm62QC&amp;pg=PA72">http://books.google.com/books?id=WNjRrqTm62QC&amp;pg=PA72</a></span></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Mathematics+and+its+history&amp;rft.aulast=%5B%5BJohn+Stillwell%5D%5D&amp;rft.au=%5B%5BJohn+Stillwell%5D%5D&amp;rft.date=2002&amp;rft.pages=pp.%26nbsp%3B72%E2%80%9376&amp;rft.ion=2&amp;rft.pub=Springer&amp;rft.isbn=9780387953366&amp;rft_id=http%3A%2F%2Fbooks.google.com%2Fbooks%3Fid%3DWNjRrqTm62QC%26pg%3DPA72&amp;rfr_id=info:sid/en.wikipedia.org:Brahmagupta%E2%80%93Fibonacci_identity"><span style="display: none;">&#160;</span></span></span></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Brahmagupta%E2%80%93Fibonacci_identity&amp;action=&amp;section=8" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://planetmath.org/encyclopedia/BrahmaguptasIdentity.html">Brahmagupta's identity at</a> <a href="/wiki/PlanetMath" title="PlanetMath">PlanetMath</a></li>
<li><a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/BrahmaguptaIdentity.html">Brahmagupta Identity</a> on <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a></li>
<li><a rel="nofollow" class="external text" href="http://www.pballew.net/fiboiden.html">Brahmagupta-Fibonacci identity</a></li>
<li><a rel="nofollow" class="external text" href="http://sites.google.com/site/tpiezas/005b/">A Collection of Algebraic Identities</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 947/1000000
Post-expand include size: 5784/2048000 bytes
Template argument size: 1309/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:326483-0!0!0!!en!*!* and timestamp 20120313121330 -->
