<div class="dablink">For other uses, see <a href="/wiki/Rationalization_(disambiguation)" title="Rationalization (disambiguation)" class="mw-redirect">Rationalization</a>.</div>
<p>In <a href="/wiki/Elementary_algebra" title="Elementary algebra">elementary algebra</a>, <b>root rationalisation</b> is a process by which <a href="/wiki/Nth_root" title="Nth root">surds</a> in the <a href="/wiki/Denominator" title="Denominator" class="mw-redirect">denominator</a> of an <a href="/wiki/Fraction_(mathematics)#Irrational_fractions.2C_continued_fractions.2C_and_partial_fractions" title="Fraction (mathematics)">irrational fraction</a> are eliminated.</p>
<p>These surds may be <a href="/wiki/Monomial" title="Monomial">monomials</a> or <a href="/wiki/Binomial" title="Binomial">binomials</a> involving <a href="/wiki/Square_root" title="Square root">square roots</a>, in simple examples. There are wide extensions to the technique.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Rationalisation_of_a_monomial_square_root_and_cube_root"><span class="tocnumber">1</span> <span class="toctext">Rationalisation of a monomial square root and cube root</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Dealing_with_more_square_roots"><span class="tocnumber">2</span> <span class="toctext">Dealing with more square roots</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Generalisations"><span class="tocnumber">3</span> <span class="toctext">Generalisations</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#See_also"><span class="tocnumber">4</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Rationalisation_(mathematics)&amp;action=&amp;section=1" title="Edit section: Rationalisation of a monomial square root and cube root"></a>]</span> <span class="mw-headline" id="Rationalisation_of_a_monomial_square_root_and_cube_root">Rationalisation of a monomial square root and cube root</span></h2>
<p>For the fundamental technique, the numerator and denominator must be multiplied, but by the same factor.</p>
<p>Example 1:</p>
<dl>
<dd><img class="tex" alt="\frac{10}{\sqrt{a}}" src="//upload.wikimedia.org/wikipedia/en/math/c/d/5/cd5788137812b0e6d5786dee4929c109.png" /></dd>
</dl>
<p>To rationalise this kind of <a href="/wiki/Monomial" title="Monomial">monomial</a>, bring in the factor <img class="tex" alt="\sqrt{a}" src="//upload.wikimedia.org/wikipedia/en/math/9/a/a/9aa82dcf5bbe75152b9a8c478a0c861f.png" />:</p>
<dl>
<dd><img class="tex" alt="\frac{10}{\sqrt{a}} = \frac{10}{\sqrt{a}} \cdot \frac{\sqrt{a}}{\sqrt{a}} = \frac{{10\sqrt{a}}}{\sqrt{a}^2}" src="//upload.wikimedia.org/wikipedia/en/math/e/6/a/e6a74e2af46869669d9dfb0c683ba6f5.png" /></dd>
</dl>
<p>The <a href="/wiki/Square_root" title="Square root">square root</a> disappears from the denominator, because it is squared:</p>
<dl>
<dd><img class="tex" alt="\frac{{10\sqrt{a}}}{\sqrt{a}^2} = \frac{10\sqrt{a}}{a}" src="//upload.wikimedia.org/wikipedia/en/math/7/e/6/7e6d9474faee0261d02aa8a886984592.png" /></dd>
</dl>
<p>This gives the result, after simplification:</p>
<dl>
<dd><img class="tex" alt="\frac{{10\sqrt{a}}}{{a}}" src="//upload.wikimedia.org/wikipedia/en/math/b/9/6/b9678735f8f89f9a8285f0cb9b08fea4.png" /></dd>
</dl>
<p><br /></p>
<p>Example 2:</p>
<dl>
<dd><img class="tex" alt="\frac{10}{\sqrt[3]{b}}" src="//upload.wikimedia.org/wikipedia/en/math/6/0/d/60dfcf88a63c2f69097599feb186840e.png" /></dd>
</dl>
<p>To rationalise this radical, bring in the factor <img class="tex" alt="\sqrt[3]{b}^2" src="//upload.wikimedia.org/wikipedia/en/math/7/3/7/737333ddc307bd148c79a3d5075f17bf.png" />:</p>
<dl>
<dd><img class="tex" alt="\frac{10}{\sqrt[3]{b}} = \frac{10}{\sqrt[3]{b}} \cdot \frac{\sqrt[3]{b}^2}{\sqrt[3]{b}^2} = \frac{{10\sqrt[3]{b}^2}}{\sqrt[3]{b}^3}" src="//upload.wikimedia.org/wikipedia/en/math/6/c/9/6c9e29b1ff5b5bf27450fe52d5ed9021.png" /></dd>
</dl>
<p>The cube root disappears from the denominator, because it is cubed:</p>
<dl>
<dd><img class="tex" alt="\frac{{10\sqrt[3]{b}^2}}{\sqrt[3]{b}^3} = \frac{10\sqrt[3]{b}^2}{b}" src="//upload.wikimedia.org/wikipedia/en/math/d/f/6/df66b328a14781ef4552e78017568803.png" /></dd>
</dl>
<p>This gives the result, after simplification:</p>
<dl>
<dd><img class="tex" alt="\frac{{10\sqrt[3]{b}^2}}{{b}}" src="//upload.wikimedia.org/wikipedia/en/math/7/b/e/7be7eacfc40f60bd6e68ef5775921be7.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Rationalisation_(mathematics)&amp;action=&amp;section=2" title="Edit section: Dealing with more square roots"></a>]</span> <span class="mw-headline" id="Dealing_with_more_square_roots">Dealing with more square roots</span></h2>
<p>For a denominator that is:</p>
<dl>
<dd><img class="tex" alt="\sqrt{2}+\sqrt{3}\," src="//upload.wikimedia.org/wikipedia/en/math/2/6/d/26d2a03948342740852f3ee9bf969165.png" /></dd>
</dl>
<p>Rationalisation can be achieved by multiplying by the <i><a href="/wiki/Conjugate_(algebra)" title="Conjugate (algebra)">Conjugate</a></i>:</p>
<dl>
<dd><img class="tex" alt="\sqrt{2}-\sqrt{3}\," src="//upload.wikimedia.org/wikipedia/en/math/c/d/b/cdb09445555c83353afac79f43d18f98.png" /></dd>
</dl>
<p>and applying the <a href="/wiki/Difference_of_two_squares" title="Difference of two squares">difference of two squares</a> identity, which here will yield 1. To get this result, the entire fraction should be multiplied by</p>
<dl>
<dd><img class="tex" alt="\frac{ \sqrt{2}-\sqrt{3} }{\sqrt{2}-\sqrt{3}} = 1." src="//upload.wikimedia.org/wikipedia/en/math/8/5/1/8510c6aeffbc027483c3428a43cbb213.png" /></dd>
</dl>
<p>This technique works much more generally. It can easily be adapted to remove one square root at a time, i.e. to rationalise</p>
<dl>
<dd><img class="tex" alt="x +\sqrt{y}\," src="//upload.wikimedia.org/wikipedia/en/math/8/b/9/8b972577e0c97c46583797b15a5a601c.png" /></dd>
</dl>
<p>by multiplication by</p>
<dl>
<dd><img class="tex" alt="x -\sqrt{y}" src="//upload.wikimedia.org/wikipedia/en/math/f/2/d/f2d9efcd65a44277fd08468308342e7d.png" /></dd>
</dl>
<p>Example:</p>
<dl>
<dd><img class="tex" alt="\frac{3}{\sqrt{3}+\sqrt{5}}" src="//upload.wikimedia.org/wikipedia/en/math/8/c/3/8c3d664ee71aae38a85f92a8fd15c326.png" /></dd>
</dl>
<p>The fraction must be multiplied by a quotient containing <img class="tex" alt="{\sqrt{3}-\sqrt{5}}" src="//upload.wikimedia.org/wikipedia/en/math/7/4/4/74456d737bf6e141f41fd17b3b1b5cf7.png" />.</p>
<dl>
<dd><img class="tex" alt="\frac{3}{\sqrt{3}+\sqrt{5}} \cdot \frac{\sqrt{3}-\sqrt{5}}{\sqrt{3}-\sqrt{5}} = \frac{3(\sqrt{3}-\sqrt{5})}{\sqrt{3}^2 - \sqrt{5}^2}" src="//upload.wikimedia.org/wikipedia/en/math/6/4/b/64b0ff099709ea5c4d784ea7646d6d9c.png" /></dd>
</dl>
<p>Now, we can proceed to remove the square roots in the denominator:</p>
<dl>
<dd><img class="tex" alt="\frac{{3(\sqrt{3}-\sqrt{5}) }}{\sqrt{3}^2 - \sqrt{5}^2} = \frac{ 3 (\sqrt{3} - \sqrt{5} ) }{ 3 - 5 } = \frac{ 3( \sqrt{3}-\sqrt{5} )  }{-2}" src="//upload.wikimedia.org/wikipedia/en/math/8/7/8/878c4aa27d1382c58e431b2d0e51d825.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Rationalisation_(mathematics)&amp;action=&amp;section=3" title="Edit section: Generalisations"></a>]</span> <span class="mw-headline" id="Generalisations">Generalisations</span></h2>
<p>Rationalisation can be extended to all <a href="/wiki/Algebraic_number" title="Algebraic number">algebraic numbers</a> and <a href="/wiki/Algebraic_function" title="Algebraic function">algebraic functions</a> (as an application of <a href="/wiki/Norm_form" title="Norm form">norm forms</a>). For example, to rationalise a <a href="/wiki/Cube_root" title="Cube root">cube root</a>, two linear factors involving <a href="/wiki/Cube_roots_of_unity" title="Cube roots of unity" class="mw-redirect">cube roots of unity</a> should be used, or equivalently a quadratic factor.</p>
<h2><span class="section">[<a href="/w/index.php?title=Rationalisation_(mathematics)&amp;action=&amp;section=4" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Conjugate_(algebra)" title="Conjugate (algebra)">Conjugate (algebra)</a></li>
<li><a href="/wiki/Sum_of_two_squares" title="Sum of two squares" class="mw-redirect">Sum of two squares</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Rationalisation_(mathematics)&amp;action=&amp;section=5" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<p>This material is carried in classic algebra texts. For example:</p>
<ul>
<li><a href="/wiki/George_Chrystal" title="George Chrystal">George Chrystal</a>, <i>Introduction to Algebra: For the Use of Secondary Schools and Technical Colleges</i> is a nineteenth-century text, first ion 1889, in print (<a href="/wiki/Special:BookSources/1402159072" class="internal mw-magiclink-isbn">ISBN 1402159072</a>); a trinomial example with square roots is on p. 256, while a general theory of rationalising factors for surds is on pp. 189199.</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 229/1000000
Post-expand include size: 463/2048000 bytes
Template argument size: 218/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:16458571-0!0!0!!en!*!* and timestamp 20120301042747 -->
