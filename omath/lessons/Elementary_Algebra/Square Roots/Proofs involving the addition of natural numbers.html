<p><a href="/wiki/Mathematical_proof" title="Mathematical proof">Mathematical proofs</a> for <a href="/wiki/Addition" title="Addition">addition</a> of the <a href="/wiki/Natural_numbers" title="Natural numbers" class="mw-redirect">natural numbers</a>: additive identity, commutativity, and associativity. These proofs are used in the article <a href="/wiki/Addition_of_natural_numbers" title="Addition of natural numbers" class="mw-redirect">Addition of natural numbers</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Definitions"><span class="tocnumber">1</span> <span class="toctext">Definitions</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Proof_of_associativity"><span class="tocnumber">2</span> <span class="toctext">Proof of associativity</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Proof_of_identity_element"><span class="tocnumber">3</span> <span class="toctext">Proof of identity element</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Proof_of_commutativity"><span class="tocnumber">4</span> <span class="toctext">Proof of commutativity</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#See_also"><span class="tocnumber">5</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#References"><span class="tocnumber">6</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=1" title="Edit section: Definitions"></a>]</span> <span class="mw-headline" id="Definitions">Definitions</span></h2>
<p>This article will use the definitions in addition of natural numbers, particularly [A1] and [A2]:</p>
<ul>
<li>a + 0 = a [A1]</li>
<li>a + S(b) = S(a + b) [A2]</li>
</ul>
<p>For the proof of commutativity, it is useful to define another natural number closely related to the successor function, namely "1". We define 1 to be the successor of 0, in other words,</p>
<dl>
<dd><img class="tex" alt="1 \equiv S(0)\," src="//upload.wikimedia.org/wikipedia/en/math/f/6/b/f6bc266901bcfc06e0e271dcb25d8a52.png" />.</dd>
</dl>
<p>Note that for all natural numbers <i>a</i>,</p>
<dl>
<dd><img class="tex" alt=" \begin{align}
S(a) &amp;= S(a + 0)\\
&amp;= a + S(0)\\
&amp;= a + 1
\end{align}
" src="//upload.wikimedia.org/wikipedia/en/math/8/6/6/86659f1a5ef40e86e9f7352d53a631ee.png" /></dd>
</dl>
<p>due to [A1] and [A2].</p>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=2" title="Edit section: Proof of associativity"></a>]</span> <span class="mw-headline" id="Proof_of_associativity">Proof of associativity</span></h2>
<p>We prove <a href="/wiki/Associativity" title="Associativity" class="mw-redirect">associativity</a> by first fixing natural numbers <i>a</i> and <i>b</i> and applying <a href="/wiki/Mathematical_induction" title="Mathematical induction">induction</a> on the natural number <i>c</i>.</p>
<p>For the base case <i>c</i> = 0,</p>
<dl>
<dd><img class="tex" alt="(a+b)+0=a+b=a+(b+0)" src="//upload.wikimedia.org/wikipedia/en/math/5/d/5/5d58c63c40fc8fb8a1c630ae31ad5737.png" /></dd>
</dl>
<p>Each equation follows by definition [A1]; the first with <i>a</i> + <i>b</i>, the second with <i>b</i>.</p>
<p>Now, for the induction. We assume the induction hypothesis, namely we assume that for some natural number <i>c</i>,</p>
<dl>
<dd><img class="tex" alt="(a+b)+c=a+(b+c)" src="//upload.wikimedia.org/wikipedia/en/math/2/4/4/24458a9428d21eba1595d844e721db6b.png" /></dd>
</dl>
<p>Then it follows,</p>
<dl>
<dd>(<i>a</i> + <i>b</i>) + <i>S</i>(<i>c</i>)</dd>
<dd>= <i>S</i>((<i>a</i> + <i>b</i>) + <i>c</i>) [by A2]</dd>
<dd>= <i>S</i>(<i>a</i> + (<i>b</i> + <i>c</i>)) [by the induction hypothesis]</dd>
<dd>= <i>a</i> + <i>S</i>(<i>b</i> + <i>c</i>) [by A2]</dd>
<dd>= <i>a</i> + (<i>b</i> + <i>S</i>(<i>c</i>)) [by A2]</dd>
</dl>
<p>In other words, the induction hypothesis holds for <i>S</i>(<i>c</i>). Therefore, the induction on <i>c</i> is complete.</p>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=3" title="Edit section: Proof of identity element"></a>]</span> <span class="mw-headline" id="Proof_of_identity_element">Proof of identity element</span></h2>
<p>Definition [A1] states directly that 0 is a <a href="/wiki/Mathematical_identity" title="Mathematical identity" class="mw-redirect">right identity</a>. We prove that 0 is a <a href="/wiki/Mathematical_identity" title="Mathematical identity" class="mw-redirect">left identity</a> by induction on the natural number <i>a</i>.</p>
<p>For the base case <i>a</i> = 0, 0 + 0 = 0 by definition [A1]. Now we assume the induction hypothesis, that 0 + <i>a</i> = <i>a</i>. Then</p>
<dl>
<dd>0 + <i>S</i>(<i>a</i>)</dd>
<dd>= <i>S</i>(0 + <i>a</i>) [by A2]</dd>
<dd>= <i>S</i>(<i>a</i>) [by the induction hypothesis]</dd>
</dl>
<p>This completes the induction on <i>a</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=4" title="Edit section: Proof of commutativity"></a>]</span> <span class="mw-headline" id="Proof_of_commutativity">Proof of commutativity</span></h2>
<p>We prove <a href="/wiki/Commutativity" title="Commutativity" class="mw-redirect">commutativity</a> (<i>a</i> + <i>b</i> = <i>b</i> + <i>a</i>) by applying induction on the natural number <i>b</i>. First we prove the base cases <i>b</i> = 0 and <i>b</i> = <i>S</i>(0) = 1 (i.e. we prove that 0 and 1 commute with everything).</p>
<p>The base case <i>b</i> = 0 follows immediately from the identity element property (0 is an <a href="/wiki/Mathematical_identity" title="Mathematical identity" class="mw-redirect">additive identity</a>), which has been proved above: <i>a</i> + 0 = <i>a</i> = 0 + <i>a</i>.</p>
<p>Next we will prove the base case <i>b</i> = 1, that 1 commutes with everything, i.e. for all natural numbers <i>a</i>, we have <i>a</i> + 1 = 1 + <i>a</i>. We will prove this by induction on <i>a</i> (an induction proof within an induction proof). Clearly, for <i>a</i> = 0, we have 0 + 1 = 0 + <i>S</i>(0) = <i>S</i>(0 + 0) = <i>S</i>(0) = 1 = 1 + 0. Now, suppose <i>a</i> + 1 = 1 + <i>a</i>. Then</p>
<dl>
<dd><i>S</i>(<i>a</i>) + 1</dd>
<dd>= <i>S</i>(<i>a</i>) + <i>S</i>(0)</dd>
<dd>= <i>S</i>(<i>S</i>(<i>a</i>) + 0) [by A2]</dd>
<dd>= <i>S</i>((<i>a</i> + 1) + 0)</dd>
<dd>= <i>S</i>(<i>a</i> + 1) [by A1]</dd>
<dd>= <i>S</i>(1 + <i>a</i>) [by the induction hypothesis]</dd>
<dd>= 1 + <i>S</i>(<i>a</i>) [by A2]</dd>
</dl>
<p>This completes the induction on <i>a</i>, and so we have proved the base case <i>b</i> = 1. Now, suppose that for all natural numbers <i>a</i>, we have <i>a</i> + <i>b</i> = <i>b</i> + <i>a</i>. We must show that for all natural numbers <i>a</i>, we have <i>a</i> + <i>S</i>(<i>b</i>) = <i>S</i>(<i>b</i>) + <i>a</i>. We have</p>
<dl>
<dd><i>a</i> + <i>S</i>(<i>b</i>)</dd>
<dd>= <i>a</i> + (<i>b</i> + 1)</dd>
<dd>= (<i>a</i> + <i>b</i>) + 1 [by associativity]</dd>
<dd>= (<i>b</i> + <i>a</i>) + 1 [by the induction hypothesis]</dd>
<dd>= <i>b</i> + (<i>a</i> + 1) [by associativity]</dd>
<dd>= <i>b</i> + (1 + <i>a</i>) [by the base case <i>b</i> = 1]</dd>
<dd>= (<i>b</i> + 1) + <i>a</i> [by associativity]</dd>
<dd>= <i>S</i>(<i>b</i>) + <i>a</i></dd>
</dl>
<p>This completes the induction on <i>b</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=5" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Binary_operation" title="Binary operation">Binary operation</a></li>
<li><a href="/wiki/Mathematical_proofs" title="Mathematical proofs" class="mw-redirect">Proof</a></li>
<li><a href="/wiki/Mathematical_ring" title="Mathematical ring" class="mw-redirect">Ring</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Proofs_involving_the_addition_of_natural_numbers&amp;action=&amp;section=6" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li><a href="/wiki/Edmund_Landau" title="Edmund Landau">Edmund Landau</a>, Foundations of Analysis, Chelsea Pub Co. <a href="/wiki/Special:BookSources/082182693X" class="internal mw-magiclink-isbn">ISBN 0-8218-2693-X</a>.</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 62/1000000
Post-expand include size: 0/2048000 bytes
Template argument size: 0/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:2219011-0!0!0!!en!*!* and timestamp 20120301220857 -->
