<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Hyperbola_one_over_x.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/43/Hyperbola_one_over_x.svg/300px-Hyperbola_one_over_x.svg.png" width="300" height="225" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Hyperbola_one_over_x.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The reciprocal function: <i>y</i> = 1/<i>x</i>. For every <i>x</i> except 0, <i>y</i> represents its multiplicative inverse.</div>
</div>
</div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, a <b>multiplicative inverse</b> or <b>reciprocal</b> for a number <i>x</i>, denoted by 1/<i>x</i> or <i>x</i><sup>1</sup>, is a number which when multiplied by <i>x</i> yields the <a href="/wiki/Multiplicative_identity" title="Multiplicative identity" class="mw-redirect">multiplicative identity</a>, 1. The multiplicative inverse of a <a href="/wiki/Rational_number" title="Rational number">fraction</a> <i>a</i>/<i>b</i> is <i>b</i>/<i>a</i>. For the multiplicative inverse of a real number, divide 1 by the number. For example, the reciprocal of 5 is one fifth (1/5 or 0.2), and the reciprocal of 0.25 is 1 divided by 0.25, or 4. The <b>reciprocal function</b>, the function <i>f</i>(<i>x</i>) that maps <i>x</i> to 1/<i>x</i>, is one of the simplest examples of a function which is self-<a href="/wiki/Inverse_function" title="Inverse function">inverse</a>.</p>
<p>The term <i>reciprocal</i> was in common use at least as far back as the third ion of <i><a href="/wiki/Encyclop%C3%A6dia_Britannica" title="Encyclopdia Britannica">Encyclopdia Britannica</a></i> (1797) to describe two numbers whose product is 1; geometrical quantities in inverse proportion are described as <i>reciprocall</i> in a 1570 translation of <a href="/wiki/Euclid" title="Euclid">Euclid</a>'s <i><a href="/wiki/Euclid%27s_Elements" title="Euclid's Elements">Elements</a></i>.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup></p>
<p>In the phrase <i>multiplicative inverse</i>, the qualifier <i>multiplicative</i> is often omitted and then tacitly understood (in contrast to the <a href="/wiki/Additive_inverse" title="Additive inverse">additive inverse</a>). Multiplicative inverses can be defined over many mathematical domains as well as numbers. In these cases it can happen that <i>ab&#160;&#160;ba</i>; then "inverse" typically implies that an element is both a left and right <a href="/wiki/Inverse_element" title="Inverse element">inverse</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Practical_applications"><span class="tocnumber">1</span> <span class="toctext">Practical applications</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Examples_and_counterexamples"><span class="tocnumber">2</span> <span class="toctext">Examples and counterexamples</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Pseudo-random_number_generation"><span class="tocnumber">3</span> <span class="toctext">Pseudo-random number generation</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Reciprocals_of_irrational_numbers"><span class="tocnumber">4</span> <span class="toctext">Reciprocals of irrational numbers</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Further_remarks"><span class="tocnumber">5</span> <span class="toctext">Further remarks</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#See_also"><span class="tocnumber">6</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Notes"><span class="tocnumber">7</span> <span class="toctext">Notes</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#References"><span class="tocnumber">8</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=1" title="Edit section: Practical applications"></a>]</span> <span class="mw-headline" id="Practical_applications">Practical applications</span></h2>
<p>The multiplicative inverse has innumerable applications in algorithms of computer science, particularly those related to number theory, since many such algorithms rely heavily on the theory of modular arithmetic. As a simple example, consider the <i>exact division problem</i> where you have a list of odd word-sized numbers each divisible by <i>k</i> and you wish to divide them all by <i>k</i>. One solution is as follows:</p>
<ol>
<li>Use the extended Euclidean algorithm to compute <i>k</i><sup>1</sup>, the <a href="/wiki/Modular_multiplicative_inverse" title="Modular multiplicative inverse">modular multiplicative inverse</a> of <i>k</i> mod 2<sup><i>w</i></sup>, where <i>w</i> is the number of bits in a word. This inverse will exist since the numbers are odd and the modulus has no odd factors.</li>
<li>For each number in the list, multiply it by <i>k</i><sup>1</sup> and take the least significant word of the result.</li>
</ol>
<p>On many machines, particularly those without hardware support for division, division is a slower operation than multiplication, so this approach can yield a considerable speedup. The first step is relatively slow but only needs to be done once.</p>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=2" title="Edit section: Examples and counterexamples"></a>]</span> <span class="mw-headline" id="Examples_and_counterexamples">Examples and counterexamples</span></h2>
<p>In the field of real numbers, <a href="/wiki/0_(number)" title="0 (number)">Zero</a> does not have a reciprocal because no real number multiplied by 0 produces 1. With the exception of zero, reciprocals of every <a href="/wiki/Complex_number" title="Complex number">complex number</a> are complex, reciprocals of every <a href="/wiki/Real_number" title="Real number">real number</a> are real, and reciprocals of every <a href="/wiki/Rational_number" title="Rational number">rational number</a> are rational. The <a href="/wiki/Imaginary_unit" title="Imaginary unit">imaginary units</a>, <span class="texhtml"><i>i</i></span>, are the only complex numbers with <a href="/wiki/Additive_inverse" title="Additive inverse">additive inverse</a> equal to multiplicative inverse. For example, additive and multiplicative inverses of <span class="texhtml"><i>i</i></span> are (<span class="texhtml"><i>i</i></span>) = <span class="texhtml"><i>i</i></span> and 1/<span class="texhtml"><i>i</i></span> = <span class="texhtml"><i>i</i></span>, respectively.</p>
<p>To approximate the reciprocal of <i>x</i>, using only multiplication and subtraction, one can guess a number <i>y</i>, and then repeatedly replace <i>y</i> with 2<i>y</i>&#160;&#160;<i>xy</i><sup>2</sup>. Once the change in <i>y</i> becomes (and stays) sufficiently small, <i>y</i> is an approximation of the reciprocal of <i>x</i>.</p>
<p>In <a href="/wiki/Constructive_mathematics" title="Constructive mathematics" class="mw-redirect">constructive mathematics</a>, for a real number <i>x</i> to have a reciprocal, it is not sufficient that <i>x</i>  0. There must instead be given a <i>rational</i> number <i>r</i> such that 0&#160;&lt;&#160;<i>r</i>&#160;&lt;&#160;|<i>x</i>|. In terms of the approximation <a href="/wiki/Algorithm" title="Algorithm">algorithm</a> in the previous paragraph, this is needed to prove that the change in <i>y</i> will eventually become arbitrarily small.</p>
<p>In <a href="/wiki/Modular_arithmetic" title="Modular arithmetic">modular arithmetic</a>, the <a href="/wiki/Modular_multiplicative_inverse" title="Modular multiplicative inverse">modular multiplicative inverse</a> of <i>a</i> is also defined: it is the number <i>x</i> such that <i>ax</i>&#160;&#160;1&#160;(mod&#160;<i>n</i>). This multiplicative inverse exists <a href="/wiki/If_and_only_if" title="If and only if">if and only if</a> <i>a</i> and <i>n</i> are <a href="/wiki/Coprime" title="Coprime">coprime</a>. For example, the inverse of 3 modulo 11 is 4 because 4&#160;&#160;3&#160;&#160;1&#160;(mod&#160;11). The <a href="/wiki/Extended_Euclidean_algorithm" title="Extended Euclidean algorithm">extended Euclidean algorithm</a> may be used to compute it.</p>
<p>The <a href="/wiki/Sedenion" title="Sedenion">sedenions</a> are an algebra in which every nonzero element has a multiplicative inverse, but which nonetheless has divisors of zero, i.e. nonzero elements <i>x</i>, <i>y</i> such that <i>xy</i>&#160;= 0.</p>
<p>A <a href="/wiki/Square_matrix" title="Square matrix" class="mw-redirect">square matrix</a> has an inverse <a href="/wiki/If_and_only_if" title="If and only if">if and only if</a> its <a href="/wiki/Determinant" title="Determinant">determinant</a> has an inverse in the coefficient <a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">ring</a>. The linear map that has the matrix <i>A</i><sup>1</sup> with respect to some base is then the reciprocal function of the map having <i>A</i> as matrix in the same base. Thus, the two distinct notions of the inverse of a function are strongly related in this case, while they must be carefully distinguished in the general case (see below).</p>
<p>The <a href="/wiki/Trigonometric_functions" title="Trigonometric functions">trigonometric functions</a> are related by the reciprocal identity: the cotangent is the reciprocal of the tangent; the secant is the reciprocal of the cosine; the cosecant is the reciprocal of the sine.</p>
<p>It is important to distinguish the reciprocal of a function <i></i> in the multiplicative sense, given by 1/<i></i>, from the reciprocal or <b><a href="/wiki/Inverse_function" title="Inverse function">inverse function</a></b> with respect to composition, denoted by <i></i><sup>1</sup> and defined by <i></i>&#160;<small>o</small>&#160;<i></i><sup>1</sup>&#160;=&#160;id. Only for linear maps are they strongly related (see above), while they are completely different for all other cases. The terminology difference <i>reciprocal</i> versus <i>inverse</i> is not sufficient to make this distinction, since many authors prefer the opposite naming convention, probably for historical reasons (for example in <a href="/wiki/French_language" title="French language">French</a>, the inverse function is preferably called <a href="//en.wikipedia.org/wiki/fr:application_r%C3%A9ciproque" class="extiw" title="w:fr:application rciproque">application rciproque</a>).</p>
<p>A <a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">ring</a> in which every nonzero element has a multiplicative inverse is a <a href="/wiki/Division_ring" title="Division ring">division ring</a>; likewise an <a href="/wiki/Algebra_(ring_theory)" title="Algebra (ring theory)">algebra</a> in which this holds is a <a href="/wiki/Division_algebra" title="Division algebra">division algebra</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=3" title="Edit section: Pseudo-random number generation"></a>]</span> <span class="mw-headline" id="Pseudo-random_number_generation">Pseudo-random number generation</span></h2>
<p>The expansion of the reciprocal 1/<i>q</i> in any base can also act <sup id="cite_ref-Mitchell_1-0" class="reference"><a href="#cite_note-Mitchell-1"><span>[</span>2<span>]</span></a></sup> as a source of <a href="/wiki/Pseudo-random_numbers" title="Pseudo-random numbers" class="mw-redirect">pseudo-random numbers</a>, if <i>q</i> is a "suitable" <a href="/wiki/Safe_prime" title="Safe prime">safe prime</a>, a prime of the form 2<i>p</i>&#160;+&#160;1 where <i>p</i> is also a prime. A sequence of pseudo-random numbers of length <i>q</i>&#160;&#160;1 will be produced by the expansion.</p>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=4" title="Edit section: Reciprocals of irrational numbers"></a>]</span> <span class="mw-headline" id="Reciprocals_of_irrational_numbers">Reciprocals of irrational numbers</span></h2>
<p>Every number excluding zero has a reciprocal, and reciprocals of certain <a href="/wiki/Irrational_number" title="Irrational number">irrational numbers</a> often can prove useful for reasons linked to the irrational number in question. Examples of this are the reciprocal of <a href="/wiki/E_(mathematical_constant)" title="E (mathematical constant)">e</a> which is special because no other positive number can produce a lower number when put to the power of itself, and the <a href="/wiki/Golden_ratio#Golden_ratio_conjugate" title="Golden ratio">golden ratio's reciprocal</a> which, being roughly 0.6180339887, is exactly one less than the <a href="/wiki/Golden_ratio" title="Golden ratio">golden ratio</a> and in turn illustrates the uniqueness of the number.</p>
<p>There are an infinite number of irrational reciprocal pairs that differ by an integer (giving the curious effect that the pairs share their infinite mantissa). These pairs can be found by simplifying <i>n</i>+(<i>n</i><sup>2</sup>+1) for any integer <i>n</i>, and taking the reciprocal.</p>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=5" title="Edit section: Further remarks"></a>]</span> <span class="mw-headline" id="Further_remarks">Further remarks</span></h2>
<p>If the multiplication is associative, an element <i>x</i> with a multiplicative inverse cannot be a <a href="/wiki/Zero_divisor" title="Zero divisor">zero divisor</a> (meaning for some <i>y</i>, <i>xy</i>&#160;= 0 with neither <i>x</i> nor <i>y</i> equal to zero). To see this, it is sufficient to multiply the equation <i>xy</i>&#160;= 0 by the inverse of <i>x</i> (on the left), and then simplify using associativity. In the absence of associativity, the <a href="/wiki/Sedenion" title="Sedenion">sedenions</a> provide a counterexample.</p>
<p>The converse does not hold: an element which is not a <a href="/wiki/Zero_divisor" title="Zero divisor">zero divisor</a> is not guaranteed to have a multiplicative inverse. Within <b>Z</b>, all integers except 1, 0, 1 provide examples; they are not zero divisors nor do they have inverses in <b>Z</b>. If the ring or algebra is <a href="/wiki/Finite_set" title="Finite set">finite</a>, however, then all elements <i>a</i> which are not zero divisors do have a (left and right) inverse. For, first observe that the map <i></i>(<i>x</i>)&#160;= <i>ax</i> must be <a href="/wiki/Injective" title="Injective" class="mw-redirect">injective</a>: <i></i>(<i>x</i>)&#160;=&#160;<i></i>(<i>y</i>) implies <i>x</i>&#160;=&#160;<i>y</i>:</p>
<dl>
<dd><img class="tex" alt="\begin{align}
 ax &amp;= ay &amp;\quad \rArr &amp; \quad  ax-ay = 0 \\
 &amp; &amp;\quad \rArr  &amp;\quad  a(x-y) = 0 \\
 &amp; &amp;\quad \rArr  &amp;\quad  x-y = 0 \\
 &amp; &amp;\quad \rArr  &amp;\quad  x = y.
\end{align}" src="//upload.wikimedia.org/wikipedia/en/math/e/6/3/e63d34f524dcd3adb8b4d407f31d25ea.png" /></dd>
</dl>
<p>Distinct elements map to distinct elements, so the image consists of the same finite number of elements, and the map is necessarily <a href="/wiki/Surjective" title="Surjective" class="mw-redirect">surjective</a>. Specifically,  (namely multiplication by <i>a</i>) must map some element <i>x</i> to 1, <i>ax</i>&#160;= 1, so that <i>x</i> is an inverse for <i>a</i>.</p>
<p>The multiplicative inverse of a fraction <img class="tex" alt="\tfrac{a}{b}" src="//upload.wikimedia.org/wikipedia/en/math/e/c/3/ec3e447b2dcda3eb68860a5cb3780bf7.png" /> is simply <img class="tex" alt="\tfrac{b}{a}." src="//upload.wikimedia.org/wikipedia/en/math/2/a/c/2ac2cb80a384ccfed57b439bdd853f4f.png" /></p>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=6" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Division_(mathematics)" title="Division (mathematics)">Division (mathematics)</a></li>
<li><a href="/wiki/Fraction_(mathematics)" title="Fraction (mathematics)">Fraction (mathematics)</a></li>
<li><a href="/wiki/Group_(mathematics)" title="Group (mathematics)">Group (mathematics)</a></li>
<li><a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">Ring (mathematics)</a></li>
<li><a href="/wiki/Division_algebra" title="Division algebra">Division algebra</a></li>
<li><a href="/wiki/Exponential_decay" title="Exponential decay">Exponential decay</a></li>
<li><a href="/wiki/Unit_fraction" title="Unit fraction">Unit fractions</a>  reciprocals of integers</li>
<li><a href="/wiki/Hyperbola" title="Hyperbola">Hyperbola</a></li>
<li><a href="/wiki/Repeating_decimal" title="Repeating decimal">Repeating decimal</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=7" title="Edit section: Notes"></a>]</span> <span class="mw-headline" id="Notes">Notes</span></h2>
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">" In equall Parallelipipedons the bases are reciprokall to their altitudes". <i>OED</i> "Reciprocal" 3<i>a</i>. Sir <a href="/wiki/Henry_Billingsley" title="Henry Billingsley">Henry Billingsley</a> translation of Elements XI, 34.</span></li>
<li id="cite_note-Mitchell-1"><b><a href="#cite_ref-Mitchell_1-0">^</a></b> <span class="reference-text">Mitchell, Douglas W., "A nonlinear random number generator with known, long cycle length," <i><a href="/wiki/Cryptologia" title="Cryptologia">Cryptologia</a></i> 17, January 1993, 55-62.</span></li>
</ol>
<h2><span class="section">[<a href="/w/index.php?title=Multiplicative_inverse&amp;action=&amp;section=8" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li>Maximally Periodic Reciprocals, Matthews R.A.J. <i>Bulletin of the Institute of Mathematics and its Applications</i> vol 28 pp 147148 1992</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 188/1000000
Post-expand include size: 210/2048000 bytes
Template argument size: 30/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:229940-0!0!0!!en!4!* and timestamp 20120413020622 -->
