<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Linear_Function_Graph.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Linear_Function_Graph.svg/300px-Linear_Function_Graph.svg.png" width="300" height="300" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Linear_Function_Graph.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Graph sample of linear equations.</div>
</div>
</div>
<p>A <b>linear equation</b> is an <a href="/wiki/Algebraic_equation" title="Algebraic equation">algebraic equation</a> in which each <a href="/wiki/Term_(mathematics)" title="Term (mathematics)">term</a> is either a <a href="/wiki/Constant_term" title="Constant term">constant</a> or the product of a constant and (the first power of) a single <a href="/wiki/Variable_(mathematics)" title="Variable (mathematics)">variable</a>.</p>
<p>Linear equations can have one or more variables. Linear equations occur with great regularity in <a href="/wiki/Applied_mathematics" title="Applied mathematics">applied mathematics</a>. While they arise quite naturally when modeling many phenomena, they are particularly useful since many <a href="/wiki/Non-linear_equation" title="Non-linear equation" class="mw-redirect">non-linear equations</a> may be reduced to linear equations by assuming that quantities of interest vary to only a small extent from some "background" state. Linear equations do not include exponents.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Linear_equations_in_two_variables"><span class="tocnumber">1</span> <span class="toctext">Linear equations in two variables</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Forms_for_2D_linear_equations"><span class="tocnumber">1.1</span> <span class="toctext">Forms for 2D linear equations</span></a>
<ul>
<li class="toclevel-3 tocsection-3"><a href="#General_form"><span class="tocnumber">1.1.1</span> <span class="toctext">General form</span></a></li>
<li class="toclevel-3 tocsection-4"><a href="#Standard_form"><span class="tocnumber">1.1.2</span> <span class="toctext">Standard form</span></a></li>
<li class="toclevel-3 tocsection-5"><a href="#Slope.E2.80.93intercept_form"><span class="tocnumber">1.1.3</span> <span class="toctext">Slopeintercept form</span></a></li>
<li class="toclevel-3 tocsection-6"><a href="#Point.E2.80.93slope_form"><span class="tocnumber">1.1.4</span> <span class="toctext">Pointslope form</span></a></li>
<li class="toclevel-3 tocsection-7"><a href="#Two-point_form"><span class="tocnumber">1.1.5</span> <span class="toctext">Two-point form</span></a></li>
<li class="toclevel-3 tocsection-8"><a href="#Intercept_form"><span class="tocnumber">1.1.6</span> <span class="toctext">Intercept form</span></a></li>
<li class="toclevel-3 tocsection-9"><a href="#Parametric_form"><span class="tocnumber">1.1.7</span> <span class="toctext">Parametric form</span></a></li>
<li class="toclevel-3 tocsection-10"><a href="#Polar_form"><span class="tocnumber">1.1.8</span> <span class="toctext">Polar form</span></a></li>
<li class="toclevel-3 tocsection-11"><a href="#Normal_form"><span class="tocnumber">1.1.9</span> <span class="toctext">Normal form</span></a></li>
<li class="toclevel-3 tocsection-12"><a href="#2D_vector_determinant_form"><span class="tocnumber">1.1.10</span> <span class="toctext">2D vector determinant form</span></a></li>
<li class="toclevel-3 tocsection-13"><a href="#Special_cases"><span class="tocnumber">1.1.11</span> <span class="toctext">Special cases</span></a></li>
</ul>
</li>
<li class="toclevel-2 tocsection-14"><a href="#Connection_with_linear_functions"><span class="tocnumber">1.2</span> <span class="toctext">Connection with linear functions</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-15"><a href="#Linear_equations_in_more_than_two_variables"><span class="tocnumber">2</span> <span class="toctext">Linear equations in more than two variables</span></a></li>
<li class="toclevel-1 tocsection-16"><a href="#See_also"><span class="tocnumber">3</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-17"><a href="#References"><span class="tocnumber">4</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-18"><a href="#External_links"><span class="tocnumber">5</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=1" title="Edit section: Linear equations in two variables"></a>]</span> <span class="mw-headline" id="Linear_equations_in_two_variables">Linear equations in two variables</span></h2>
<p>A common form of a linear equation in the two variables <i>x</i> and <i>y</i> is</p>
<dl>
<dd><img class="tex" alt="y = mx + b,\," src="//upload.wikimedia.org/wikipedia/en/math/d/2/4/d24ebc87176b242c935535a363c5fc10.png" /></dd>
</dl>
<p>where <i>m</i> and <i>b</i> designate constants. The origin of the name "linear" comes from the fact that the set of solutions of such an equation forms a <a href="/wiki/Straight_line" title="Straight line" class="mw-redirect">straight line</a> in the plane. In this particular equation, the constant <i>m</i> determines the <a href="/wiki/Slope" title="Slope">slope</a> or gradient of that line, and the <a href="/wiki/Constant_term" title="Constant term">constant term</a> "b" determines the point at which the line crosses the <i>y</i>-axis, otherwise known as the y-intercept.</p>
<p>Since terms of linear equations cannot contain products of distinct or equal variables, nor any power (other than 1) or other function of a variable, equations involving terms such as <i>xy</i>, <i>x</i><sup>2</sup>, <i>y</i><sup>1/3</sup>, and sin(<i>x</i>) are <i><a href="/wiki/Nonlinear_system" title="Nonlinear system">nonlinear</a></i>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=2" title="Edit section: Forms for 2D linear equations"></a>]</span> <span class="mw-headline" id="Forms_for_2D_linear_equations">Forms for 2D linear equations</span></h3>
<p>Linear equations can be rewritten using the laws of <a href="/wiki/Elementary_algebra" title="Elementary algebra">elementary algebra</a> into several different forms. These equations are often referred to as the "equations of the straight line." In what follows, <i>x</i>, <i>y</i>, <i>t</i>, and <i></i> are variables; other letters represent <a href="/wiki/Constant_term" title="Constant term">constants</a> (fixed numbers).</p>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=3" title="Edit section: General form"></a>]</span> <span class="mw-headline" id="General_form">General form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="Ax + By + C = 0, \," src="//upload.wikimedia.org/wikipedia/en/math/f/e/7/fe70eb55a73ac2c2797af797140ceda9.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>where <i>A</i> and <i>B</i> are not both equal to zero. The equation is usually written so that <i>A</i>  0, by convention. The <a href="/wiki/Cartesian_coordinate_system" title="Cartesian coordinate system">graph</a> of the equation is a <a href="/wiki/Line_(geometry)" title="Line (geometry)">straight line</a>, and every straight line can be represented by an equation in the above form. If <i>A</i> is nonzero, then the <i>x</i>-intercept, that is, the <i>x</i>-<a href="/wiki/Coordinate" title="Coordinate" class="mw-redirect">coordinate</a> of the point where the graph crosses the <i>x</i>-axis (where, <i>y</i> is zero), is <i>C</i>/<i>A</i>. If <i>B</i> is nonzero, then the <i>y</i>-intercept, that is the <i>y</i>-coordinate of the point where the graph crosses the <i>y</i>-axis (where x is zero), is <i>C</i>/<i>B</i>, and the <a href="/wiki/Slope" title="Slope">slope</a> of the line is <i>A</i>/<i>B</i>.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=4" title="Edit section: Standard form"></a>]</span> <span class="mw-headline" id="Standard_form">Standard form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="Ax + By = C,\," src="//upload.wikimedia.org/wikipedia/en/math/7/e/6/7e6859919d495035e99c333d04e3e0b9.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>where <i>A</i> and <i>B</i> are not both equal to zero, <i>A</i>, <i>B</i>, and <i>C</i> are <a href="/wiki/Coprime" title="Coprime">coprime</a> integers, and <i>A</i> is nonnegative (if zero, <i>B</i> must be positive). The standard form can be converted to the general form, but not always to all the other forms if <i>A</i> or <i>B</i> is zero. It is worth noting that, while the term occurs frequently in school-level US algebra textbooks, most lines cannot be described by such equations. For instance, the line <i>x</i> + <i>y</i> = <span style="text-decoration:over line"><i>2</i></span> cannot be described by a linear equation with integer coefficients since <a href="/wiki/Square_root_of_2#Proofs_of_irrationality" title="Square root of 2"><span style="text-decoration:over line"><i>2</i></span> is irrational</a>.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=5" title="Edit section: Slopeintercept form"></a>]</span> <span class="mw-headline" id="Slope.E2.80.93intercept_form">Slopeintercept form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="y = mx + b,\," src="//upload.wikimedia.org/wikipedia/en/math/d/2/4/d24ebc87176b242c935535a363c5fc10.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>where <i>m</i> is the slope of the line and <i>b</i> is the <i>y</i>-intercept, which is the <i>y</i>-coordinate of the location where line crosses the <i>y</i> axis. This can be seen by letting <i>x</i> = 0, which immediately gives <i>y</i> = <i>b</i>. It may be helpful to think about this in terms of <i>y</i> = <i>b</i> + <i>mx</i>; where the line originates at (0, <i>b</i>) and extends outward at a slope of <i>m</i>. Vertical lines, having undefined slope, cannot be represented by this form.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=6" title="Edit section: Pointslope form"></a>]</span> <span class="mw-headline" id="Point.E2.80.93slope_form">Pointslope form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="y - y_1 = m( x - x_1 ),\," src="//upload.wikimedia.org/wikipedia/en/math/1/6/3/1630ed8fb9e5ff108c1ce0d334510300.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>where <i>m</i> is the slope of the line and (<i>x</i><sub>1</sub>,<i>y</i><sub>1</sub>) is any point on the line.</dd>
</dl>
<dl>
<dd>The point-slope form expresses the fact that the difference in the <i>y</i> coordinate between two points on a line (that is, <img class="tex" alt="y - y_1" src="//upload.wikimedia.org/wikipedia/en/math/d/6/b/d6b944d93b87f684655d07b9993e1eb1.png" />) is proportional to the difference in the <i>x</i> coordinate (that is, <img class="tex" alt="x - x_1" src="//upload.wikimedia.org/wikipedia/en/math/c/7/3/c73c9c3dafba7ee510993d32ae5daffa.png" />). The proportionality constant is <i>m</i> (the slope of the line).</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=7" title="Edit section: Two-point form"></a>]</span> <span class="mw-headline" id="Two-point_form">Two-point form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="y - y_1 = \frac{y_2 - y_1}{x_2 - x_1} (x - x_1),\," src="//upload.wikimedia.org/wikipedia/en/math/b/a/6/ba65ae69f70f05c690d155c5ad2f4379.png" /></dd>
</dl>
</dd>
<dd>where <img class="tex" alt="(x_1,y_1)" src="//upload.wikimedia.org/wikipedia/en/math/6/1/3/613c8cdd5c639e212bb058608712c542.png" /> and <img class="tex" alt="(x_2,y_2)" src="//upload.wikimedia.org/wikipedia/en/math/b/2/8/b283d7414b5877e4cc3234eee29bc11d.png" /> are two points on the line with <img class="tex" alt="x_2" src="//upload.wikimedia.org/wikipedia/en/math/8/f/4/8f43fce8dbdf3c4f8d0ac91f0de1d43d.png" />  <img class="tex" alt="x_1" src="//upload.wikimedia.org/wikipedia/en/math/f/9/a/f9a3b8e9e501458e8face47cae8826de.png" />. This is equivalent to the point-slope form above, where the slope is explicitly given as <img class="tex" alt="\frac{y_2 - y_1}{x_2 - x_1}" src="//upload.wikimedia.org/wikipedia/en/math/1/0/5/105c9caf57ea2bc141db9088948d974e.png" />.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=8" title="Edit section: Intercept form"></a>]</span> <span class="mw-headline" id="Intercept_form">Intercept form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="\frac{x}{a} + \frac{y}{b} = 1,\," src="//upload.wikimedia.org/wikipedia/en/math/9/6/d/96dfe2b3d959fd86971668e692fb6a76.png" /></dd>
</dl>
</dd>
<dd>where <i>a</i> and <i>b</i> must be nonzero. The graph of the equation has <i>x</i>-intercept <i>a</i> and <i>y</i>-intercept <i>b</i>. The intercept form can be converted to the standard form by setting <i>A</i> = 1/<i>a</i>, <i>B</i> = 1/<i>b</i> and <i>C</i> = 1.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=9" title="Edit section: Parametric form"></a>]</span> <span class="mw-headline" id="Parametric_form">Parametric form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="x = T t + U\," src="//upload.wikimedia.org/wikipedia/en/math/9/2/1/9214a74cdd0affe36340c35a864f7673.png" /></dd>
</dl>
</dd>
<dd>and
<dl>
<dd><img class="tex" alt="y = V t + W.\," src="//upload.wikimedia.org/wikipedia/en/math/a/4/d/a4da4b276cfffef11300d3bae03052e7.png" /></dd>
</dl>
</dd>
<dd>Two <a href="/wiki/Simultaneous_equations" title="Simultaneous equations">simultaneous equations</a> in terms of a variable parameter <i>t</i>, with slope <i>m</i> = <i>V</i> / <i>T</i>, <i>x</i>-intercept (<i>VU</i><i>WT</i>) / <i>V</i> and <i>y</i>-intercept (<i>WT</i><i>VU</i>) / <i>T</i>.</dd>
<dd>This can also be related to the two-point form, where <i>T</i> = <i>p</i><i>h</i>, <i>U</i> = <i>h</i>, <i>V</i> = <i>q</i><i>k</i>, and <i>W</i> = <i>k</i>:
<dl>
<dd><img class="tex" alt="x = (p - h) t + h\," src="//upload.wikimedia.org/wikipedia/en/math/1/6/9/169c655598a3f7df7078a9fa7de2d611.png" /></dd>
</dl>
</dd>
<dd>and
<dl>
<dd><img class="tex" alt="y = (q - k)t + k.\," src="//upload.wikimedia.org/wikipedia/en/math/1/2/c/12c52d9fc52373c79f7520a31d856b94.png" /></dd>
</dl>
</dd>
<dd>In this case <i>t</i> varies from 0 at point (<i>h</i>,<i>k</i>) to 1 at point (<i>p</i>,<i>q</i>), with values of <i>t</i> between 0 and 1 providing <a href="/wiki/Interpolation" title="Interpolation">interpolation</a> and other values of <i>t</i> providing <a href="/wiki/Extrapolation" title="Extrapolation">extrapolation</a>.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=10" title="Edit section: Polar form"></a>]</span> <span class="mw-headline" id="Polar_form">Polar form</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="r=\frac{mr\cos\theta+b}{\sin\theta}," src="//upload.wikimedia.org/wikipedia/en/math/2/0/0/20021a183a446f2e2840085fe715211b.png" /></dd>
</dl>
</dd>
<dd>where m is the slope of the line and b is the <a href="/wiki/Y-intercept" title="Y-intercept">y-intercept</a>. When <i> = 0</i> the graph will be undefined. The equation can be rewritten to eliminate discontinuities:
<dl>
<dd><img class="tex" alt="r\sin\theta=mr\cos\theta+b.\," src="//upload.wikimedia.org/wikipedia/en/math/c/b/0/cb04bd5f516edbc6acd0458db9669b97.png" /></dd>
</dl>
</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=11" title="Edit section: Normal form"></a>]</span> <span class="mw-headline" id="Normal_form">Normal form</span></h4>
<dl>
<dd>The <i>normal</i> for a given line is defined to be the shortest segment between the line and the origin. The normal form of the equation of a straight line is given by:
<dl>
<dd><img class="tex" alt=" y \sin \theta + x \cos \theta - p = 0,\," src="//upload.wikimedia.org/wikipedia/en/math/d/a/e/dae51458a9cf90a77d2026a9f23c58b6.png" /></dd>
</dl>
</dd>
<dd>where <i></i> is the angle of inclination of the normal, and <i>p</i> is the length of the normal. The normal form can be derived from general form by dividing all of the coefficients by</dd>
</dl>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="\frac{|C|}{-C}\sqrt{A^2 + B^2}." src="//upload.wikimedia.org/wikipedia/en/math/d/6/0/d600ba766ebe527508e88bf9277dd3e6.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>This form is also called the Hesse standard form, after the German mathematician <a href="/wiki/Otto_Hesse" title="Otto Hesse">Ludwig Otto Hesse</a>.</dd>
</dl>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=12" title="Edit section: 2D vector determinant form"></a>]</span> <span class="mw-headline" id="2D_vector_determinant_form">2D vector determinant form</span></h4>
<p>The equation of a line can also be written as the determinant of two vectors. If <img class="tex" alt="P_1" src="//upload.wikimedia.org/wikipedia/en/math/4/7/d/47ddd0a8d1607438330cf19c0c1ac45e.png" /> and <img class="tex" alt="P_2" src="//upload.wikimedia.org/wikipedia/en/math/2/d/1/2d145e3684093dda8dbfe869afa543f9.png" /> are unique points on the line, then <img class="tex" alt="P" src="//upload.wikimedia.org/wikipedia/en/math/4/4/c/44c29edb103a2872f519ad0c9a0fdaaa.png" /> will also be a point on the line if the following is true:</p>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="\det( \overrightarrow{P_1 P} , \overrightarrow{P_1 P_2} ) = 0. " src="//upload.wikimedia.org/wikipedia/en/math/7/c/4/7c4a9d8cb68712726f3291165ad1d28f.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>One way to understand this formula is to use the fact that the determinant of two vectors on the plane will give the area of the parallelogram they form. Therefore, if the determinate equals zero then the parallelogram has no area, and that will happen when to vectors are on the same line.</dd>
</dl>
<p>To expand on this we can say that <img class="tex" alt="P_1 = (x_1 ,\, y_1)" src="//upload.wikimedia.org/wikipedia/en/math/9/3/0/93002ce511b79123f736ff031b19db41.png" />, <img class="tex" alt="P_2 = (x_2 ,\, y_2)" src="//upload.wikimedia.org/wikipedia/en/math/9/b/b/9bb3e833f78e2acd2ab4c57ccc35275c.png" /> and <img class="tex" alt="P = (x ,\, y)" src="//upload.wikimedia.org/wikipedia/en/math/d/7/a/d7a1e7d0506bd100af80a94fb952c872.png" />. Thus <img class="tex" alt="\overrightarrow{P_1 P} = (x-x_1 ,\, y-y_1) " src="//upload.wikimedia.org/wikipedia/en/math/3/0/0/30054d37f9f13b8a1184282c68f6b0bf.png" /> and <img class="tex" alt="\overrightarrow{P_1 P_2} = (x_2-x_1 ,\, y_2-y_1)" src="//upload.wikimedia.org/wikipedia/en/math/0/c/f/0cf3e8a5bb7303552c71b7bae7ed7b82.png" />, then the above equation becomes:</p>
<dl>
<dd><img class="tex" alt="\det \begin{pmatrix}x-x_1&amp;y-y_1\\x_2-x_1&amp;y_2-y_1\end{pmatrix} = 0." src="//upload.wikimedia.org/wikipedia/en/math/b/0/9/b091c93b4eff6c7ae88deb2d8bdb7e91.png" /></dd>
</dl>
<p>Thus,</p>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="( x - x_1 )( y_2 - y_1 ) - ( y - y_1 )( x_2 - x_1 )=0." src="//upload.wikimedia.org/wikipedia/en/math/8/2/4/824a4e3925bf1cc4a07fa81082366a15.png" /></dd>
</dl>
</dd>
</dl>
<p>Ergo,</p>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="( x - x_1 )( y_2 - y_1 ) = ( y - y_1 )( x_2 - x_1 )." src="//upload.wikimedia.org/wikipedia/en/math/c/7/b/c7b2e38c07521b6f26705949b45426f5.png" /></dd>
</dl>
</dd>
</dl>
<p>Then dividing both side by <img class="tex" alt="( x_2 - x_1 )" src="//upload.wikimedia.org/wikipedia/en/math/6/e/1/6e1e7e74f0a20a67102b2967c28d9c00.png" /> would result in the Two-point form shown above, but leaving it here allows the equation to still be valid when <img class="tex" alt="x_1 = x_2" src="//upload.wikimedia.org/wikipedia/en/math/a/2/d/a2dc8663950f53a424c27fee0c7af6d9.png" />.</p>
<h4><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=13" title="Edit section: Special cases"></a>]</span> <span class="mw-headline" id="Special_cases">Special cases</span></h4>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="y = b\," src="//upload.wikimedia.org/wikipedia/en/math/3/7/d/37d09870849fedef6192834237e7eaaa.png" />
<div class="thumb tright">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:Horizontal-Line.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Horizontal-Line.png/220px-Horizontal-Line.png" width="220" height="144" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Horizontal-Line.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Horizontal Line <i>y</i> = <i>b</i></div>
</div>
</div>
</dd>
</dl>
</dd>
<dd>This is a special case of the standard form where <i>A</i> = 0 and <i>B</i> = 1, or of the slope-intercept form where the slope <i>M</i> = 0. The graph is a horizontal line with <i>y</i>-intercept equal to <i>b</i>. There is no <i>x</i>-intercept, unless <i>b</i> = 0, in which case the graph of the line is the <i>x</i>-axis, and so every real number is an <i>x</i>-intercept.</dd>
</dl>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="x = a\," src="//upload.wikimedia.org/wikipedia/en/math/5/d/3/5d3f711b9a5e7c2aa2b105557a68dd6a.png" />
<div class="thumb tright">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:Vertical-Line.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/69/Vertical-Line.png/220px-Vertical-Line.png" width="220" height="145" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Vertical-Line.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Vertical Line <i>x</i> = <i>a</i></div>
</div>
</div>
</dd>
</dl>
</dd>
<dd>This is a special case of the standard form where <i>A</i> = 1 and <i>B</i> = 0. The graph is a vertical line with <i>x</i>-intercept equal to <i>a</i>. The slope is undefined. There is no <i>y</i>-intercept, unless <i>a</i> = 0, in which case the graph of the line is the <i>y</i>-axis, and so every real number is a <i>y</i>-intercept.</dd>
</dl>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="y = y \ " src="//upload.wikimedia.org/wikipedia/en/math/0/a/0/0a007f9fd8c304bfae1588ca86d09ce9.png" /> and <img class="tex" alt=" x = x\," src="//upload.wikimedia.org/wikipedia/en/math/7/a/5/7a544d5211853e37241deb3b4d2af0a4.png" /></dd>
</dl>
</dd>
<dd>In this case all variables and constants have canceled out, leaving a trivially true statement. The original equation, therefore, would be called an <i><a href="/wiki/Identity_(mathematics)" title="Identity (mathematics)">identity</a></i> and one would not normally consider its graph (it would be the entire <i>xy</i>-plane). An example is 2<i>x</i> + 4<i>y</i> = 2(<i>x</i> + 2<i>y</i>). The two expressions on either side of the equal sign are <i>always</i> equal, no matter what values are used for <i>x</i> and <i>y</i>.</dd>
</dl>
<dl>
<dd>
<dl>
<dd><img class="tex" alt=" e = f\, " src="//upload.wikimedia.org/wikipedia/en/math/0/6/2/062099893307254de99d40b4fca4848e.png" /></dd>
</dl>
</dd>
</dl>
<dl>
<dd>In situations where algebraic manipulation leads to a statement such as <b>1 = 0</b>, then the original equation is called <i>inconsistent</i>, meaning it is untrue for any values of <i>x</i> and <i>y</i> (i.e., its graph would be the <a href="/wiki/Empty_set" title="Empty set">empty set</a>). An example would be 3<i>x</i> + 2 = 3<i>x</i>  5.</dd>
</dl>
<h3><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=14" title="Edit section: Connection with linear functions"></a>]</span> <span class="mw-headline" id="Connection_with_linear_functions">Connection with linear functions</span></h3>
<p>A linear equation, written in the form <i>y</i> = <i>f</i>(<i>x</i>) whose graph crosses through the origin, that is whose <i>y</i>-intercept is 0, has the following properties:</p>
<dl>
<dd><img class="tex" alt=" f ( x_1 + x_2 ) = f ( x_1) + f ( x_2 )\ " src="//upload.wikimedia.org/wikipedia/en/math/5/a/6/5a6df8ab02b9c3107d533df1ce70a153.png" /></dd>
</dl>
<p>and</p>
<dl>
<dd><img class="tex" alt=" f ( a x ) = a f ( x ),\," src="//upload.wikimedia.org/wikipedia/en/math/9/7/6/9760d5824466b43115971d3b9d93a223.png" /></dd>
</dl>
<p>where <i>a</i> is any <a href="/wiki/Scalar_(mathematics)" title="Scalar (mathematics)">scalar</a>. A function which satisfies these properties is called a <i>linear function</i> (or <i>linear operator</i>, or more generally a <i><a href="/wiki/Linear_map" title="Linear map">linear map</a></i>). However, linear equations that have non-zero <i>y</i>-intercepts will have neither property above and hence are not linear functions in this sense.</p>
<h2><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=15" title="Edit section: Linear equations in more than two variables"></a>]</span> <span class="mw-headline" id="Linear_equations_in_more_than_two_variables">Linear equations in more than two variables</span></h2>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/System_of_linear_equations" title="System of linear equations">System of linear equations</a></div>
<p>A linear equation can involve more than two variables. The general linear equation in <i>n</i> variables is:</p>
<dl>
<dd><img class="tex" alt="a_1 x_1 + a_2 x_2 + \cdots + a_n x_n = b." src="//upload.wikimedia.org/wikipedia/en/math/d/8/e/d8ea72a9337f856e607ad2b87c3c6656.png" /></dd>
</dl>
<p>In this form, <i>a</i><sub>1</sub>, <i>a</i><sub>2</sub>, , <i>a</i><sub><i>n</i></sub> are the coefficients, <i>x</i><sub>1</sub>, <i>x</i><sub>2</sub>, , <i>x</i><sub><i>n</i></sub> are the variables, and <i>b</i> is the constant. When dealing with three or fewer variables, it is common to replace <i>x</i><sub>1</sub> with just <i>x</i>, <i>x</i><sub>2</sub> with <i>y</i>, and <i>x</i><sub>3</sub> with <i>z</i>, as appropriate.</p>
<p>Such an equation will represent an (<i>n</i>1)-dimensional <a href="/wiki/Hyperplane" title="Hyperplane">hyperplane</a> in <i>n</i>-dimensional <a href="/wiki/Euclidean_space" title="Euclidean space">Euclidean space</a> (for example, a plane in 3-space).</p>
<p>In vector notation, this can be expressed as:</p>
<dl>
<dd><img class="tex" alt="\overrightarrow{n} \cdot \overrightarrow{x} = \overrightarrow{n} \cdot \overrightarrow{x_0}" src="//upload.wikimedia.org/wikipedia/en/math/3/4/d/34d7d9ae0d024d91f98b0e5b50e7aed2.png" /></dd>
</dl>
<p>where <img class="tex" alt="\overrightarrow{n}" src="//upload.wikimedia.org/wikipedia/en/math/6/1/6/6163aed54efd156e758028a4964eda73.png" /> is a vector normal to the plane, <img class="tex" alt="\overrightarrow{x}" src="//upload.wikimedia.org/wikipedia/en/math/0/b/8/0b884a8aa04309ace1ef8af90d7b7c42.png" /> are the coordinates of any point on the plane, and <img class="tex" alt="\overrightarrow{x_0}" src="//upload.wikimedia.org/wikipedia/en/math/c/3/9/c391fca5524762444af33cae43458ee5.png" /> are the coordinates of the origin of the plane.</p>
<h2><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=16" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Line_(geometry)" title="Line (geometry)">Line (geometry)</a></li>
<li><a href="/wiki/Quadratic_equation" title="Quadratic equation">Quadratic equation</a></li>
<li><a href="/wiki/Cubic_equation" title="Cubic equation" class="mw-redirect">Cubic equation</a></li>
<li><a href="/wiki/Quartic_equation" title="Quartic equation" class="mw-redirect">Quartic equation</a></li>
<li><a href="/wiki/Quintic_equation" title="Quintic equation" class="mw-redirect">Quintic equation</a></li>
<li><a href="/wiki/Linear_inequality" title="Linear inequality">Linear inequality</a></li>
<li><a href="/wiki/Linear_belief_function" title="Linear belief function">Linear belief function</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=17" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;"></div>
<h2><span class="section">[<a href="/w/index.php?title=Linear_equation&amp;action=&amp;section=18" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://eqworld.ipmnet.ru/en/solutions/ae.htm">Algebraic Equations</a> at EqWorld: The World of Mathematical Equations.</li>
<li><a rel="nofollow" class="external autonumber" href="http://video.google.com/videoplay?docid=-5991926520926012350#">[1]</a> Video tutorial on solving one step to multistep equations</li>
<li><a rel="nofollow" class="external text" href="http://catalog.flatworldknowledge.com/bookhub/reader/128?e=fwk-redden-ch02">Linear Equations and Inequalities</a> Open Elementary Algebra textbook chapter on linear equations and inequalities.</li>
</ul>
<table cellspacing="0" class="navbox" style="border-spacing:0;;">
<tr>
<td style="padding:2px;">
<table cellspacing="0" class="nowraplinks collapsible autocollapse navbox-inner" style="border-spacing:0;background:transparent;color:inherit;;">
<tr>
<th scope="col" style=";" class="navbox-title" colspan="2">
<div class="noprint plainlinks hlist navbar mini" style="">
<ul>
<li class="nv-view"><a href="/wiki/Template:Polynomials" title="Template:Polynomials"><span title="View this template" style=";;background:none transparent;border:none;">v</span></a></li>
<li class="nv-talk"><a href="/w/index.php?title=Template_talk:Polynomials&amp;action=&amp;redlink=1" class="new" title="Template talk:Polynomials (page does not exist)"><span title="Discuss this template" style=";;background:none transparent;border:none;">t</span></a></li>
<li class="nv-"><a class="external text" href="//en.wikipedia.org/w/index.php?title=Template:Polynomials&amp;action="><span title="Edit this template" style=";;background:none transparent;border:none;">e</span></a></li>
</ul>
</div>
<div class="" style="font-size:110%;"><a href="/wiki/Polynomial" title="Polynomial">Polynomials</a></div>
</th>
</tr>
<tr style="height:2px;">
<td></td>
</tr>
<tr>
<th scope="row" class="navbox-group" style=";;">By name</th>
<td style="text-align:left;border-left-width:2px;border-left-style:solid;width:100%;padding:0px;;;" class="navbox-list navbox-odd">
<div style="padding:0em 0.25em"><a href="/wiki/Zero_polynomial" title="Zero polynomial" class="mw-redirect">Zero polynomial</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Constant_function" title="Constant function">Constant function</a> &#160;<span style="font-weight:bold;"></span>  <strong class="selflink">Linear equation</strong> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quadratic_function" title="Quadratic function">Quadratic function</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Cubic_function" title="Cubic function">Cubic function</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quartic_function" title="Quartic function">Quartic function</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quintic_function" title="Quintic function">Quintic function</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Sextic_equation" title="Sextic equation">Sextic equation</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Septic_equation" title="Septic equation">Septic equation</a> &#160;<span style="font-weight:bold;"></span>  <a href="/w/index.php?title=Octic_equation&amp;action=&amp;redlink=1" class="new" title="Octic equation (page does not exist)">Octic equation</a> &#160;<span style="font-weight:bold;"></span>  <a href="/w/index.php?title=Nonic_equation&amp;action=&amp;redlink=1" class="new" title="Nonic equation (page does not exist)">Nonic equation</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Decic_equation" title="Decic equation">Decic equation</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Hectic_equation" title="Hectic equation" class="mw-redirect">Hectic equation</a></div>
</td>
</tr>
<tr style="height:2px">
<td></td>
</tr>
<tr>
<th scope="row" class="navbox-group" style=";;">By <a href="/wiki/Degree_of_a_polynomial" title="Degree of a polynomial">degree</a></th>
<td style="text-align:left;border-left-width:2px;border-left-style:solid;width:100%;padding:0px;;;" class="navbox-list navbox-even">
<div style="padding:0em 0.25em"><a href="/wiki/Constant_function" title="Constant function">0</a> &#160;<span style="font-weight:bold;"></span>  <strong class="selflink">1</strong> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quadratic_function" title="Quadratic function">2</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Cubic_function" title="Cubic function">3</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quartic_function" title="Quartic function">4</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Quintic_function" title="Quintic function">5</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Sextic_equation" title="Sextic equation">6</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Septic_equation" title="Septic equation">7</a> &#160;<span style="font-weight:bold;"></span>  <a href="/w/index.php?title=Octic_equation&amp;action=&amp;redlink=1" class="new" title="Octic equation (page does not exist)">8</a> &#160;<span style="font-weight:bold;"></span>  <a href="/w/index.php?title=Nonic_equation&amp;action=&amp;redlink=1" class="new" title="Nonic equation (page does not exist)">9</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Decic_equation" title="Decic equation">10</a> &#160;<span style="font-weight:bold;"></span>  <a href="/wiki/Hectic_equation" title="Hectic equation" class="mw-redirect">100</a></div>
</td>
</tr>
</table>
</td>
</tr>
</table>


<!-- 
NewPP limit report
Preprocessor node count: 954/1000000
Post-expand include size: 14827/2048000 bytes
Template argument size: 5484/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:17570-0!0!0!!en!4!* and timestamp 20120417055944 -->
