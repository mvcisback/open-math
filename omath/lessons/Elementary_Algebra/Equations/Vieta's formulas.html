<div class="dablink">For a method for computing , see <a href="/wiki/Vi%C3%A8te%27s_formula" title="Vite's formula">Vite's formula</a>.</div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, <b>Vieta's formulas</b> are <a href="/wiki/Formula" title="Formula">formulas</a> that relate the coefficients of a <a href="/wiki/Polynomial" title="Polynomial">polynomial</a> to sums and products of its <a href="/wiki/Root_of_a_function" title="Root of a function" class="mw-redirect">roots</a>. Named after <a href="/wiki/Fran%C3%A7ois_Vi%C3%A8te" title="Franois Vite">Franois Vite</a> (more commonly referred to by the Latinised form of his name, <b>Franciscus Vieta</b>), the formulas are used specifically in <a href="/wiki/Algebra" title="Algebra">algebra</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#The_Laws"><span class="tocnumber">1</span> <span class="toctext">The Laws</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Basic_formulas"><span class="tocnumber">1.1</span> <span class="toctext">Basic formulas</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Generalization_to_rings"><span class="tocnumber">1.2</span> <span class="toctext">Generalization to rings</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Example"><span class="tocnumber">2</span> <span class="toctext">Example</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Proof"><span class="tocnumber">3</span> <span class="toctext">Proof</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#History"><span class="tocnumber">4</span> <span class="toctext">History</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#See_also"><span class="tocnumber">5</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#References"><span class="tocnumber">6</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=1" title="Edit section: The Laws"></a>]</span> <span class="mw-headline" id="The_Laws">The Laws</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=2" title="Edit section: Basic formulas"></a>]</span> <span class="mw-headline" id="Basic_formulas">Basic formulas</span></h3>
<p>Any general polynomial of degree <i>n</i></p>
<dl>
<dd><img class="tex" alt="p(X)=a_nX^n  + a_{n-1}X^{n-1} +\cdots + a_1 X+ a_0 \, " src="//upload.wikimedia.org/wikipedia/en/math/4/f/1/4f188c872d23b87f8860342709d13eec.png" /></dd>
</dl>
<p>(with the coefficients being real or complex numbers and <i>a</i><sub><i>n</i></sub>&#160;&#160;0) is known by the <a href="/wiki/Fundamental_theorem_of_algebra" title="Fundamental theorem of algebra">fundamental theorem of algebra</a> to have <i>n</i> (not necessarily distinct) complex roots <i>x</i><sub>1</sub>,&#160;<i>x</i><sub>2</sub>,&#160;...,&#160;<i>x</i><sub><i>n</i></sub>. Vieta's formulas relate the polynomial's coefficients {&#160;<i>a</i><sub><i>k</i></sub>&#160;} to signed sums and products of its roots {&#160;<i>x</i><sub><i>i</i></sub>&#160;} as follows:</p>
<dl>
<dd><img class="tex" alt="\begin{cases} x_1 + x_2 + \dots + x_{n-1} + x_n = -\tfrac{a_{n-1}}{a_n} \\ 
(x_1 x_2 + x_1 x_3+\cdots + x_1x_n) + (x_2x_3+x_2x_4+\cdots + x_2x_n)+\cdots + x_{n-1}x_n = \frac{a_{n-2}}{a_n} \\
{} \quad \vdots \\ x_1 x_2 \dots x_n = (-1)^n \tfrac{a_0}{a_n}. \end{cases}" src="//upload.wikimedia.org/wikipedia/en/math/b/3/9/b39f53a77fcf4227622de404e2298260.png" /></dd>
</dl>
<p>Equivalently stated, the (<i>n</i>&#160;&#160;<i>k</i>)th coefficient <i>a</i><sub><i>n</i><i>k</i></sub> is related to a signed sum of all possible subproducts of roots, taken <i>k</i>-at-a-time:</p>
<dl>
<dd><img class="tex" alt="\sum_{1\le i_1 &lt; i_2 &lt; \cdots &lt; i_k\le n} x_{i_1}x_{i_2}\cdots x_{i_k}=(-1)^k\frac{a_{n-k}}{a_n}" src="//upload.wikimedia.org/wikipedia/en/math/9/2/7/9274c5d51e4e10ab6a1361232ff59dc5.png" /></dd>
</dl>
<p>for <i>k</i>&#160;=&#160;1,&#160;2,&#160;...,&#160;<i>n</i> (where we wrote the indices <i>i</i><sub><i>k</i></sub> in increasing order to ensure each subproduct of roots is used exactly once).</p>
<p>The left hand sides of Vieta's formulas are the <b><a href="/wiki/Elementary_symmetric_polynomial" title="Elementary symmetric polynomial">elementary symmetric functions</a></b> of the roots.</p>
<h3><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=3" title="Edit section: Generalization to rings"></a>]</span> <span class="mw-headline" id="Generalization_to_rings">Generalization to rings</span></h3>
<p>Vieta's formulas are frequently used with polynomials with coefficients in any <a href="/wiki/Integral_domain" title="Integral domain">integral domain</a> <i>R</i>. In this case the quotients <img class="tex" alt="a_i/a_n" src="//upload.wikimedia.org/wikipedia/en/math/0/c/8/0c852d8630356875f19641568a62d27c.png" /> belong to the <a href="/wiki/Ring_of_fractions" title="Ring of fractions" class="mw-redirect">ring of fractions</a> of <i>R</i> (or in <i>R</i> itself if <img class="tex" alt="a_n" src="//upload.wikimedia.org/wikipedia/en/math/9/d/e/9ded7825070b255e7bc092cdc2c8e98a.png" /> is invertible in <i>R</i>) and the roots <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" /> are taken in an <a href="/wiki/Algebraically_closed_field" title="Algebraically closed field">algebraically closed extension</a>. Typically, <i>R</i> is the ring of the <a href="/wiki/Integer" title="Integer">integers</a>, the field of fractions is the field of the <a href="/wiki/Rational_number" title="Rational number">rational numbers</a> and the algebraically closed field is the field of the <a href="/wiki/Complex_numbers" title="Complex numbers" class="mw-redirect">complex numbers</a>.</p>
<p>Vieta's formulas are useful in this situation, because they provide relations between the roots without to have to compute them.</p>
<p>For polynomials over a commutative ring which is not an integral domain, Vieta's formulas may be used only when the <img class="tex" alt="a_i" src="//upload.wikimedia.org/wikipedia/en/math/d/8/d/d8dd7d0f3eb7145ca41c711457b7eb8f.png" />'s are computed from the <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" />'s. For example, in the ring of the integers <a href="/wiki/Modulo" title="Modulo">modulo</a> 8, the polynomial <img class="tex" alt="x^2-1" src="//upload.wikimedia.org/wikipedia/en/math/8/7/1/8713a66a0a3e5f37cbf1103af6f56511.png" /> has four roots 1, 3, 5, 7, and Vieta's formulas are not true if, say, <img class="tex" alt="x_1=1" src="//upload.wikimedia.org/wikipedia/en/math/6/3/6/6366c1b5884f645a527ca215c47e6327.png" /> and <img class="tex" alt="x_2=3" src="//upload.wikimedia.org/wikipedia/en/math/3/f/e/3fec3bb7d5a5a6be9bd113ae7d765a1d.png" />.</p>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=4" title="Edit section: Example"></a>]</span> <span class="mw-headline" id="Example">Example</span></h2>
<p>Vieta's formulas applied to quadratic and cubic polynomial:</p>
<p>For the <a href="/wiki/Second_degree_polynomial" title="Second degree polynomial" class="mw-redirect">second degree polynomial</a> (quadratic) <img class="tex" alt="p(X)=aX^2 + bX + c" src="//upload.wikimedia.org/wikipedia/en/math/d/c/f/dcfe4c03aaa59de82d21e27baad308ed.png" />, roots <img class="tex" alt="x_1, x_2" src="//upload.wikimedia.org/wikipedia/en/math/9/8/6/9865b118af4cfc107929ec116ab9eb80.png" /> of the equation <img class="tex" alt="p(X)=0" src="//upload.wikimedia.org/wikipedia/en/math/d/7/e/d7e7adc2dd48a6c03be5b50453ceab2b.png" /> satisfy</p>
<dl>
<dd><img class="tex" alt=" x_1 + x_2 = - \frac{b}{a}, \quad x_1 x_2 = \frac{c}{a}." src="//upload.wikimedia.org/wikipedia/en/math/9/d/7/9d7abdfe24fd93c51013c00ee24f1c26.png" /></dd>
</dl>
<p>The first of these equations can be used to find the minimum (or maximum) of <i>p</i>. See <a href="/wiki/Quadratic_equation#Vieta.27s_formulas" title="Quadratic equation">second order polynomial</a>.</p>
<p>For the <a href="/wiki/Cubic_polynomial" title="Cubic polynomial" class="mw-redirect">cubic polynomial</a> <img class="tex" alt="p(X)=aX^3 + bX^2 + cX + d" src="//upload.wikimedia.org/wikipedia/en/math/c/0/7/c07ac7a3c69376e3a1331e6482506def.png" />, roots <img class="tex" alt="x_1, x_2, x_3" src="//upload.wikimedia.org/wikipedia/en/math/f/1/b/f1b850cae96c67f362658f6786f507d8.png" /> of the equation <img class="tex" alt="p(X)=0" src="//upload.wikimedia.org/wikipedia/en/math/d/7/e/d7e7adc2dd48a6c03be5b50453ceab2b.png" /> satisfy</p>
<dl>
<dd><img class="tex" alt=" x_1 + x_2 + x_3 = - \frac{b}{a}, \quad x_1 x_2 + x_1 x_3 + x_2 x_3 = \frac{c}{a}, \quad x_1 x_2 x_3 = - \frac{d}{a}." src="//upload.wikimedia.org/wikipedia/en/math/7/d/1/7d1709a87868e84b4e4e57fc47a06b0e.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=5" title="Edit section: Proof"></a>]</span> <span class="mw-headline" id="Proof">Proof</span></h2>
<p>Vieta's formulas can be proved by expanding the equality</p>
<dl>
<dd><img class="tex" alt="a_nX^n  + a_{n-1}X^{n-1} +\cdots + a_1 X+ a_0 = a_n(X-x_1)(X-x_2)\cdots (X-x_n)" src="//upload.wikimedia.org/wikipedia/en/math/d/8/1/d81235b6992150f1de50a84306bf756f.png" /></dd>
</dl>
<p>(which is true since <img class="tex" alt="x_1, x_2, \dots, x_n" src="//upload.wikimedia.org/wikipedia/en/math/3/4/f/34feb8e4d8944bfb796e8813d614d3bd.png" /> are all the roots of this polynomial), multiplying the factors on the right-hand side, and identifying the coefficients of each power of <img class="tex" alt="X." src="//upload.wikimedia.org/wikipedia/en/math/3/d/2/3d2f9e254afe4be361f104d3748e8570.png" /></p>
<p>Formally, if one expands <img class="tex" alt="(X-x_1)(X-x_2)\cdots(X-x_n)," src="//upload.wikimedia.org/wikipedia/en/math/7/c/3/7c30b2a7efa494113e9ac74837398466.png" /> the terms are precisely <img class="tex" alt="(-1)^{n-k}x_1^{b_1}\cdots x_n^{b_n} X^k," src="//upload.wikimedia.org/wikipedia/en/math/5/e/c/5ec5527f89e640902f531f8acb54347c.png" /> where <img class="tex" alt="b_i" src="//upload.wikimedia.org/wikipedia/en/math/c/9/f/c9f6d8557ce40f989fa727b5c0bb1ddf.png" /> is either 0 or 1, accordingly as whether <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" /> is included in the product or not, and <i>k</i> is the number of <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" /> that are excluded, so the total number of factors in the product is <i>n</i> (counting <i><img class="tex" alt="X^k" src="//upload.wikimedia.org/wikipedia/en/math/0/5/7/0571e9aafebb2766957bf962d30f4276.png" /></i> with multiplicity <i>k</i>)  as there are <i>n</i> binary choices (include <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" /> or <i>X</i>), there are <img class="tex" alt="2^n" src="//upload.wikimedia.org/wikipedia/en/math/9/a/a/9aa0ec0374c89d2f7f3d9cd2e05a4bc5.png" /> terms  geometrically, these can be understood as the vertices of a hypercube. Grouping these terms by degree yields the elementary symmetric polynomials in <img class="tex" alt="x_i" src="//upload.wikimedia.org/wikipedia/en/math/0/5/e/05e42209d67fe1eb15a055e9d3b3770e.png" />  for <i>X<sup>k</sup>,</i> all distinct <i>k</i>-fold products of <img class="tex" alt="x_i." src="//upload.wikimedia.org/wikipedia/en/math/2/5/a/25ab85b2cdafba728e87ae9552550e53.png" /></p>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=6" title="Edit section: History"></a>]</span> <span class="mw-headline" id="History">History</span></h2>
<p>As reflected in the name, these formulas were discovered by the 16th century French mathematician <a href="/wiki/Fran%C3%A7ois_Vi%C3%A8te" title="Franois Vite">Franois Vite</a>, for the case of positive roots.</p>
<p>In the opinion of the 18th century British mathematician <a href="/wiki/Charles_Hutton" title="Charles Hutton">Charles Hutton</a>, as quoted in (<a href="#CITEREFFunkhouser">Funkhouser</a>), the general principle (not only for positive real roots) was first understood by the 17th century French mathematician <a href="/wiki/Albert_Girard" title="Albert Girard">Albert Girard</a>; Hutton writes:</p>
<blockquote>
<p>...[Girard was] the first person who understood the general doctrine of the formation of the coefficients of the powers from the sum of the roots and their products. He was the first who discovered the rules for summing the powers of the roots of any equation.</p>
</blockquote>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=7" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Newton%27s_identities" title="Newton's identities">Newton's identities</a></li>
<li><a href="/wiki/Elementary_symmetric_polynomial" title="Elementary symmetric polynomial">Elementary symmetric polynomial</a></li>
<li><a href="/wiki/Symmetric_polynomial" title="Symmetric polynomial">Symmetric polynomial</a></li>
<li><a href="/wiki/Content_(algebra)" title="Content (algebra)">Content (algebra)</a></li>
<li><a href="/wiki/Properties_of_polynomial_roots" title="Properties of polynomial roots">Properties of polynomial roots</a></li>
<li><a href="/wiki/Gauss%E2%80%93Lucas_theorem" title="GaussLucas theorem">GaussLucas theorem</a></li>
<li><a href="/wiki/Rational_root_theorem" title="Rational root theorem">Rational root theorem</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Vieta%27s_formulas&amp;action=&amp;section=8" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li><span class="citation" id="CITEREFFunkhouser1930">Funkhouser, H. Gray (1930), "A short account of the history of symmetric functions of roots of equations", <i>American Mathematical Monthly</i> (Mathematical Association of America) <b>37</b> (7): 357365, <a href="/wiki/Digital_object_identifier" title="Digital object identifier">doi</a>:<a rel="nofollow" class="external text" href="http://dx.doi.org/10.2307%2F2299273">10.2307/2299273</a>, <a href="/wiki/JSTOR" title="JSTOR">JSTOR</a>&#160;<a rel="nofollow" class="external text" href="http://www.jstor.org/stable/2299273">2299273</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=A+short+account+of+the+history+of+symmetric+functions+of+roots+of+equations&amp;rft.jtitle=American+Mathematical+Monthly&amp;rft.aulast=Funkhouser&amp;rft.aufirst=H.+Gray&amp;rft.au=Funkhouser%2C%26%2332%3BH.+Gray&amp;rft.date=1930&amp;rft.volume=37&amp;rft.issue=7&amp;rft.pages=357%E2%80%93365&amp;rft.pub=Mathematical+Association+of+America&amp;rft_id=info:doi/10.2307%2F2299273&amp;rft.jstor=2299273&amp;rfr_id=info:sid/en.wikipedia.org:Vieta%27s_formulas"><span style="display: none;">&#160;</span></span></li>
</ul>
<ul>
<li><span class="citation" id="CITEREFVinberg2003"><a href="/wiki/Ernest_Vinberg" title="Ernest Vinberg">Vinberg, E. B.</a> (2003), <i>A course in algebra</i>, American Mathematical Society, Providence, R.I, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/0821834134" title="Special:BookSources/0821834134">0821834134</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=A+course+in+algebra&amp;rft.aulast=Vinberg&amp;rft.aufirst=E.+B.&amp;rft.au=Vinberg%2C%26%2332%3BE.+B.&amp;rft.date=2003&amp;rft.pub=American+Mathematical+Society%2C+Providence%2C+R.I&amp;rft.isbn=0821834134&amp;rfr_id=info:sid/en.wikipedia.org:Vieta%27s_formulas"><span style="display: none;">&#160;</span></span></li>
</ul>
<ul>
<li><span class="citation" id="CITEREFDjuki.C4.872006">Djuki, Duan, et al. (2006), <i>The IMO compendium: a collection of problems suggested for the International Mathematical Olympiads, 1959-2004</i>, Springer, New York, NY, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/0387242996" title="Special:BookSources/0387242996">0387242996</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=The+IMO+compendium%3A+a+collection+of+problems+suggested+for+the+International+Mathematical+Olympiads%2C+1959-2004&amp;rft.aulast=Djuki%C4%87&amp;rft.aufirst=Du%C5%A1an%2C+et+al.&amp;rft.au=Djuki%C4%87%2C%26%2332%3BDu%C5%A1an%2C+et+al.&amp;rft.date=2006&amp;rft.pub=Springer%2C+New+York%2C+NY&amp;rft.isbn=0387242996&amp;rfr_id=info:sid/en.wikipedia.org:Vieta%27s_formulas"><span style="display: none;">&#160;</span></span></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 2555/1000000
Post-expand include size: 16541/2048000 bytes
Template argument size: 3721/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:714050-0!0!0!!en!*!* and timestamp 20120311215857 -->
