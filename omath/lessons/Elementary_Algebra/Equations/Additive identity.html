<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a> the <b>additive identity</b> of a <a href="/wiki/Set_(mathematics)" title="Set (mathematics)">set</a> which is equipped with the <a href="/wiki/Operation_(mathematics)" title="Operation (mathematics)">operation</a> of <a href="/wiki/Addition" title="Addition">addition</a> is an <a href="/wiki/Element_(mathematics)" title="Element (mathematics)">element</a> which, when added to any element <i>x</i> in the set, yields <i>x</i>. One of the most familiar additive identities is the <a href="/wiki/Number" title="Number">number</a> <a href="/wiki/0_(number)" title="0 (number)">0</a> from <a href="/wiki/Elementary_mathematics" title="Elementary mathematics">elementary mathematics</a>, but additive identities occur in other mathematical structures where addition is defined, such as in <a href="/wiki/Group_(mathematics)" title="Group (mathematics)">groups</a> and <a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">rings</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Elementary_examples"><span class="tocnumber">1</span> <span class="toctext">Elementary examples</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Formal_definition"><span class="tocnumber">2</span> <span class="toctext">Formal definition</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Further_examples"><span class="tocnumber">3</span> <span class="toctext">Further examples</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Proofs"><span class="tocnumber">4</span> <span class="toctext">Proofs</span></a>
<ul>
<li class="toclevel-2 tocsection-5"><a href="#The_additive_identity_is_unique_in_a_group"><span class="tocnumber">4.1</span> <span class="toctext">The additive identity is unique in a group</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#The_additive_identity_annihilates_ring_elements"><span class="tocnumber">4.2</span> <span class="toctext">The additive identity annihilates ring elements</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#The_additive_and_multiplicative_identities_are_different_in_a_non-trivial_ring"><span class="tocnumber">4.3</span> <span class="toctext">The additive and multiplicative identities are different in a non-trivial ring</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-8"><a href="#See_also"><span class="tocnumber">5</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#References"><span class="tocnumber">6</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#External_links"><span class="tocnumber">7</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=1" title="Edit section: Elementary examples"></a>]</span> <span class="mw-headline" id="Elementary_examples">Elementary examples</span></h2>
<ul>
<li>The additive identity familiar from <a href="/wiki/Elementary_mathematics" title="Elementary mathematics">elementary mathematics</a> is zero, denoted <a href="/wiki/0_(number)" title="0 (number)">0</a>. For example,
<dl>
<dd>5 + 0 = 5 = 0 + 5.</dd>
</dl>
</li>
<li>In the <a href="/wiki/Natural_number" title="Natural number">natural numbers</a> <b>N</b> and all of its <a href="/wiki/Superset" title="Superset" class="mw-redirect">supersets</a> (the <a href="/wiki/Integer" title="Integer">integers</a> <b>Z</b>, the <a href="/wiki/Rational_number" title="Rational number">rational numbers</a> <b>Q</b>, the <a href="/wiki/Real_number" title="Real number">real numbers</a> <b>R</b>, or the <a href="/wiki/Complex_number" title="Complex number">complex numbers</a> <b>C</b>), the additive identity is 0. Thus for any one of these <a href="/wiki/Number" title="Number">numbers</a> <i>n</i>,
<dl>
<dd><i>n</i> + 0 = <i>n</i> = 0 + <i>n</i>. The additive identity is zero in an addition problem</dd>
</dl>
</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=2" title="Edit section: Formal definition"></a>]</span> <span class="mw-headline" id="Formal_definition">Formal definition</span></h2>
<p>Let <i>N</i> be a <a href="/wiki/Set_(mathematics)" title="Set (mathematics)">set</a> which is closed under the <a href="/wiki/Operation_(mathematics)" title="Operation (mathematics)">operation</a> of <a href="/wiki/Addition" title="Addition">addition</a>, denoted <a href="/wiki/%2B" title="+" class="mw-redirect">+</a>. An additive identity for <i>N</i> is any element <i>e</i> such that for any element <i>n</i> in <i>N</i>,</p>
<dl>
<dd><i>e</i> + <i>n</i> = <i>n</i> = <i>n</i> + <i>e</i>.</dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=3" title="Edit section: Further examples"></a>]</span> <span class="mw-headline" id="Further_examples">Further examples</span></h2>
<ul>
<li>In a <a href="/wiki/Group_(mathematics)" title="Group (mathematics)">group</a> the additive identity is the <a href="/wiki/Identity_element" title="Identity element">identity element</a> of the group, is often denoted 0, and is unique (see below for proof).</li>
<li>A <a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">ring</a> or <a href="/wiki/Field_(mathematics)" title="Field (mathematics)">field</a> is a group under the operation of addition and thus these also have a unique additive identity 0. This is defined to be different from the <a href="/wiki/Multiplicative_identity" title="Multiplicative identity" class="mw-redirect">multiplicative identity</a> <a href="/wiki/1_(number)" title="1 (number)">1</a> if the ring (or field) has more than one element. If the additive identity and the multiplicative identity are the same, then the ring is <a href="/wiki/Trivial_(mathematics)" title="Trivial (mathematics)" class="mw-redirect">trivial</a> (proved below).</li>
<li>In the ring M<sub><i>m</i><i>n</i></sub>(<i>R</i>) of <i>m</i> by <i>n</i> <a href="/wiki/Matrix_(mathematics)" title="Matrix (mathematics)">matrices</a> over a ring <i>R</i>, the additive identity is denoted <b>0</b> and is the <i>m</i> by <i>n</i> matrix whose entries consist entirely of the identity element 0 in <i>R</i>. For example, in the 2 by 2 matrices over the integers M<sub>2</sub>(<b>Z</b>) the additive identity is
<dl>
<dd><img class="tex" alt="0 = \begin{pmatrix}0 &amp; 0 \\ 0 &amp; 0\end{pmatrix}." src="//upload.wikimedia.org/wikipedia/en/math/1/3/2/132dd618df267568c51f3b237543ce80.png" /></dd>
</dl>
</li>
<li>In the <a href="/wiki/Quaternions" title="Quaternions" class="mw-redirect">quaternions</a>, 0 is the additive identity.</li>
<li>In the ring of <a href="/wiki/Function_(mathematics)" title="Function (mathematics)">functions</a> from <b>R</b> to <b>R</b>, the function <a href="/wiki/Map_(mathematics)" title="Map (mathematics)">mapping</a> every number to 0 is the additive identity.</li>
<li>In the <a href="/wiki/Additive_group" title="Additive group">additive group</a> of <a href="/wiki/Vector_(geometric)" title="Vector (geometric)" class="mw-redirect">vectors</a> in <b>R</b><sup><i>n</i></sup>, the origin or <a href="/wiki/Zero_vector" title="Zero vector" class="mw-redirect">zero vector</a> is the additive identity.</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=4" title="Edit section: Proofs"></a>]</span> <span class="mw-headline" id="Proofs">Proofs</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=5" title="Edit section: The additive identity is unique in a group"></a>]</span> <span class="mw-headline" id="The_additive_identity_is_unique_in_a_group">The additive identity is unique in a group</span></h3>
<p>Let (<i>G</i>, +) be a group and let 0 and 0' in <i>G</i> both denote additive identities, so for any <i>g</i> in <i>G</i>,</p>
<dl>
<dd>0 + <i>g</i> = <i>g</i> = <i>g</i> + 0 and 0' + <i>g</i> = <i>g</i> = <i>g</i> + 0'.</dd>
</dl>
<p>It follows from the above that</p>
<dl>
<dd>(0') = (0') + 0 = 0' + (0) = (0).</dd>
</dl>
<h3><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=6" title="Edit section: The additive identity annihilates ring elements"></a>]</span> <span class="mw-headline" id="The_additive_identity_annihilates_ring_elements">The additive identity annihilates ring elements</span></h3>
<p>In a system with a multiplication operation that distributes over addition, the additive identity is a multiplicative <a href="/wiki/Absorbing_element" title="Absorbing element">absorbing element</a>, meaning that for any <i>s</i> in <i>S</i>, <i>s</i>0 = 0. This can be seen because:</p>
<dl>
<dd><i>s</i>0 = <i>s</i>(0 + 0) = <i>s</i>0 + <i>s</i>0  <i>s</i>0 = 0 (by cancellation).</dd>
</dl>
<h3><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=7" title="Edit section: The additive and multiplicative identities are different in a non-trivial ring"></a>]</span> <span class="mw-headline" id="The_additive_and_multiplicative_identities_are_different_in_a_non-trivial_ring">The additive and multiplicative identities are different in a non-trivial ring</span></h3>
<p>Let <i>R</i> be a ring and suppose that the additive identity 0 and the multiplicative identity 1 are equal, or 0 = 1. Let <i>r</i> be any <a href="/wiki/Element_(mathematics)" title="Element (mathematics)">element</a> of <i>R</i>. Then</p>
<dl>
<dd><i>r</i> = <i>r</i>  1 = <i>r</i>  0 = 0,</dd>
</dl>
<p>proving that <i>R</i> is trivial, that is, <i>R</i> = {0}. The <a href="/wiki/Contrapositive" title="Contrapositive" class="mw-redirect">contrapositive</a>, that if <i>R</i> is non-trivial then 0 is not equal to 1, is therefore shown.</p>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=8" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/0_(number)" title="0 (number)">0 (number)</a></li>
<li><a href="/wiki/Additive_inverse" title="Additive inverse">Additive inverse</a></li>
<li><a href="/wiki/Identity_element" title="Identity element">Identity element</a></li>
<li><a href="/wiki/Multiplicative_identity" title="Multiplicative identity" class="mw-redirect">Multiplicative identity</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=9" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li>David S. Dummit, Richard M. Foote, <i>Abstract Algebra</i>, Wiley (3d ed.): 2003, <a href="/wiki/Special:BookSources/0471433349" class="internal mw-magiclink-isbn">ISBN 0-471-43334-9</a>.</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Additive_identity&amp;action=&amp;section=10" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://planetmath.org/encyclopedia/UniquenessOfAdditiveIdentityInARing2.html">uniqueness of additive identity in a ring</a> at <a href="/wiki/PlanetMath" title="PlanetMath">PlanetMath</a></li>
<li><span class="citation mathworld" id="Reference-Mathworld-Additive_Identity">Margherita Barile, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/AdditiveIdentity.html">Additive Identity</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 73/1000000
Post-expand include size: 332/2048000 bytes
Template argument size: 144/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:4178225-0!0!0!!en!*!* and timestamp 20120414043240 -->
