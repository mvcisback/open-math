<div class="dablink">For other uses, see <a href="/wiki/Identity_(disambiguation)" title="Identity (disambiguation)" class="mw-redirect">Identity (disambiguation)</a>.</div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, the term <b>identity</b> has several different important meanings:</p>
<ul>
<li>An <b>identity</b> is a relation which is <a href="/wiki/Tautology_(logic)" title="Tautology (logic)">tautologically</a> true. This means that whatever the number or value may be, the answer stays the same. For example, algebraically, this occurs if an equation is satisfied for all values of the involved variables. Definitions are often indicated by the '<a href="/wiki/Triple_bar" title="Triple bar">triple bar</a>' symbol , such as <i>A<sup>2</sup>  xx</i>. The symbol  can also be used with other meanings, but these can usually be interpreted in some way as a definition, or something which is otherwise tautologically true (for example, a <a href="/wiki/Congruence_relation" title="Congruence relation">congruence relation</a>).</li>
</ul>
<ul>
<li>In <a href="/wiki/Algebra" title="Algebra">algebra</a>, an <b>identity</b> or <b><a href="/wiki/Identity_element" title="Identity element">identity element</a></b> of a set <i>S</i> with a <a href="/wiki/Binary_operation" title="Binary operation">binary operation</a>  is an element <i>e</i> that, when combined with any element <i>x</i> of <i>S</i>, produces that same <i>x</i>. That is, <span class="nowrap"><i>e</i><i>x</i> = <i>x</i><i>e</i> = <i>x</i></span> for all <i>x</i> in <i>S</i>. An example of this is the <b><a href="/wiki/Identity_matrix" title="Identity matrix">identity matrix</a></b>.</li>
</ul>
<ul>
<li>The <b><a href="/wiki/Identity_function" title="Identity function">identity function</a></b> from a set <i>S</i> to itself, often denoted <img class="tex" alt="\mathrm{id}" src="//upload.wikimedia.org/wikipedia/en/math/6/3/f/63fb79355b4d187e39f98fa3629b532f.png" /> or <img class="tex" alt="\mathrm{id}_S" src="//upload.wikimedia.org/wikipedia/en/math/a/c/d/acd5e5109a5c5bc883e80c87bc637ea6.png" />, is the function which maps every element to itself. In other words, <img class="tex" alt="\mathrm{id}(x) = x" src="//upload.wikimedia.org/wikipedia/en/math/5/4/c/54c78b1a4065208a4bf44b474f5762b7.png" /> for all <i>x</i> in <i>S</i>. This function serves as the identity element in the set of all functions from <i>S</i> to itself with respect to <a href="/wiki/Function_composition" title="Function composition">function composition</a>.</li>
</ul>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Examples"><span class="tocnumber">1</span> <span class="toctext">Examples</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Identity_element"><span class="tocnumber">1.1</span> <span class="toctext">Identity element</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Identity_function"><span class="tocnumber">1.2</span> <span class="toctext">Identity function</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Comparison"><span class="tocnumber">2</span> <span class="toctext">Comparison</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#External_links"><span class="tocnumber">3</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Identity_(mathematics)&amp;action=&amp;section=1" title="Edit section: Examples"></a>]</span> <span class="mw-headline" id="Examples">Examples</span></h2>
<p>A common example of the first meaning is the <a href="/wiki/Trigonometric_identity" title="Trigonometric identity" class="mw-redirect">trigonometric identity</a></p>
<dl>
<dd><img class="tex" alt=" \sin ^2 \theta +  \cos ^2 \theta \equiv 1\," src="//upload.wikimedia.org/wikipedia/en/math/f/e/a/feaa3dab7b0924ff9402cb141ee76059.png" /></dd>
</dl>
<p>which is true for all <a href="/wiki/Complex_number" title="Complex number">complex</a> values of <img class="tex" alt="\theta" src="//upload.wikimedia.org/wikipedia/en/math/5/0/d/50d91f80cbb8feda1d10e167107ad1ff.png" /> (since the complex numbers <img class="tex" alt="\Bbb{C}" src="//upload.wikimedia.org/wikipedia/en/math/8/4/f/84feda6433ec704f8ff2098173f9413f.png" /> are the domain of sin and cos), as opposed to</p>
<dl>
<dd><img class="tex" alt="\cos \theta = 1,\," src="//upload.wikimedia.org/wikipedia/en/math/6/9/9/6995465439f1ba36f301e9b6be095134.png" /></dd>
</dl>
<p>which is true only for some values of <img class="tex" alt="\theta" src="//upload.wikimedia.org/wikipedia/en/math/5/0/d/50d91f80cbb8feda1d10e167107ad1ff.png" />, not all. For example, the latter equation is true when <img class="tex" alt=" \theta = 0,\," src="//upload.wikimedia.org/wikipedia/en/math/6/f/1/6f1029dfa89c9baa0f817164cd5457c2.png" /> false when <img class="tex" alt="\theta = 2\," src="//upload.wikimedia.org/wikipedia/en/math/d/e/7/de77003a70a96b1fd693298759f92b1a.png" />.</p>
<p>See also <a href="/wiki/List_of_mathematical_identities" title="List of mathematical identities">list of mathematical identities</a>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Identity_(mathematics)&amp;action=&amp;section=2" title="Edit section: Identity element"></a>]</span> <span class="mw-headline" id="Identity_element">Identity element</span></h3>
<p>The concepts of "additive identity" and "multiplicative identity" are central to the <a href="/wiki/Peano_axioms" title="Peano axioms">Peano axioms</a>. The number <b>0</b> is the "additive identity" for integers, real numbers, and complex numbers. For the real numbers, for all <img class="tex" alt="a\in\Bbb{R}," src="//upload.wikimedia.org/wikipedia/en/math/5/c/5/5c5e0baa0e01aa5ca742fc3cbcea1c88.png" /></p>
<dl>
<dd><img class="tex" alt="0 + a = a,\," src="//upload.wikimedia.org/wikipedia/en/math/4/8/8/488760abac78e85e2fdb2cb197c1dad6.png" /></dd>
</dl>
<dl>
<dd><img class="tex" alt="a + 0 = a,\," src="//upload.wikimedia.org/wikipedia/en/math/8/2/0/8202ee00789292df1def075869d20508.png" /> and</dd>
</dl>
<dl>
<dd><img class="tex" alt="0 + 0 = 0.\," src="//upload.wikimedia.org/wikipedia/en/math/a/d/b/adb8a98fb414e583a79f570d63c34cdb.png" /></dd>
</dl>
<p>Similarly, The number <b>1</b> is the "multiplicative identity" for integers, real numbers, and complex numbers. For the real numbers, for all <img class="tex" alt="a\in\Bbb{R}," src="//upload.wikimedia.org/wikipedia/en/math/5/c/5/5c5e0baa0e01aa5ca742fc3cbcea1c88.png" /></p>
<dl>
<dd><img class="tex" alt="1 \times a = a,\," src="//upload.wikimedia.org/wikipedia/en/math/2/b/b/2bb809bd4f9e7df98d144c77caec4a6b.png" /></dd>
</dl>
<dl>
<dd><img class="tex" alt="a \times 1 = a,\," src="//upload.wikimedia.org/wikipedia/en/math/1/9/6/196ab27cf556e3d8343032dc416e9af8.png" /> and</dd>
</dl>
<dl>
<dd><img class="tex" alt="1 \times 1 = 1.\," src="//upload.wikimedia.org/wikipedia/en/math/7/d/d/7dd039365c2dd59f023188fe80f2416a.png" /></dd>
</dl>
<h3><span class="section">[<a href="/w/index.php?title=Identity_(mathematics)&amp;action=&amp;section=3" title="Edit section: Identity function"></a>]</span> <span class="mw-headline" id="Identity_function">Identity function</span></h3>
<p>A common example of an identity function is the identity <a href="/wiki/Permutation" title="Permutation">permutation</a>, which sends each element of the set <img class="tex" alt="\{ 1, 2, \ldots, n \}" src="//upload.wikimedia.org/wikipedia/en/math/a/3/e/a3ebf794f28dbeec6babca08f9d4ba38.png" /> to itself or <img class="tex" alt="\{a_1,a_2, \ldots, a_n \}" src="//upload.wikimedia.org/wikipedia/en/math/8/6/4/86409ad98d42def41b424f22e814da2c.png" /> to itself in natural order.</p>
<h2><span class="section">[<a href="/w/index.php?title=Identity_(mathematics)&amp;action=&amp;section=4" title="Edit section: Comparison"></a>]</span> <span class="mw-headline" id="Comparison">Comparison</span></h2>
<p>These meanings are not mutually exclusive; for instance, the identity permutation is the identity element in the <a href="/wiki/Group_(mathematics)" title="Group (mathematics)">group</a> of permutations of <img class="tex" alt="\{ 1, 2, \ldots, n \}" src="//upload.wikimedia.org/wikipedia/en/math/a/3/e/a3ebf794f28dbeec6babca08f9d4ba38.png" /> under <a href="/wiki/Function_composition" title="Function composition">composition</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Identity_(mathematics)&amp;action=&amp;section=5" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://sites.google.com/site/tpiezas/Home/">A Collection of Algebraic Identities</a><a href="//en.wikibooks.org/wiki/%D0%A2%D1%8A%D0%B6%D0%B4%D0%B5%D1%81%D1%82%D0%B2%D0%BE" class="extiw" title="b:">b:</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 250/1000000
Post-expand include size: 333/2048000 bytes
Template argument size: 144/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:298428-0!0!0!!en!*!* and timestamp 20120417093404 -->
