<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, a <a href="/wiki/Binary_relation" title="Binary relation">binary relation</a> <i>R</i> over a <a href="/wiki/Set_(mathematics)" title="Set (mathematics)">set</a> <i>X</i> is <b>transitive</b> if whenever an element <i>a</i> is related to an element <i>b</i>, and <i>b</i> is in turn related to an element <i>c</i>, then <i>a</i> is also related to <i>c</i>.</p>
<p>In mathematical syntax: <img class="tex" alt="\forall a,b,c\in X:\left(aRb\wedge bRc\right)\implies aRc" src="//upload.wikimedia.org/wikipedia/en/math/2/4/8/248739bcccd120469e4be8a0c3f3971f.png" /></p>
<p>Transitivity is a key property of both <a href="/wiki/Partial_order" title="Partial order" class="mw-redirect">partial order</a> relations and <a href="/wiki/Equivalence_relation" title="Equivalence relation">equivalence relations</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Examples"><span class="tocnumber">1</span> <span class="toctext">Examples</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Closure_properties"><span class="tocnumber">2</span> <span class="toctext">Closure properties</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Other_properties_that_require_transitivity"><span class="tocnumber">3</span> <span class="toctext">Other properties that require transitivity</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Counting_transitive_relations"><span class="tocnumber">4</span> <span class="toctext">Counting transitive relations</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Examples_2"><span class="tocnumber">5</span> <span class="toctext">Examples</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#See_also"><span class="tocnumber">6</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Notes"><span class="tocnumber">7</span> <span class="toctext">Notes</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#References"><span class="tocnumber">8</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#External_links"><span class="tocnumber">9</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=1" title="Edit section: Examples"></a>]</span> <span class="mw-headline" id="Examples">Examples</span></h2>
<p>For example, "is greater than," "is at least as great as," and "is equal to" (<a href="/wiki/Equality_(mathematics)" title="Equality (mathematics)">equality</a>) are transitive relations:</p>
<dl>
<dd>whenever A &gt; B and B &gt; C, then also A &gt; C</dd>
<dd>whenever A  B and B  C, then also A  C</dd>
<dd>whenever A = B and B = C, then also A = C.</dd>
</dl>
<p>On the other hand, "is the mother of" is not a transitive relation, because if Alice is the mother of Brenda, and Brenda is the mother of Claire, then Alice is not the mother of Claire. What is more, it is <a href="/wiki/Antitransitive" title="Antitransitive" class="mw-redirect">antitransitive</a>: Alice can <i>never</i> be the mother of Claire.</p>
<p>Then again, in biology we often need to consider motherhood over an arbitrary number of generations: the relation "is a <a href="/wiki/Matrilinear" title="Matrilinear" class="mw-redirect">matrilinear</a> ancestor of". This <i>is</i> a transitive relation. More precisely, it is the <a href="/wiki/Transitive_closure" title="Transitive closure">transitive closure</a> of the relation "is the mother of".</p>
<p>More examples of transitive relations:</p>
<ul>
<li>"is a <a href="/wiki/Subset" title="Subset">subset</a> of" (<a href="/wiki/Set_inclusion" title="Set inclusion" class="mw-redirect">set inclusion</a>)</li>
<li>"divides" (<a href="/wiki/Divisor" title="Divisor">divisibility</a>)</li>
<li>"implies" (<a href="/wiki/Logical_implication" title="Logical implication" class="mw-redirect">implication</a>)</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=2" title="Edit section: Closure properties"></a>]</span> <span class="mw-headline" id="Closure_properties">Closure properties</span></h2>
<p>The converse of a transitive relation is always transitive: e.g. knowing that "is a <a href="/wiki/Subset" title="Subset">subset</a> of" is transitive and "is a <a href="/wiki/Superset" title="Superset" class="mw-redirect">superset</a> of" is its converse, we can conclude that the latter is transitive as well.</p>
<p>The intersection of two transitive relations is always transitive: knowing that "was born before" and "has the same first name as" are transitive, we can conclude that "was born before and also has the same first name as" is also transitive.</p>
<p>The union of two transitive relations is not always transitive. For instance "was born before or has the same first name as" is not generally a transitive relation.</p>
<p>The complement of a transitive relation is not always transitive. For instance, while "equal to" is transitive, "not equal to" is only transitive on sets with at most one element.</p>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=3" title="Edit section: Other properties that require transitivity"></a>]</span> <span class="mw-headline" id="Other_properties_that_require_transitivity">Other properties that require transitivity</span></h2>
<ul>
<li><a href="/wiki/Preorder" title="Preorder">Preorder</a>  a <a href="/wiki/Reflexive_relation" title="Reflexive relation">reflexive</a> transitive relation</li>
<li><a href="/wiki/Partially_ordered_set" title="Partially ordered set">partial order</a>  an <a href="/wiki/Antisymmetric_relation" title="Antisymmetric relation">antisymmetric</a> preorder</li>
<li><a href="/wiki/Total_preorder" title="Total preorder" class="mw-redirect">Total preorder</a>  a <a href="/wiki/Total_relation" title="Total relation">total</a> preorder</li>
<li><a href="/wiki/Equivalence_relation" title="Equivalence relation">Equivalence relation</a>  a <a href="/wiki/Symmetric_relation" title="Symmetric relation">symmetric</a> preorder</li>
<li><a href="/wiki/Strict_weak_ordering" title="Strict weak ordering">Strict weak ordering</a>  a strict partial order in which incomparability is an equivalence relation</li>
<li><a href="/wiki/Total_ordering" title="Total ordering" class="mw-redirect">Total ordering</a>  a <a href="/wiki/Total_relation" title="Total relation">total</a>, <a href="/wiki/Antisymmetric_relation" title="Antisymmetric relation">antisymmetric</a> transitive relation</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=4" title="Edit section: Counting transitive relations"></a>]</span> <span class="mw-headline" id="Counting_transitive_relations">Counting transitive relations</span></h2>
<p>Unlike other relation properties, no general formula that counts the number of transitive relations on a finite set (sequence <span class="nowrap"><a href="//oeis.org/A006905" class="extiw" title="oeis:A006905">A006905</a></span> in <a href="/wiki/On-Line_Encyclopedia_of_Integer_Sequences" title="On-Line Encyclopedia of Integer Sequences">OEIS</a>) is known.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup> However, there is a formula for finding the number of relations that are simultaneously reflexive, symmetric, and transitive  in other words, <a href="/wiki/Equivalence_relation" title="Equivalence relation">equivalence relations</a>  (sequence <span class="nowrap"><a href="//oeis.org/A000110" class="extiw" title="oeis:A000110">A000110</a></span> in <a href="/wiki/On-Line_Encyclopedia_of_Integer_Sequences" title="On-Line Encyclopedia of Integer Sequences">OEIS</a>), those that are symmetric and transitive, those that are symmetric, transitive, and antisymmetric, and those that are total, transitive, and antisymmetric. Pfeiffer<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup> has made some progress in this direction, expressing relations with combinations of these properties in terms of each other, but still calculating any one is difficult. See also.<sup id="cite_ref-2" class="reference"><a href="#cite_note-2"><span>[</span>3<span>]</span></a></sup></p>
<table class="wikitable">
<tr>
<th colspan="9">Number of <i>n</i>-element binary relations of different types</th>
</tr>
<tr>
<th><i>n</i></th>
<th><a href="/wiki/Binary_relation" title="Binary relation">all</a></th>
<th><strong class="selflink">transitive</strong></th>
<th><a href="/wiki/Reflexive_relation" title="Reflexive relation">reflexive</a></th>
<th><a href="/wiki/Preorder" title="Preorder">preorder</a></th>
<th><a href="/wiki/Partially_ordered_set" title="Partially ordered set">partial order</a></th>
<th><a href="/wiki/Strict_weak_ordering" title="Strict weak ordering">total preorder</a></th>
<th><a href="/wiki/Total_order" title="Total order">total order</a></th>
<th><a href="/wiki/Equivalence_relation" title="Equivalence relation">equivalence relation</a></th>
</tr>
<tr>
<td>0</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
</tr>
<tr>
<td>1</td>
<td>2</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
</tr>
<tr>
<td>2</td>
<td>16</td>
<td>13</td>
<td>4</td>
<td>4</td>
<td>3</td>
<td>3</td>
<td>2</td>
<td>2</td>
</tr>
<tr>
<td>3</td>
<td>512</td>
<td>171</td>
<td>64</td>
<td>29</td>
<td>19</td>
<td>13</td>
<td>6</td>
<td>5</td>
</tr>
<tr>
<td>4</td>
<td>65536</td>
<td>3994</td>
<td>4096</td>
<td>355</td>
<td>219</td>
<td>75</td>
<td>24</td>
<td>15</td>
</tr>
<tr>
<td><a href="/wiki/OEIS" title="OEIS" class="mw-redirect">OEIS</a></td>
<td><a href="//oeis.org/A002416" class="extiw" title="oeis:A002416">A002416</a></td>
<td><a href="//oeis.org/A006905" class="extiw" title="oeis:A006905">A006905</a></td>
<td><a href="//oeis.org/A053763" class="extiw" title="oeis:A053763">A053763</a></td>
<td><a href="//oeis.org/A000798" class="extiw" title="oeis:A000798">A000798</a></td>
<td><a href="//oeis.org/A001035" class="extiw" title="oeis:A001035">A001035</a></td>
<td><a href="//oeis.org/A000670" class="extiw" title="oeis:A000670">A000670</a></td>
<td><a href="//oeis.org/A000142" class="extiw" title="oeis:A000142">A000142</a></td>
<td><a href="//oeis.org/A000110" class="extiw" title="oeis:A000110">A000110</a></td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=5" title="Edit section: Examples"></a>]</span> <span class="mw-headline" id="Examples_2">Examples</span></h2>
<ul>
<li><a href="/wiki/Equality_(mathematics)" title="Equality (mathematics)">equality</a></li>
<li>less/greater than (or equal)</li>
<li>inclusion of sets</li>
<li>inequality is not transitive</li>
<li>being an ancestor of is transitive</li>
<li>being a parent of is not transitive</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=6" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Transitive_closure" title="Transitive closure">Transitive closure</a></li>
<li><a href="/wiki/Transitive_reduction" title="Transitive reduction">Transitive reduction</a></li>
<li><a href="/wiki/Intransitivity" title="Intransitivity">Intransitivity</a></li>
<li><a href="/wiki/Reflexive_relation" title="Reflexive relation">Reflexive relation</a></li>
<li><a href="/wiki/Symmetric_relation" title="Symmetric relation">Symmetric relation</a></li>
<li><a href="/wiki/Quasitransitive_relation" title="Quasitransitive relation">Quasitransitive relation</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=7" title="Edit section: Notes"></a>]</span> <span class="mw-headline" id="Notes">Notes</span></h2>
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">Steven R. Finch, <a rel="nofollow" class="external text" href="http://algo.inria.fr/csolve/posets.pdf">"Transitive relations, topologies and partial orders"</a>, 2003.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text">Gtz Pfeiffer, "<a rel="nofollow" class="external text" href="http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Pfeiffer/pfeiffer6.html">Counting Transitive Relations</a>", <i>Journal of Integer Sequences</i>, Vol. 7 (2004), Article 04.3.2.</span></li>
<li id="cite_note-2"><b><a href="#cite_ref-2">^</a></b> <span class="reference-text">Gunnar Brinkmann and Brendan D. McKay,"<a rel="nofollow" class="external text" href="http://cs.anu.edu.au/~bdm/papers/topologies.pdf">Counting unlabelled topologies and transitive relations</a>"</span></li>
</ol>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=8" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<table class="metadata plainlinks ambox ambox-style ambox-More_footnotes" style="">
<tr>
<td class="mbox-image">
<div style="width: 52px;"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Text_document_with_red_question_mark.svg/40px-Text_document_with_red_question_mark.svg.png" width="40" height="40" /></div>
</td>
<td class="mbox-text" style="">This article includes a <a href="/wiki/Wikipedia:Citing_sources" title="Wikipedia:Citing sources">list of references</a>, but <b>its sources remain unclear because it has insufficient <a href="/wiki/Wikipedia:INCITE" title="Wikipedia:INCITE" class="mw-redirect">inline citations</a></b>. Please help to <a href="/wiki/Wikipedia:WikiProject_Fact_and_Reference_Check" title="Wikipedia:WikiProject Fact and Reference Check">improve</a> this article by <a href="/wiki/Wikipedia:When_to_cite" title="Wikipedia:When to cite">introducing</a> more precise citations. <small><i>(November 2010)</i></small></td>
</tr>
</table>
<ul>
<li><i>Discrete and Combinatorial Mathematics</i> - Fifth Edition - by <a href="/wiki/Ralph_Grimaldi" title="Ralph Grimaldi">Ralph P. Grimaldi</a> <a href="/wiki/Special:BookSources/0201199122" class="internal mw-magiclink-isbn">ISBN 0-201-19912-2</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Transitive_relation&amp;action=&amp;section=9" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.cut-the-knot.org/triangle/remarkable.shtml">Transitivity in Action</a> at <a href="/wiki/Cut-the-knot" title="Cut-the-knot" class="mw-redirect">cut-the-knot</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 595/1000000
Post-expand include size: 5226/2048000 bytes
Template argument size: 1637/2048000 bytes
Expensive parser function count: 1/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:200463-0!0!0!!en!4!* and timestamp 20120410235615 -->
