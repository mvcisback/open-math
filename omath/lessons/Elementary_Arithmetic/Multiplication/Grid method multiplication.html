<p>In <a href="/wiki/Mathematics_education" title="Mathematics education">mathematics education</a> at the level of <a href="/wiki/Primary_school" title="Primary school">primary school</a> or <a href="/wiki/Elementary_school" title="Elementary school">elementary school</a>, the <b>grid method</b> (also known as the <b>box method</b>) of multiplication is an introductory approach to multi-digit multiplication calculations, i.e. multiplications involving numbers larger than ten.</p>
<p>Compared to traditional <a href="/wiki/Long_multiplication" title="Long multiplication" class="mw-redirect">long multiplication</a>, the grid method differs in clearly breaking the multiplication and addition into two steps, and in being less dependent on place value.</p>
<p>Whilst less <i>efficient</i> than the traditional method, grid multiplication is considered to be more <i>reliable</i>, in that children are less likely to make mistakes. Most pupils will go on to learn the traditional method, once they are comfortable with the grid method; but knowledge of the grid method remains a useful "fall back", in the event of confusion. It is also argued that since anyone doing a lot of multiplication would nowadays use a pocket calculator, efficiency for its own sake is less important; equally, since this means that most children will use the multiplication algorithm less often, it is useful for them to become familiar with a more explicit (and hence more memorable) method.</p>
<p>Use of the grid method has been standard in mathematics education in primary schools in England and Wales since the introduction of a <a href="/wiki/National_Numeracy_Strategy" title="National Numeracy Strategy">National Numeracy Strategy</a> with its "numeracy hour" in the 1990s. It can also be found included in various curricula elsewhere. Essentially the same calculation approach, but not necessarily with the explicit grid arrangement, is also known as the <b>partial products algorithm</b> or <b>partial products method</b>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Calculations"><span class="tocnumber">1</span> <span class="toctext">Calculations</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Introductory_motivation"><span class="tocnumber">1.1</span> <span class="toctext">Introductory motivation</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Standard_blocks"><span class="tocnumber">1.2</span> <span class="toctext">Standard blocks</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Larger_numbers"><span class="tocnumber">1.3</span> <span class="toctext">Larger numbers</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Other_applications"><span class="tocnumber">2</span> <span class="toctext">Other applications</span></a>
<ul>
<li class="toclevel-2 tocsection-6"><a href="#Fractions"><span class="tocnumber">2.1</span> <span class="toctext">Fractions</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#Algebra"><span class="tocnumber">2.2</span> <span class="toctext">Algebra</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-8"><a href="#Mathematics"><span class="tocnumber">3</span> <span class="toctext">Mathematics</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#See_also"><span class="tocnumber">4</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-11"><a href="#External_links"><span class="tocnumber">6</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=1" title="Edit section: Calculations"></a>]</span> <span class="mw-headline" id="Calculations">Calculations</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=2" title="Edit section: Introductory motivation"></a>]</span> <span class="mw-headline" id="Introductory_motivation">Introductory motivation</span></h3>
<p>The grid method can be introduced by thinking about how to add up the number of points in a regular array, for example the number of squares of chocolate in a chocolate bar. As the size of the calculation becomes larger, it becomes easier to start counting in tens; and to represent the calculation as a box which can be sub-divided, rather than drawing lots and lots of dots. <sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup><sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup></p>
<p>At the simplest level, pupils might be asked to apply the method to a calculation like 3  17. Breaking up ("partitioning") the 17 as (10 + 7), this unfamiliar multiplication can be worked out as the sum of two simple multiplications:</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="40pt">10</th>
<th scope="col" width="40pt">7</th>
</tr>
<tr>
<th scope="row">3</th>
<td>30</td>
<td>21</td>
</tr>
</table>
</dd>
</dl>
<p>so 3  17 = 30 + 21 = 51.</p>
<p>This is the "grid" or "boxes" structure which gives the multiplication method its name.</p>
<p>Faced with a slightly larger multiplication, such as 34  13, pupils may initially be encouraged to also break this into tens. So, expanding 34 as 10 + 10 + 10 + 4 and 13 as 10 + 3, the product 34  13 might be represented:</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="40pt">10</th>
<th scope="col" width="40pt">10</th>
<th scope="col" width="40pt">10</th>
<th scope="col" width="40pt">4</th>
</tr>
<tr>
<th scope="row">10</th>
<td>100</td>
<td>100</td>
<td>100</td>
<td>40</td>
</tr>
<tr>
<th scope="row">3</th>
<td>30</td>
<td>30</td>
<td>30</td>
<td>12</td>
</tr>
</table>
</dd>
</dl>
<p>Totalling the contents of each row, it is apparent that the final result of the calculation is (100 + 100 + 100 + 40) + (30 + 30 + 30 + 12) = 340 + 102 = 442.</p>
<h3><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=3" title="Edit section: Standard blocks"></a>]</span> <span class="mw-headline" id="Standard_blocks">Standard blocks</span></h3>
<p>Once pupils have become comfortable with the idea of splitting the whole product into contributions from separate boxes, it is a natural step to group the tens together, so that the calculation 34  13 becomes</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="120pt">30</th>
<th scope="col" width="40pt">4</th>
</tr>
<tr>
<th scope="row">10</th>
<td>300</td>
<td>40</td>
</tr>
<tr>
<th scope="row">3</th>
<td>90</td>
<td>12</td>
</tr>
</table>
</dd>
</dl>
<p>giving the addition</p>
<dl>
<dd>
<dl>
<dd>
<table>
<tr>
<td>
<pre>
  300
   40
   90
 + 12
 ----
  442
</pre></td>
</tr>
</table>
</dd>
</dl>
</dd>
</dl>
<p>so 34  13 = 442.</p>
<p>This is the most usual form for a grid calculation. In countries such as the U.K. where teaching of the grid method is usual, pupils may spend a considerable period regularly setting out calculations like the above, until the method is entirely comfortable and familiar.</p>
<h3><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=4" title="Edit section: Larger numbers"></a>]</span> <span class="mw-headline" id="Larger_numbers">Larger numbers</span></h3>
<p>The grid method extends straightforwardly to calculations involving larger numbers.</p>
<p>For example, to calculate 345  28, the student could construct the grid with six easy multiplications</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="40pt">300</th>
<th scope="col" width="40pt">40</th>
<th scope="col" width="40pt">5</th>
</tr>
<tr>
<th scope="row">20</th>
<td>6000</td>
<td>800</td>
<td>100</td>
</tr>
<tr>
<th scope="row">8</th>
<td>2400</td>
<td>320</td>
<td>40</td>
</tr>
</table>
</dd>
</dl>
<p>to find the answer 6900 + 2760 = 9660.</p>
<p>However, by this stage (at least in standard current U.K. teaching practice) pupils may be starting to be encouraged to set out such a calculation using the traditional long multiplication form without having to draw up a grid.</p>
<p>Traditional long multiplication can be related to a grid multiplication in which only one of the numbers is broken into tens and units parts to be multiplied separately:</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt"></th>
<th scope="col" width="120pt">&#160;345</th>
</tr>
<tr>
<th scope="row">20</th>
<td>6900</td>
</tr>
<tr>
<th scope="row">8</th>
<td>2760</td>
</tr>
</table>
</dd>
</dl>
<p>The traditional method is ultimately faster and much more compact; but it requires two significantly more difficult multiplications which pupils may at first struggle with. Compared to the grid method, traditional long multiplication may also be more abstract and less manifestly clear, so some pupils find it harder to remember what is to be done at each stage and why. Pupils may therefore be encouraged for quite a period to use the simpler grid method alongside the more efficient traditional long multiplication method, as a check and a fall-back.</p>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=5" title="Edit section: Other applications"></a>]</span> <span class="mw-headline" id="Other_applications">Other applications</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=6" title="Edit section: Fractions"></a>]</span> <span class="mw-headline" id="Fractions">Fractions</span></h3>
<p>While not normally taught as a standard method for <a href="/wiki/Fractions#Multiplication" title="Fractions" class="mw-redirect">multiplying fractions</a>, the grid method can readily be applied to simple cases where it is easier to find a product by breaking it down.</p>
<p>For example, the calculation 2  1 can be set out using the grid method</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="40pt">2</th>
<th scope="col" width="40pt"></th>
</tr>
<tr>
<th scope="row">1</th>
<td>2</td>
<td></td>
</tr>
<tr>
<th scope="row"></th>
<td>1</td>
<td></td>
</tr>
</table>
</dd>
</dl>
<p>to find that the resulting product is 2 +  + 1 +  = 3</p>
<h3><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=7" title="Edit section: Algebra"></a>]</span> <span class="mw-headline" id="Algebra">Algebra</span></h3>
<p>The grid method can also be used to illustrate the multiplying out of a product of <a href="/wiki/Binomial" title="Binomial">binomials</a>, such as (<i>a</i> + 3)(<i>b</i> + 2), a standard topic in elementary algebra (although one not usually met until <a href="/wiki/Secondary_school" title="Secondary school">secondary school</a>):</p>
<dl>
<dd>
<table class="wikitable" border="1" cellspacing="0" cellpadding="15" style="text-align: center;">
<tr>
<th scope="col" width="40pt">&#160;</th>
<th scope="col" width="40pt"><i>a</i></th>
<th scope="col" width="40pt">3</th>
</tr>
<tr>
<th scope="row"><i>b</i></th>
<td><i>ab</i></td>
<td>3<i>b</i></td>
</tr>
<tr>
<th scope="row">2</th>
<td>2<i>a</i></td>
<td>6</td>
</tr>
</table>
</dd>
</dl>
<p>Thus (<i>a</i> + 3)(<i>b</i> + 2) = <i>ab</i> + 3<i>b</i> + 2<i>a</i> + 6.</p>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=8" title="Edit section: Mathematics"></a>]</span> <span class="mw-headline" id="Mathematics">Mathematics</span></h2>
<p>Mathematically, the ability to break up a multiplication in this way is known as the <a href="/wiki/Distributive_law" title="Distributive law" class="mw-redirect">distributive law</a>, which can be expressed in algebra as the property that <i>a</i>(<i>b</i>+<i>c</i>) = <i>ab</i> + <i>ac</i>. The grid method uses the distibutive property twice to expand the product, once for the horizontal factor, and once for the vertical factor.</p>
<p>Historically the grid calculation (tweaked slightly) was the basis of a method called <a href="/wiki/Lattice_multiplication" title="Lattice multiplication">lattice multiplication</a>, which was the standard method of multiple-digit multiplication developed in medieval Arabic and Hindu mathematics. Lattice multiplication was introduced into Europe by <a href="/wiki/Fibonacci" title="Fibonacci">Fibonacci</a> at the start of the thirteenth century along with the so-called Arabic numerals themelves; although, like the numerals also, the ways he suggested to calculate with them were initially slow to catch on. <a href="/wiki/Napier%27s_bones" title="Napier's bones">Napier's bones</a> were a calculating help introduced by the Scot <a href="/wiki/John_Napier" title="John Napier">John Napier</a> in 1617 to assist lattice method calculations.</p>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=9" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Multiplication_algorithm" title="Multiplication algorithm">Multiplication algorithm</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=10" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li><a href="/wiki/Rob_Eastaway" title="Rob Eastaway">Rob Eastaway</a> and Mike Askew, <i>Maths for Mums and Dads</i>, Square Peg, 2010. <a href="/wiki/Special:BookSources/9780224086356" class="internal mw-magiclink-isbn">ISBN 9780224086356</a>. pp. 140153.</li>
</ul>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <a rel="nofollow" class="external text" href="http://www.mathsonline.org/pages/boxmult.html?56&amp;551">Long multiplication  The Box method</a></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <a rel="nofollow" class="external text" href="http://www.bbc.co.uk/schools/gcsebitesize/maths/number/multiplicationdivisionrev1.shtml">Long multiplication and division</a></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Grid_method_multiplication&amp;action=&amp;section=11" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.mathsonline.org/pages/boxmult.html?56&amp;551">Long multiplication  The Box method</a>, <i>Maths online</i>.</li>
<li><a rel="nofollow" class="external text" href="http://www.bbc.co.uk/schools/gcsebitesize/maths/number/multiplicationdivisionrev1.shtml">Long multiplication and division</a>, BBC GCSE Bitesize</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 120/1000000
Post-expand include size: 165/2048000 bytes
Template argument size: 0/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:5696420-0!*!0!!en!*!* and timestamp 20120229101838 generated by srv270 -->
