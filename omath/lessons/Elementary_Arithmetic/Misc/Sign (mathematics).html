<div class="thumb tright">
<div class="thumbinner" style="width:152px;"><a href="/wiki/File:PlusMinus.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/c/c5/PlusMinus.svg/150px-PlusMinus.svg.png" width="150" height="103" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:PlusMinus.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The <a href="/wiki/Plus_and_minus_signs" title="Plus and minus signs">plus and minus symbols</a> are used to show the sign of a number.</div>
</div>
</div>
<div class="dablink">Not to be confused with the <a href="/wiki/Sine" title="Sine">sine function</a> in trigonometry.</div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, the word <b>sign</b> refers to the property of being positive or <a href="/wiki/Negative_number" title="Negative number">negative</a>. Every nonzero <a href="/wiki/Real_number" title="Real number">real number</a> is either positive or negative, and therefore has a sign. <a href="/wiki/0_(number)" title="0 (number)">Zero</a> itself is signless, although in some contexts it makes sense to consider a <a href="/wiki/Signed_zero" title="Signed zero">signed zero</a>. In addition to its application to real numbers, the word sign is used throughout mathematics to indicate aspects of mathematical objects that resemble positivity and negativity, such as the <a href="/wiki/Parity_of_a_permutation" title="Parity of a permutation">sign of a permutation</a>.</p>
<p>The word sign is also sometimes used to refer to various mathematical <a href="/wiki/Symbol" title="Symbol">symbols</a>, such as the <a href="/wiki/Plus_and_minus_signs" title="Plus and minus signs">plus and minus symbols</a> and the <a href="/wiki/Multiplication_sign" title="Multiplication sign">multiplication symbol</a>. See the <a href="/wiki/Table_of_mathematical_symbols" title="Table of mathematical symbols" class="mw-redirect">table of mathematical symbols</a> for more information on signs and symbols in mathematics.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Sign_of_a_number"><span class="tocnumber">1</span> <span class="toctext">Sign of a number</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Sign_of_zero"><span class="tocnumber">1.1</span> <span class="toctext">Sign of zero</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Terminology_for_signs"><span class="tocnumber">1.2</span> <span class="toctext">Terminology for signs</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Sign_convention"><span class="tocnumber">1.3</span> <span class="toctext">Sign convention</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Sign_function"><span class="tocnumber">2</span> <span class="toctext">Sign function</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Meanings_of_sign"><span class="tocnumber">3</span> <span class="toctext">Meanings of sign</span></a>
<ul>
<li class="toclevel-2 tocsection-7"><a href="#Sign_of_an_angle"><span class="tocnumber">3.1</span> <span class="toctext">Sign of an angle</span></a></li>
<li class="toclevel-2 tocsection-8"><a href="#Sign_of_a_change"><span class="tocnumber">3.2</span> <span class="toctext">Sign of a change</span></a></li>
<li class="toclevel-2 tocsection-9"><a href="#Sign_of_a_direction"><span class="tocnumber">3.3</span> <span class="toctext">Sign of a direction</span></a></li>
<li class="toclevel-2 tocsection-10"><a href="#Signedness_in_computing"><span class="tocnumber">3.4</span> <span class="toctext">Signedness in computing</span></a></li>
<li class="toclevel-2 tocsection-11"><a href="#Other_meanings"><span class="tocnumber">3.5</span> <span class="toctext">Other meanings</span></a></li>
</ul>
</li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=1" title="Edit section: Sign of a number"></a>]</span> <span class="mw-headline" id="Sign_of_a_number">Sign of a number</span></h2>
<p>A <a href="/wiki/Real_number" title="Real number">real number</a> is said to be positive if it is <a href="/wiki/Inequality_(mathematics)" title="Inequality (mathematics)">greater than</a> zero, and <a href="/wiki/Negative_number" title="Negative number">negative</a> if it is less than zero. The attribute of being positive or negative is called the <b>sign</b> of the number. Zero itself is not considered to have a sign.</p>
<p>In <a href="/wiki/Arithmetic" title="Arithmetic">arithmetic</a>, the sign of a number is often denoted by placing a <a href="/wiki/Plus_or_minus_sign" title="Plus or minus sign" class="mw-redirect">plus or minus sign</a> before the number. For example, +3 would denote a positive 3, and 3 would denote a negative 3. When no plus or minus sign is given, the default interpretation is that a number is positive.</p>
<p>In <a href="/wiki/Algebra" title="Algebra">algebra</a>, a minus sign is usually thought of as representing the operation of <a href="/wiki/Negation_(algebra)" title="Negation (algebra)">negation</a>, with the negation of a positive number being negative and the negation of a negative number being positive. In this context, it makes sense to write (3)&#160;=&#160;+3.</p>
<p>The sign of any nonzero number can be changed to positive using the <a href="/wiki/Absolute_value" title="Absolute value">absolute value</a> function. For example, the absolute value of 3 and the absolute value of 3 are both equal to 3. In symbols, this would be written |3| = 3 and |3| = 3.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=2" title="Edit section: Sign of zero"></a>]</span> <span class="mw-headline" id="Sign_of_zero">Sign of zero</span></h3>
<p>The number <a href="/wiki/0_(number)" title="0 (number)">zero</a> is neither positive nor negative, and therefore has no sign. In arithmetic, +0 and 0 both denote the same number 0, and the <a href="/wiki/Negation_(algebra)" title="Negation (algebra)">negation</a> of zero is zero itself.</p>
<p>In some contexts, such as <a href="/wiki/Signed_number_representations" title="Signed number representations">signed number representations</a> in <a href="/wiki/Computing" title="Computing">computing</a>, it makes sense to consider signed versions of zero, with positive zero and negative zero being different numbers (see <a href="/wiki/Signed_zero" title="Signed zero">signed zero</a>).</p>
<p>One also sees +0 and 0 in <a href="/wiki/Calculus" title="Calculus">calculus</a> and <a href="/wiki/Mathematical_analysis" title="Mathematical analysis">mathematical analysis</a> when evaluating certain <a href="/wiki/Limit" title="Limit">limits</a>. This notation refers to the behaviour of a function as the input variable approaches 0 from positive or negative values respectively; these behaviours are not necessarily the same.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=3" title="Edit section: Terminology for signs"></a>]</span> <span class="mw-headline" id="Terminology_for_signs"><span id="non-negative_and_non-positive"></span> Terminology for signs</span></h3>
<p>Because zero is neither positive nor negative, the following phrases are sometimes used to refer to the sign of an unknown number:</p>
<ul>
<li>A number is <b>positive</b> if it is greater than zero.</li>
<li>A number is <b>negative</b> if it is less than zero.</li>
<li>A number is <b>non-negative</b> if it is greater than or equal to zero.</li>
<li>A number is <b>non-positive</b> if it is less than or equal to zero.</li>
</ul>
<p>Thus a non-negative number is either positive or zero, while a non-positive number is either negative or zero. For example, the <a href="/wiki/Absolute_value" title="Absolute value">absolute value</a> of a real number is always non-negative, but is not necessarily positive.</p>
<p>The same terminology is sometimes used for <a href="/wiki/Function_(mathematics)" title="Function (mathematics)">functions</a> that take real or integer values. For example, a function would be called positive if all of its values are positive, or non-negative if all of its values are non-negative.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=4" title="Edit section: Sign convention"></a>]</span> <span class="mw-headline" id="Sign_convention">Sign convention</span></h3>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Sign_convention" title="Sign convention">Sign convention</a></div>
<p>In many contexts the choice of sign convention (which range of values is considered positive and which negative) is natural, whereas in others the choice is arbitrary subject only to consistency, the latter necessitating an explicit sign convention.</p>
<h2><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=5" title="Edit section: Sign function"></a>]</span> <span class="mw-headline" id="Sign_function">Sign function</span></h2>
<div class="thumb tright">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:Signum_function.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Signum_function.svg/200px-Signum_function.svg.png" width="200" height="160" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Signum_function.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Signum function y = sgn(x)</div>
</div>
</div>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Sign_function" title="Sign function">Sign function</a></div>
<p>The <b>sign function</b> or <b>signum function</b> is sometimes used to extract the sign of a number. This function is usually defined as follows:</p>
<dl>
<dd><img class="tex" alt=" \sgn(x) = \begin{cases}
-1 &amp; \text{if } x &lt; 0, \\
~~\, 0 &amp; \text{if } x = 0, \\
~~\, 1 &amp; \text{if } x &gt; 0. \end{cases}" src="//upload.wikimedia.org/wikipedia/en/math/5/c/f/5cf6a9d12f1371026747fda87e131a95.png" /></dd>
</dl>
<p>Thus sgn(<i>x</i>) is 1 when <i>x</i> is positive, and sgn(<i>x</i>) is 1 when <i>x</i> is negative. For nonzero values of <i>x</i>, this function can also be defined by the formula</p>
<dl>
<dd><img class="tex" alt=" \sgn(x) = \frac{x}{|x|}" src="//upload.wikimedia.org/wikipedia/en/math/d/f/c/dfc1d6fb12e0f808a0aea0ac489b884c.png" /></dd>
</dl>
<p>where |<i>x</i>| is the <a href="/wiki/Absolute_value" title="Absolute value">absolute value</a> of <i>x</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=6" title="Edit section: Meanings of sign"></a>]</span> <span class="mw-headline" id="Meanings_of_sign">Meanings of sign</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=7" title="Edit section: Sign of an angle"></a>]</span> <span class="mw-headline" id="Sign_of_an_angle">Sign of an angle</span></h3>
<div class="thumb tright">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:Yhvyvczsa7.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/1/1f/Yhvyvczsa7.png" width="220" height="192" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Yhvyvczsa7.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Measuring from the <a href="/wiki/X-axis" title="X-axis" class="mw-redirect">x-axis</a>, angles on the <a href="/wiki/Unit_circle" title="Unit circle">unit circle</a> count as positive in the <a href="/wiki/Counterclockwise" title="Counterclockwise" class="mw-redirect">counterclockwise</a> direction, and negative in the <a href="/wiki/Clockwise" title="Clockwise">clockwise</a> direction.</div>
</div>
</div>
<p>In many contexts, it is common to associate a sign with the measure of an <a href="/wiki/Angle" title="Angle">angle</a>, particularly an oriented angle or an angle of <a href="/wiki/Rotation" title="Rotation">rotation</a>. In such a situation, the sign indicates whether the angle is in the <a href="/wiki/Clockwise" title="Clockwise">clockwise</a> or counterclockwise direction. Though different conventions can be used, it is common in <a href="/wiki/Mathematics" title="Mathematics">mathematics</a> to have counterclockwise angles count as positive, and clockwise angles count as negative.</p>
<p>It is also possible to associate a sign to an angle of rotation in three dimensions, assuming the <a href="/wiki/Axis_of_rotation" title="Axis of rotation" class="mw-redirect">axis of rotation</a> has been oriented. Specifically, a <a href="/wiki/Right-hand_rule" title="Right-hand rule">right-handed</a> rotation around an oriented axis typically counts as positive, while a left-handed rotation counts as negative.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=8" title="Edit section: Sign of a change"></a>]</span> <span class="mw-headline" id="Sign_of_a_change">Sign of a change</span></h3>
<p>When a quantity <i>x</i> changes over time, the <a href="/wiki/Finite_difference" title="Finite difference">change</a> in the value of <i>x</i> is typically defined by the equation</p>
<dl>
<dd><img class="tex" alt="\Delta x = x_\text{final} - x_\text{initial}. \," src="//upload.wikimedia.org/wikipedia/en/math/2/2/a/22a9eb645e708a1ae524b53bb3302a3a.png" /></dd>
</dl>
<p>Using this convention, an increase in <i>x</i> counts as positive change, while a decrease of <i>x</i> counts as negative change. In <a href="/wiki/Calculus" title="Calculus">calculus</a>, this same convention is used in the definition of the <a href="/wiki/Derivative" title="Derivative">derivative</a>. As a result, any <a href="/wiki/Monotonic_function" title="Monotonic function">increasing function</a> has positive derivative, while a decreasing function has negative derivative.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=9" title="Edit section: Sign of a direction"></a>]</span> <span class="mw-headline" id="Sign_of_a_direction">Sign of a direction</span></h3>
<p>In <a href="/wiki/Analytic_geometry" title="Analytic geometry">analytic geometry</a> and <a href="/wiki/Physics" title="Physics">physics</a>, it is common to label certain directions as positive or negative. For a basic example, the <a href="/wiki/Number_line" title="Number line">number line</a> is usually drawn with positive numbers to the right, and negative numbers to the left:</p>
<div class="center">
<div class="floatnone"><a href="/wiki/File:Number-line.gif" class="image"><img alt="Number-line.gif" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/09/Number-line.gif/600px-Number-line.gif" width="600" height="46" /></a></div>
</div>
<p>As a result, when discussing <a href="/wiki/Linear_motion" title="Linear motion">linear motion</a>, <a href="/wiki/Displacement_(vector)" title="Displacement (vector)">displacement</a> or <a href="/wiki/Velocity" title="Velocity">velocity</a> to the right is usually thought of as being positive, while similar motion to the left is thought of as being negative.</p>
<p>On the <a href="/wiki/Cartesian_plane" title="Cartesian plane" class="mw-redirect">Cartesian plane</a>, the rightward and upward directions are usually thought of as positive, with rightward being the positive <i>x</i>-direction, and upward being the positive <i>y</i>-direction. If a displacement or velocity <a href="/wiki/Euclidean_vector" title="Euclidean vector">vector</a> is separated into its <a href="/wiki/Vector_component" title="Vector component" class="mw-redirect">vector components</a>, then the horizontal part will be positive for motion to the right and negative for motion to the left, while the vertical part will be positive for motion upward and negative for motion downward.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=10" title="Edit section: Signedness in computing"></a>]</span> <span class="mw-headline" id="Signedness_in_computing">Signedness in computing</span></h3>
<div class="thumb tright">
<div style="width:20em;">
<table style="width:100%; margin:0;" cellspacing="0">
<tr>
<td align="center" style="background-color:#ddeeff;"><a href="/wiki/Most_significant_bit" title="Most significant bit">most-significant bit</a></td>
<td align="center" colspan="9"></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>127</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>126</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>2</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>1</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>0</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>1</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>2</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>127</b></td>
</tr>
<tr>
<td align="center" style="width:2em; background-color:#ddeeff; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-left:1px solid #aaaaaa;"><b>1</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em; border-top:1px solid #aaaaaa; border-bottom:1px solid #aaaaaa; border-right:1px solid #aaaaaa;"><b>0</b></td>
<td align="center" style="width:2em;"><b>=</b></td>
<td align="right" style="width:2em;"><b>128</b></td>
</tr>
</table>
<div class="thumbcaption" style="font-size: 90%;">Most computers use <a href="/wiki/Two%27s_complement" title="Two's complement">two's complement</a> to represent the sign of an integer.</div>
</div>
</div>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Signedness" title="Signedness">Signedness</a></div>
<p>In <a href="/wiki/Computing" title="Computing">computing</a>, a numeric value may be either signed or unsigned, depending on whether the computer is keeping track of a sign for the number. By restricting a <a href="/wiki/Variable_(programming)" title="Variable (programming)" class="mw-redirect">variable</a> to non-negative values only, one more <a href="/wiki/Bit" title="Bit">bit</a> can be used for storing the value of a number.</p>
<p>Because of the way arithmetic is done within computers, the sign of a signed variable is usually not stored as a single independent bit, but is instead stored using <a href="/wiki/Two%27s_complement" title="Two's complement">two's complement</a> or some other <a href="/wiki/Signed_number_representation" title="Signed number representation" class="mw-redirect">signed number representation</a>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Sign_(mathematics)&amp;action=&amp;section=11" title="Edit section: Other meanings"></a>]</span> <span class="mw-headline" id="Other_meanings">Other meanings</span></h3>
<div class="thumb tright">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:VFPt_dipole_electric.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/d/df/VFPt_dipole_electric.svg/220px-VFPt_dipole_electric.svg.png" width="220" height="220" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:VFPt_dipole_electric.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
<a href="/wiki/Electric_charge" title="Electric charge">Electric charge</a> may be positive or negative.</div>
</div>
</div>
<p>In addition to the sign of a real number, the word sign is also used in various related ways throughout mathematics and the sciences:</p>
<ul>
<li>The <a href="/wiki/Parity_of_a_permutation" title="Parity of a permutation">sign of a permutation</a> is defined to be positive if the permutation is even, and negative if the permutation is odd.</li>
<li>In <a href="/wiki/Graph_theory" title="Graph theory">graph theory</a>, a <a href="/wiki/Signed_graph" title="Signed graph">signed graph</a> is a graph in which each edge has been marked with a positive or negative sign.</li>
<li>In <a href="/wiki/Mathematical_analysis" title="Mathematical analysis">mathematical analysis</a>, a <a href="/wiki/Signed_measure" title="Signed measure">signed measure</a> is a generalization of the concept of <a href="/wiki/Measure_(mathematics)" title="Measure (mathematics)">measure</a> in which the measure of a set may have positive or negative values.</li>
<li>In a <a href="/wiki/Signed-digit_representation" title="Signed-digit representation">signed-digit representation</a>, each digit of a number may have a positive or negative sign.</li>
<li>The ideas of <a href="/w/index.php?title=Signed_area&amp;action=&amp;redlink=1" class="new" title="Signed area (page does not exist)">signed area</a> and <a href="/wiki/Signed_volume" title="Signed volume" class="mw-redirect">signed volume</a> are sometimes used when it is convenient for certain areas or volumes to count as negative. This is particularly true in the theory of <a href="/wiki/Determinant" title="Determinant">determinants</a>.</li>
<li>In <a href="/wiki/Physics" title="Physics">physics</a>, any <a href="/wiki/Electric_charge" title="Electric charge">electric charge</a> comes with a sign, either positive or negative. By convention, a positive charge is a charge with the same sign as that of a <a href="/wiki/Proton" title="Proton">proton</a>, and a negative charge is a charge with the same sign as that of an <a href="/wiki/Electron" title="Electron">electron</a>.</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 279/1000000
Post-expand include size: 1489/2048000 bytes
Template argument size: 622/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:7951270-0!0!0!!en!4!* and timestamp 20120321221855 -->
