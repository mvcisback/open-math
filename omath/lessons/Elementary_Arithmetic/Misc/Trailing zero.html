<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, <b>trailing zeros</b> are a sequence of <a href="/wiki/0_(number)" title="0 (number)">0s</a> in the <a href="/wiki/Decimal" title="Decimal">decimal</a> representation (or more generally, in any <a href="/wiki/Positional_notation" title="Positional notation">positional representation</a>) of a number, after which no other <a href="/wiki/Numerical_digit" title="Numerical digit">digits</a> follow.</p>
<p>Trailing zeros to the right of a <a href="/wiki/Decimal_point" title="Decimal point" class="mw-redirect">decimal point</a>, as in 12.3400, do not affect the value of a number and may be omitted if all that is of interest is its numerical value. This is true even if the zeros <a href="/wiki/Recurring_decimal" title="Recurring decimal" class="mw-redirect">recur infinitely</a>. However, trailing zeros may be useful for indicating the number of <a href="/wiki/Significant_figure" title="Significant figure" class="mw-redirect">significant figures</a>, for example in a measurement. In such a context, "simplifying" a number by removing trailing zeros would be incorrect.</p>
<p>The number of trailing zeros in a base-<i>b</i> <a href="/wiki/Integer" title="Integer">integer</a> <i>n</i> equals the exponent of the highest power of <i>b</i> that divides <i>n</i>. For example, 14000 has three trailing zeros and is therefore divisible by 1000 = 10<sup>3</sup>. This property is useful when looking for small factors in <a href="/wiki/Integer_factorization" title="Integer factorization">integer factorization</a>. <a href="/wiki/Binary_number" title="Binary number" class="mw-redirect">Binary numbers</a> with many trailing zero bits are handled similarly in <a href="/wiki/Computer_arithmetic" title="Computer arithmetic" class="mw-redirect">computer arithmetic</a>. In computer software, the <a href="/wiki/Count_trailing_zeros" title="Count trailing zeros" class="mw-redirect">count trailing zeros</a> operation efficiently determines the number of trailing zero bits in a machine word.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Factorial"><span class="tocnumber">1</span> <span class="toctext">Factorial</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#See_also"><span class="tocnumber">2</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#References"><span class="tocnumber">3</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#External_links"><span class="tocnumber">4</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Trailing_zero&amp;action=&amp;section=1" title="Edit section: Factorial"></a>]</span> <span class="mw-headline" id="Factorial">Factorial</span></h2>
<p>The number of trailing zeros in the <a href="/wiki/Decimal_representation" title="Decimal representation">decimal representation</a> of <i>n</i>!, the <a href="/wiki/Factorial" title="Factorial">factorial</a> of a <a href="/wiki/Non-negative" title="Non-negative" class="mw-redirect">non-negative</a> <a href="/wiki/Integer" title="Integer">integer</a> <i>n</i>, is simply the multiplicity of the <a href="/wiki/Prime_number" title="Prime number">prime</a> factor&#160;5 in <i>n</i>!. This can be determined with this special case of <a href="/wiki/De_Polignac%27s_formula" title="De Polignac's formula">de Polignac's formula</a>:<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt="f(n) = \sum_{i=1}^k \left \lfloor \frac{n}{5^i} \right \rfloor =
\left \lfloor \frac{n}{5} \right \rfloor + \left \lfloor \frac{n}{5^2} \right \rfloor + \left \lfloor \frac{n}{5^3} \right \rfloor + \cdots + \left \lfloor \frac{n}{5^k} \right \rfloor, \," src="//upload.wikimedia.org/wikipedia/en/math/b/a/4/ba407ce5567558a041a1f8760473aebd.png" /></dd>
</dl>
<p>where <i>k</i> must be chosen such that</p>
<dl>
<dd><img class="tex" alt="5^{k+1} &gt; n \ge 5^k,\," src="//upload.wikimedia.org/wikipedia/en/math/a/d/4/ad4f86dca0ac330be70e1db87f047b6e.png" /></dd>
</dl>
<p>and <i>a</i> denotes the <a href="/wiki/Floor_function" title="Floor function" class="mw-redirect">floor function</a> applied to <i>a</i>. For <i>n</i>&#160;=&#160;0,&#160;1,&#160;2,&#160;... this is</p>
<dl>
<dd>0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 6, ... (sequence <span class="nowrap"><a href="//oeis.org/A027868" class="extiw" title="oeis:A027868">A027868</a></span> in <a href="/wiki/On-Line_Encyclopedia_of_Integer_Sequences" title="On-Line Encyclopedia of Integer Sequences">OEIS</a>).</dd>
</dl>
<p>For example, 5<sup>3</sup>&#160;&gt;&#160;32, and therefore&#160;32!&#160;=&#160;263130836933693530167218012160000000 ends in</p>
<dl>
<dd><img class="tex" alt="\left \lfloor \frac{32}{5} \right \rfloor + \left \lfloor \frac{32}{5^2} \right \rfloor = 6 + 1 = 7\," src="//upload.wikimedia.org/wikipedia/en/math/9/4/4/944c6abf69b79a029f53f53ab9850215.png" /></dd>
</dl>
<p>zeros. If <i>n</i>&#160;&lt;&#160;5, the inequality is satisfied by <i>k</i>&#160;=&#160;0; in that case the sum is <a href="/wiki/Empty_sum" title="Empty sum">empty</a>, giving the answer&#160;0.</p>
<p>The formula actually counts the number of factors&#160;5 in <i>n</i>!, but since there are at least as many factors&#160;2, this is equivalent to the number of factors 10, each of which gives one more trailing zero.</p>
<p>Defining</p>
<dl>
<dd><img class="tex" alt="q_i = \left \lfloor \frac{n}{5^i} \right \rfloor,\," src="//upload.wikimedia.org/wikipedia/en/math/c/f/6/cf6c7ab8a415108cd66e3fc3ce97488c.png" /></dd>
</dl>
<p>the following <a href="/wiki/Recurrence_relation" title="Recurrence relation">recurrence relation</a> holds:</p>
<dl>
<dd><img class="tex" alt="\begin{align}q_0\,\,\,\,\, &amp; = \,\,\,n,\quad \\
 q_{i+1} &amp; = \left \lfloor \frac{q_i}{5} \right \rfloor.\,\end{align}" src="//upload.wikimedia.org/wikipedia/en/math/6/5/0/650878c828865d2d620a5ae8aee9c869.png" /></dd>
</dl>
<p>This can be used to simplify the computation of the terms of the summation, which can be stopped as soon as <i>q<sub>&#160;i</sub></i> reaches zero. The condition <span class="nowrap">5<sup><i>k</i>+1</sup> &gt; <i>n</i></span> is equivalent to <span class="nowrap"><i>q</i><sub>&#160;<i>k</i>+1</sub> = 0.</span></p>
<h2><span class="section">[<a href="/w/index.php?title=Trailing_zero&amp;action=&amp;section=2" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Leading_zero" title="Leading zero">Leading zero</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Trailing_zero&amp;action=&amp;section=3" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">Summarized from <a rel="nofollow" class="external text" href="http://www.purplemath.com/modules/factzero.htm">Factorials and Trailing Zeroes</a></span></li>
</ol>
<h2><span class="section">[<a href="/w/index.php?title=Trailing_zero&amp;action=&amp;section=4" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://speleotrove.com/decimal/decifaq1.html#tzeros"><i>Why are trailing fractional zeros important?</i></a> for some examples of when trailing zeros are significant</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 132/1000000
Post-expand include size: 286/2048000 bytes
Template argument size: 99/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:7282654-0!0!0!!en!*!* and timestamp 20120322174621 -->
