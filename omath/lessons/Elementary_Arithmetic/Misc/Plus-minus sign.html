<div class="dablink">For other uses, see <a href="/wiki/Plus-minus_(disambiguation)" title="Plus-minus (disambiguation)">plus-minus (disambiguation)</a>.</div>
<div style="float:right; margin: 0 0 10px 10px; padding:40px; font-size:400%; font-family: Times; background-color: #ddddff; border: 1px solid #aaaaff;"><b><span class="Unicode"></span></b></div>
<p>The <b>plus-minus sign</b> (<span class="Unicode"></span>) is a mathematical symbol commonly used either</p>
<ul>
<li>to indicate the <a href="/wiki/Accuracy_and_precision" title="Accuracy and precision">precision</a> of an <a href="/wiki/Approximation" title="Approximation">approximation</a>, or</li>
<li>to indicate a value that can be of either sign.</li>
</ul>
<p>The sign is normally pronounced "plus or minus". In experimental sciences, the sign commonly indicates the <a href="/wiki/Confidence_interval" title="Confidence interval">confidence interval</a> or <a href="/wiki/Errors_and_residuals_in_statistics" title="Errors and residuals in statistics">error</a> in a measurement, often the <a href="/wiki/Standard_deviation" title="Standard deviation">standard deviation</a> or <a href="/wiki/Standard_error" title="Standard error">standard error</a>. The sign may also represent an inclusive range of values that a reading might have. In mathematics, it may indicate two possible values: one positive, and one negative. It is commonly used in indicating a range of values, such as in mathematical statements.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Precision_indication"><span class="tocnumber">1</span> <span class="toctext">Precision indication</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Shorthand"><span class="tocnumber">2</span> <span class="toctext">Shorthand</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Minus-plus_sign"><span class="tocnumber">3</span> <span class="toctext">Minus-plus sign</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Other_uses"><span class="tocnumber">4</span> <span class="toctext">Other uses</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Encodings"><span class="tocnumber">5</span> <span class="toctext">Encodings</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Similar_characters"><span class="tocnumber">6</span> <span class="toctext">Similar characters</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#See_also"><span class="tocnumber">7</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#References"><span class="tocnumber">8</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=1" title="Edit section: Precision indication"></a>]</span> <span class="mw-headline" id="Precision_indication">Precision indication</span></h2>
<p>The use of <span class="Unicode"></span> for an approximation is most commonly encountered in presenting the numerical value of a quantity together with its <a href="/wiki/Tolerance_(engineering)" title="Tolerance (engineering)" class="mw-redirect">tolerance</a> or its statistical <a href="/wiki/Margin_of_error" title="Margin of error">margin of error</a>. For example, "<span style="white-space:nowrap">5.7<span style="white-space:nowrap;margin-left:0.3em;margin-right:0.15em"><span class="Unicode"></span></span>0.2</span>" denotes a quantity that is specified or estimated to be within 0.2 units of 5.7; it may be anywhere in the range from <span class="nowrap">5.7  0.2</span> (i.e., 5.5) to <span class="nowrap">5.7 + 0.2</span> (5.9). In scientific usage it sometimes refers to a probability of being within the stated interval, usually corresponding to either 1 or 2 <a href="/wiki/Standard_deviation" title="Standard deviation">standard deviations</a> (a probability of 68.3% or 95.4% in a Normal distribution).</p>
<p>A <a href="/wiki/Percentage" title="Percentage">percentage</a> may also be used to indicate the error margin. For example, <span class="nowrap">230 <span class="Unicode"></span> 10% V</span> refers to a voltage within 10% of either side of 230&#160;V (207&#160;V to 253&#160;V). Separate values for the upper and lower bounds may also be used. For example, to indicate that a value is most likely 5.7 but may be as high as 5.9 or as low as 5.6, one could write <span style="white-space:nowrap">5.7<span style="white-space:nowrap;margin-left:0.3em;"><span style="display:inline-block; margin-bottom:-0.3em; vertical-align:-0.4em; line-height:1.2em; font-family:monospace,'Courier New'; font-size:85%; text-align:right;">+0.2<br />
0.1</span></span></span>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=2" title="Edit section: Shorthand"></a>]</span> <span class="mw-headline" id="Shorthand">Shorthand</span></h2>
<p>In <a href="/wiki/Mathematical_equation" title="Mathematical equation" class="mw-redirect">mathematical equations</a>, the use of <span class="Unicode"></span> may be found as shorthand, to present two equations in one formula: + <i>or</i> , represented with <span class="Unicode"></span>.</p>
<p>For example, given the equation <i>x</i><sup>2</sup> = 1, one may give the solution as <i>x</i> = 1, such that both <i>x</i> = +1 and <i>x</i> = 1 are valid solutions.</p>
<p>More generally we have the <a href="/wiki/Quadratic_formula" title="Quadratic formula" class="mw-redirect">quadratic formula</a>:</p>
<p>If <i>ax</i><sup>2</sup>&#160;+&#160;<i>bx</i>&#160;+&#160;<i>c</i>&#160;=&#160;0 then</p>
<dl>
<dd><img class="tex" alt="\displaystyle x = \frac{-b \pm \sqrt{b^2-4ac}}{2a}." src="//upload.wikimedia.org/wikipedia/en/math/7/a/b/7ab49a37e997cc409582e44c1850c8eb.png" /></dd>
</dl>
<p>Written out in full, this states that there are two solutions to the equation, i.e. that</p>
<dl>
<dd><img class="tex" alt="\text{either } x = \frac{-b + \sqrt {b^2-4ac}}{2a} \text{ or } x = \frac{-b - \sqrt {b^2-4ac}}{2a}." src="//upload.wikimedia.org/wikipedia/en/math/a/b/c/abc9953ba65beda8b49826a0738ea609.png" /></dd>
</dl>
<p>Another example is found in the <a href="/wiki/Trigonometric_identity" title="Trigonometric identity" class="mw-redirect">trigonometric identity</a></p>
<dl>
<dd><img class="tex" alt="\sin(x \pm y) = \sin(x) \cos(y) \pm \cos(x) \sin(y).\," src="//upload.wikimedia.org/wikipedia/en/math/6/a/5/6a59fc64bb23e64ab893b07440ef1e3f.png" /></dd>
</dl>
<p>This stands for two identities: one with "+" on both sides of the equation, and one with "" on both sides.</p>
<p>A somewhat different use is found in this presentation of the formula for the <a href="/wiki/Taylor_series" title="Taylor series">Taylor series</a> of the sine function:</p>
<dl>
<dd><img class="tex" alt="\sin\left( x \right) = x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \cdots \pm \frac{1}{(2n+1)!} x^{2n+1} + \cdots. " src="//upload.wikimedia.org/wikipedia/en/math/b/9/f/b9f4c62d39449ab3c37fbbc13f7f9886.png" /></dd>
</dl>
<p>This mild <a href="/wiki/Abuse_of_notation" title="Abuse of notation">abuse of notation</a> is intended to indicate that the signs of the terms alternate, where (starting the count at&#160;0) the terms with an even index&#160;<i>n</i> are added while those with an odd index are subtracted. A more rigorous presentation would use the expression (1)<sup><i>n</i></sup>, which gives +1 when <i>n</i> is even and 1 when <i>n</i> is odd.</p>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=3" title="Edit section: Minus-plus sign"></a>]</span> <span class="mw-headline" id="Minus-plus_sign">Minus-plus sign</span></h2>
<p>There is another symbol, the <b>minus-plus sign</b> (<span class="Unicode"></span>). It is generally used in conjunction with the "<span class="Unicode"></span>" sign, in such expressions as "x <span class="Unicode"></span> y <span class="Unicode"></span> z", which can be interpreted as meaning "<i>x</i> + <i>y</i>  <i>z</i>" or/and "<i>x</i>  <i>y</i> + <i>z</i>", but <i>not</i> "<i>x</i> + <i>y</i> + <i>z</i>" nor "<i>x</i>  <i>y</i>  <i>z</i>". The upper "" in "<span class="Unicode"></span>" is considered to be associated to the "+" of "<span class="Unicode"></span>" (and similarly for the two lower symbols) even though there is no visual indication of the dependency. (However, the "<span class="Unicode"></span>" sign is generally preferred over the "<span class="Unicode"></span>" sign, so if they both appear in an equation it is safe to assume that they are linked. On the other hand, if there are two instances of the "<span class="Unicode"></span>" sign in an expression, it is impossible to tell from notation alone whether the intended interpretation is as two or four distinct expressions.) The original expression can be rewritten as "<i>x</i> <span class="Unicode"></span> (<i>y</i>  <i>z</i>)" to avoid confusion, but cases such as the trigonometric identity</p>
<dl>
<dd><img class="tex" alt="\cos(x \pm y) = \cos(x) \cos(y) \mp \sin(x) \sin(y) " src="//upload.wikimedia.org/wikipedia/en/math/a/4/2/a42fcc8a91ac5cf8b042f95c89a8aaf5.png" /></dd>
</dl>
<p>are most neatly written using the "<span class="Unicode"></span>" sign. The trigonometric equation above thus represents the two equations:</p>
<dl>
<dd><img class="tex" alt="\cos(x + y) = \cos(x)\cos(y) - \sin(x) \sin(y)\," src="//upload.wikimedia.org/wikipedia/en/math/9/e/b/9ebb361ce1cd17247be44d7a47fabac4.png" /></dd>
<dd><img class="tex" alt="\cos(x - y) = \cos(x)\cos(y) + \sin(x) \sin(y)\," src="//upload.wikimedia.org/wikipedia/en/math/6/5/8/658606805a61437f5f06d4f320db2700.png" /></dd>
</dl>
<p>but <i>never</i></p>
<dl>
<dd><img class="tex" alt="\cos(x + y) = \cos(x)\cos(y) + \sin(x) \sin(y)\," src="//upload.wikimedia.org/wikipedia/en/math/b/5/6/b56e4f35d8063a01eabe385058c7048c.png" /></dd>
<dd><img class="tex" alt="\cos(x - y) = \cos(x)\cos(y) - \sin(x) \sin(y)\," src="//upload.wikimedia.org/wikipedia/en/math/9/5/3/953767d1c920b581cc956528a964c925.png" /></dd>
</dl>
<p>because the signs are exclusively alternating.</p>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=4" title="Edit section: Other uses"></a>]</span> <span class="mw-headline" id="Other_uses">Other uses</span></h2>
<p>The symbols <span class="Unicode"></span> and <span class="Unicode"></span> are used in <a href="/wiki/Punctuation_(chess)#Position_evaluation_symbols" title="Punctuation (chess)">chess notation</a> to denote an advantage for white and black respectively.</p>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=5" title="Edit section: Encodings"></a>]</span> <span class="mw-headline" id="Encodings">Encodings</span></h2>
<ul>
<li>In <a href="/wiki/ISO_8859-1" title="ISO 8859-1" class="mw-redirect">ISO 8859-1</a>, <a href="/wiki/ISO_8859-7" title="ISO 8859-7" class="mw-redirect">-7</a>, <a href="/wiki/ISO_8859-8" title="ISO 8859-8" class="mw-redirect">-8</a>, <a href="/wiki/ISO_8859-9" title="ISO 8859-9" class="mw-redirect">-9</a>, <a href="/wiki/ISO_8859-13" title="ISO 8859-13" class="mw-redirect">-13</a>, <a href="/wiki/ISO_8859-15" title="ISO 8859-15" class="mw-redirect">-15</a>, and <a href="/wiki/ISO_8859-16" title="ISO 8859-16" class="mw-redirect">-16</a>, the plus-minus symbol is given by the code 0xB1<sub><a href="/wiki/Hexadecimal" title="Hexadecimal">hex</a></sub> Since the first 256 code points of Unicode are identical to the contents of ISO-8859-1 this symbol is also at <a href="/wiki/Unicode" title="Unicode">Unicode</a> code point U+00B1.</li>
<li>The symbol also has a <a href="/wiki/Character_entity_reference" title="Character entity reference">HTML entity</a> representation of <code>&amp;plusmn;</code>.</li>
<li>On <a href="/wiki/Microsoft_Windows" title="Microsoft Windows">Windows</a> systems, it may be entered by means of <a href="/wiki/Alt_code" title="Alt code">Alt codes</a>, by holding the ALT key while typing the numbers 0177 on the <a href="/wiki/Numeric_keypad" title="Numeric keypad">numeric keypad</a>.</li>
<li>On Unix-like systems, it can be entered by typing the sequence <a href="/wiki/Compose_key" title="Compose key">compose</a> + -.</li>
<li>On Macintosh systems, it may be entered by pressing option shift = (on the non-numeric keypad).</li>
<li>The rarer minus-plus sign (<span class="Unicode"></span>) is not generally found in legacy encodings and does not have a named HTML entity but is available in Unicode with codepoint U+2213 and so can be used in HTML using <code>&amp;#x2213;</code> or <code>&amp;#8723;</code>.</li>
<li>In <a href="/wiki/TeX" title="TeX">TeX</a> 'plus-or-minus' and 'minus-or-plus' symbols are denoted <code>\pm</code> and <code>\mp</code>, respectively.</li>
<li>These characters are also seen written as a (rather untidy) underlined or overlined + symbol. (&#160;<u>+</u>&#160; or <span style="text-decoration:overline;">+</span>&#160;).</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=6" title="Edit section: Similar characters"></a>]</span> <span class="mw-headline" id="Similar_characters">Similar characters</span></h2>
<p>The plus-minus sign resembles the <a href="/wiki/Chinese_character" title="Chinese character" class="mw-redirect">Chinese character</a> <a href="//en.wiktionary.org/wiki/%E5%A3%AB" class="extiw" title="wikt:"></a>, whereas the minus-plus sign resembles <a href="//en.wiktionary.org/wiki/%E5%B9%B2" class="extiw" title="wikt:"></a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=7" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Plus_and_minus_signs" title="Plus and minus signs">Plus and minus signs</a></li>
<li><a href="/wiki/Table_of_mathematical_symbols" title="Table of mathematical symbols" class="mw-redirect">Table of mathematical symbols</a></li>
<li><a href="/wiki/%E2%89%88" title="" class="mw-redirect"></a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Plus-minus_sign&amp;action=&amp;section=8" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<table class="metadata plainlinks ambox ambox-content ambox-Refimprove" style="">
<tr>
<td class="mbox-image">
<div style="width: 52px;"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/50px-Question_book-new.svg.png" width="50" height="39" /></div>
</td>
<td class="mbox-text" style="">This article <b>needs additional <a href="/wiki/Wikipedia:Citing_sources#Inline_citations" title="Wikipedia:Citing sources">citations</a> for <a href="/wiki/Wikipedia:Verifiability" title="Wikipedia:Verifiability">verification</a></b>. Please help <a class="external text" href="//en.wikipedia.org/w/index.php?title=Plus-minus_sign&amp;action=">improve this article</a> by adding citations to <a href="/wiki/Wikipedia:Identifying_reliable_sources" title="Wikipedia:Identifying reliable sources">reliable sources</a>. Unsourced material may be <a href="/wiki/Template:Citation_needed" title="Template:Citation needed">challenged</a> and <a href="/wiki/Wikipedia:Verifiability#Burden_of_evidence" title="Wikipedia:Verifiability">removed</a>. <small><i>(March 2011)</i></small></td>
</tr>
</table>


<!-- 
NewPP limit report
Preprocessor node count: 1311/1000000
Post-expand include size: 10730/2048000 bytes
Template argument size: 2219/2048000 bytes
Expensive parser function count: 1/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:367225-0!0!0!!en!4!* and timestamp 20120414174504 -->
