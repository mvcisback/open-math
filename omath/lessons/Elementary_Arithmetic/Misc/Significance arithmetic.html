<p><b>Significance arithmetic</b> is a set of rules (sometimes called <b>significant figure rules</b>) for approximating the propagation of uncertainty in scientific or statistical calculations. These rules can be used to find the appropriate number of <a href="/wiki/Significant_figures" title="Significant figures">significant figures</a> to use to represent the result of a calculation. If a calculation is done without analysis of the uncertainty involved, a result that is written with too many significant figures can be taken to imply a higher <a href="/wiki/Arithmetic_precision" title="Arithmetic precision">precision</a> than is known, and a result that is written with too few significant figures results in an avoidable loss of precision. Understanding these rules requires a good understanding of the concept of <a href="/wiki/Significant_figures" title="Significant figures">significant and insignificant figures</a>.</p>
<p>The rules of significance arithmetic are an approximation based on statistical rules for dealing with probability distributions. See the article on <a href="/wiki/Propagation_of_uncertainty" title="Propagation of uncertainty">propagation of uncertainty</a> for these more advanced and precise rules. Significance arithmetic rules rely on the assumption that the number of significant figures in the <a href="/wiki/Operand" title="Operand">operands</a> gives accurate information about the uncertainty of the operands and hence the uncertainty of the result. For an alternative see <a href="/wiki/Interval_arithmetic" title="Interval arithmetic">interval arithmetic</a>.</p>
<p>An important caveat is that significant figures apply only to <i>measured</i> values. Values known to be exact should be ignored for determining the number of significant figures that belong in the result. Examples of such values include:</p>
<ul>
<li><a href="/wiki/Integer" title="Integer">integer</a> counts (e.g., the number of oranges in a bag)</li>
<li>definitions of one unit in terms of another (e.g. a minute is 60 seconds)</li>
<li>actual prices asked or offered, and quantities given in requirement specifications</li>
<li>legally defined conversions, such as international currency exchange</li>
<li>scalar operations, such as "tripling" or "halving"</li>
<li>mathematical constants, such as <a href="/wiki/%CE%A0" title="" class="mw-redirect"></a> and <a href="/wiki/E_(mathematical_constant)" title="E (mathematical constant)">e</a></li>
</ul>
<p>Physical constants such as <a href="/wiki/Avogadro%27s_number" title="Avogadro's number" class="mw-redirect">Avogadro's number</a>, on the other hand, have a limited number of significant digits, because these constants are known to us only by measurement.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Multiplication_and_division_using_significance_arithmetic"><span class="tocnumber">1</span> <span class="toctext">Multiplication and division using significance arithmetic</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Addition_and_subtraction_using_significance_arithmetic"><span class="tocnumber">2</span> <span class="toctext">Addition and subtraction using significance arithmetic</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Rounding_rules"><span class="tocnumber">3</span> <span class="toctext">Rounding rules</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Disagreements_about_importance"><span class="tocnumber">4</span> <span class="toctext">Disagreements about importance</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#See_also"><span class="tocnumber">6</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#External_links"><span class="tocnumber">7</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=1" title="Edit section: Multiplication and division using significance arithmetic"></a>]</span> <span class="mw-headline" id="Multiplication_and_division_using_significance_arithmetic">Multiplication and division using significance arithmetic</span></h2>
<p>When multiplying or dividing numbers, the result is <a href="/wiki/Rounding" title="Rounding">rounded</a> to the <i>number</i> of significant figures in the factor with the least significant figures. Here, the <i>quantity</i> of significant figures in each of the factors is importantnot the <i>position</i> of the significant figures. For instance, using significance arithmetic rules:</p>
<ul>
<li>8  8 = 6  10<sup>1</sup></li>
<li>8  8.0 = 6  10<sup>1</sup></li>
<li>8.0  8.0 = 64</li>
<li>8.02  8.02 = 64.3</li>
<li>8 / 2.0 = 4</li>
<li>8.6 /2.0012 = 4.3</li>
<li>2  0.8 = 2</li>
</ul>
<p>If, in the above, the numbers are assumed to be measurements (and therefore probably inexact) then "8" above represents an inexact measurement with only one significant digit. Therefore, the result of "8  8" is rounded to a result with only one significant digit, i.e., "6  10<sup>1</sup>" instead of the unrounded "64" that one might expect. In many cases, the rounded result is less accurate than the non-rounded result; a measurement of "8" has an actual underlying quantity between 7.5 and 8.5. The true square would be in the range between 56.25 and 72.25. So 6  10<sup>1</sup> is the best one can give, as other possible answers give a false sense of accuracy. Further, the 6  10<sup>1</sup> is itself confusing (as it might be considered to imply 60 5, which is over-optimistic; more accurate would be 64 8).</p>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=2" title="Edit section: Addition and subtraction using significance arithmetic"></a>]</span> <span class="mw-headline" id="Addition_and_subtraction_using_significance_arithmetic">Addition and subtraction using significance arithmetic</span></h2>
<p>When adding or subtracting using significant figures rules, results are rounded to the <i>position</i> of the least significant digit in the most uncertain of the numbers being summed (or subtracted). That is, the result is rounded to the last digit that is significant in <i>each</i> of the numbers being summed. Here the <i>position</i> of the significant figures is important, but the <i>quantity</i> of significant figures is irrelevant. Some examples using these rules:</p>
<ul>
<li>1 + 1.1 = 2
<ul>
<li>1 is significant to the ones place, 1.1 is significant to the tenths place. Of the two, the least accurate is the ones place. The answer cannot have any significant figures past the ones place.</li>
</ul>
</li>
</ul>
<ul>
<li>1.0 + 1.1 = 2.1
<ul>
<li>1.0 and 1.1 are significant to the tenths place, so the answer will also have a number in the tenths place.</li>
</ul>
</li>
</ul>
<ul>
<li>100 + 110 = 200
<ul>
<li>100 is significant to the hundreds place, while 110 is significant to the tens place. Therefore, the answer must be rounded to the nearest hundred.</li>
</ul>
</li>
</ul>
<ul>
<li>100. + 110. = 210.
<ul>
<li>100. and 110. are both significant to the ones place (as indicated by the decimal), so the answer is also significant to the ones place.</li>
</ul>
</li>
</ul>
<ul>
<li>110<sup>2</sup> + 1.110<sup>2</sup> = 210<sup>2</sup>
<ul>
<li>100 is significant up to the hundreds place, while 110 is up to the tens place. Of the two, the least accurate is the hundreds place. The answer should not have significant digits past the hundreds place.</li>
</ul>
</li>
</ul>
<ul>
<li>1.010<sup>2</sup> + 111 = 2.110<sup>2</sup>
<ul>
<li>1.010<sup>2</sup> is significant up to the tens place while 111 has numbers up until the ones place. The answer will have no significant figures past the tens place.</li>
</ul>
</li>
</ul>
<ul>
<li>123.25 + 46.0 + 86.26 = 255.5
<ul>
<li>123.25 and 86.26 are significant until the hundredths place while 46.0 is only significant until the tenths place. The answer will be significant up until the tenths place.</li>
</ul>
</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=3" title="Edit section: Rounding rules"></a>]</span> <span class="mw-headline" id="Rounding_rules">Rounding rules</span></h2>
<p>Because significance arithmetic involves rounding, it is useful to understand a specific rounding rule that is often used when doing scientific calculations: the <a href="/wiki/Rounding#Round_half_to_even" title="Rounding">round-to-even rule</a> (also called <i>banker's rounding</i>). It is especially useful when dealing with large data sets.</p>
<p>This rule helps to eliminate the upwards skewing of data when using traditional rounding rules. Whereas traditional rounding always rounds up when the following digit is 5, bankers sometimes round down to eliminate this upwards bias.</p>
<p><i>See the article on <a href="/wiki/Rounding" title="Rounding">rounding</a> for more information on rounding rules and a detailed explanation of the round-to-even rule.</i></p>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=4" title="Edit section: Disagreements about importance"></a>]</span> <span class="mw-headline" id="Disagreements_about_importance">Disagreements about importance</span></h2>
<p>Significant figures are used extensively in high school and undergraduate courses as a shorthand for the precision with which a measurement is known. However, significant figures are <i>not</i> a perfect representation of uncertainty, and are not meant to be. Instead, they are a useful tool for avoiding expressing more information than the experimenter actually knows, and for avoiding rounding numbers in such a way as to lose precision.</p>
<p>For example, many see these as important differences between significant figure rules and uncertainty:</p>
<ul>
<li>Uncertainty is not the same as a mistake. If the outcome of a particular experiment is reported as 1.2340.056 it does not mean the observer made a mistake; it may be that the outcome is inherently statistical, and is best described by the expression 1.2340.056. To describe that outcome as 1.2340.002 would be incorrect, even though it expresses <i>less</i> uncertainty.</li>
<li>Uncertainty is not the same as insignificance, and vice versa. An uncertain number may be highly significant (example: <a rel="nofollow" class="external text" href="http://www.av8n.com/physics/uncertainty.htm#sec-extracting">signal averaging</a>). Conversely, a completely certain number may be insignificant.</li>
<li>Significance is not the same as significant <i>digits</i>. Digit-counting is not as rigorous a way to represent significance as specifying the uncertainty separately and explicitly (such as 1.2340.056).</li>
<li>Manual, algebraic <a href="/wiki/Propagation_of_uncertainty" title="Propagation of uncertainty">propagation of uncertainty</a>the nominal topic of this articleis possible, but challenging. Alternative methods include the <a rel="nofollow" class="external text" href="http://www.av8n.com/physics/uncertainty.htm#sec-crank3">crank three times</a> method and the <a href="/wiki/Monte_Carlo_method" title="Monte Carlo method">Monte Carlo method</a>. Another option is <a href="/wiki/Interval_arithmetic" title="Interval arithmetic">interval arithmetic</a>, which can provide a strict upper bound on the uncertainty, but generally it is not a tight upper bound (i.e. it does not provide a <i>best estimate</i> of the uncertainty). For most purposes, Monte Carlo is more useful than interval arithmetic<sup class="Template-Fact" style="white-space:nowrap;">[<i><a href="/wiki/Wikipedia:Citation_needed" title="Wikipedia:Citation needed"><span title="This claim needs references to reliable sources from March 2012">citation needed</span></a></i>]</sup>. <a href="/wiki/William_Kahan" title="William Kahan">Kahan</a> considers significance arithmetic to be unreliable as a form of automated error analysis <sup id="cite_ref-JavaHurt_0-0" class="reference"><a href="#cite_note-JavaHurt-0"><span>[</span>1<span>]</span></a></sup>.</li>
</ul>
<p>In order to explicitly express the uncertainty in any uncertain result, the uncertainty should be given separately.</p>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=5" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-JavaHurt-0"><b><a href="#cite_ref-JavaHurt_0-0">^</a></b> <span class="reference-text"><span class="citation web">William Kahan (1 March 1998). <a rel="nofollow" class="external text" href="http://www.cs.berkeley.edu/~wkahan/JAVAhurt.pdf">"How JAVA's Floating-Point Hurts Everyone Everywhere"</a>. pp.&#160;37-39<span class="printonly">. <a rel="nofollow" class="external free" href="http://www.cs.berkeley.edu/~wkahan/JAVAhurt.pdf">http://www.cs.berkeley.edu/~wkahan/JAVAhurt.pdf</a></span>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=bookitem&amp;rft.btitle=How+JAVA%27s+Floating-Point+Hurts+Everyone+Everywhere&amp;rft.atitle=&amp;rft.aulast=William+Kahan&amp;rft.au=William+Kahan&amp;rft.date=1+March+1998&amp;rft.pages=pp.%26nbsp%3B37-39&amp;rft_id=http%3A%2F%2Fwww.cs.berkeley.edu%2F%7Ewkahan%2FJAVAhurt.pdf&amp;rfr_id=info:sid/en.wikipedia.org:Significance_arithmetic"><span style="display: none;">&#160;</span></span></span></li>
</ol>
</div>
<ul>
<li>Daniel B. Delury. "Computation with Approximate Numbers". <i>The Mathematics Teacher</i>, v51, pp521-530. November 1958.</li>
<li><a href="/wiki/ASTM" title="ASTM" class="mw-redirect">ASTM</a> E29-06b, Standard Practice for Using Significant Digits in Test Data to Determine Conformance with Specifications</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=6" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Rounding" title="Rounding">Rounding</a></li>
<li><a href="/wiki/Propagation_of_uncertainty" title="Propagation of uncertainty">Propagation of uncertainty</a></li>
<li><a href="/wiki/Significant_figures" title="Significant figures">Significant figures</a></li>
<li><a href="/wiki/Accuracy_and_precision" title="Accuracy and precision">Accuracy and precision</a></li>
<li><a href="/wiki/MANIAC_III" title="MANIAC III">MANIAC III</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Significance_arithmetic&amp;action=&amp;section=7" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://speleotrove.com/decimal/decifaq4.html#signif">The Decimal Arithmetic FAQ  Is the decimal arithmetic significance arithmetic?</a></li>
<li><a rel="nofollow" class="external text" href="http://www.av8n.com/physics/uncertainty.htm">Advanced methods for handling uncertainty</a> and some explanations of the shortcomings of significance arithmetic and significant figures.</li>
<li><a rel="nofollow" class="external text" href="http://ostermiller.org/calc/sigfig.html">Significant Figures Calculator</a>  Displays a number with the desired number of significant digits.</li>
<li><a rel="nofollow" class="external text" href="http://www.av8n.com/physics/uncertainty.htm">Measurements and Uncertainties versus Significant Digits or Significant Figures</a>  Proper methods for expressing uncertainty, including a detailed discussion of the problems with any notion of significant digits.</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 1128/1000000
Post-expand include size: 6682/2048000 bytes
Template argument size: 2609/2048000 bytes
Expensive parser function count: 1/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:1237823-0!*!0!!en!*!* and timestamp 20120407084038 -->
