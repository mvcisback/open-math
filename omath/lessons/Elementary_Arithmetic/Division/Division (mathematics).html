<div class="dablink">"Divided" redirects here. For other uses, see <a href="/wiki/Divided_(disambiguation)" title="Divided (disambiguation)">Divided (disambiguation)</a>.</div>
<div class="dablink">For the digital implementation of mathematical division, see <a href="/wiki/Division_(digital)" title="Division (digital)">Division (digital)</a>.</div>
<div class="thumb tright">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:Divide20by4.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Divide20by4.svg/200px-Divide20by4.svg.png" width="200" height="271" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Divide20by4.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
<img class="tex" alt="20 \div 4=5" src="//upload.wikimedia.org/wikipedia/en/math/4/2/8/42861b1889a382e3413c9e08a138eb4a.png" /></div>
</div>
</div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, especially in <a href="/wiki/Elementary_arithmetic" title="Elementary arithmetic">elementary arithmetic</a>, <b>division</b> () is an arithmetic operation. Specifically, if <i>b</i> times <i>c</i> equals <i>a</i>, written:</p>
<dl>
<dd><img class="tex" alt="a = b \times c \," src="//upload.wikimedia.org/wikipedia/en/math/e/f/1/ef13bc57f63c84fec6094f86d87d4a18.png" /></dd>
</dl>
<p>where <i>b</i> is not <a href="/wiki/0_(number)" title="0 (number)">zero</a>, then <i>a</i> divided by <i>b</i> equals <i>c</i>, written:</p>
<dl>
<dd>a  b = c</dd>
</dl>
<p>For instance,</p>
<dl>
<dd>6  3 = 2</dd>
</dl>
<p>since</p>
<dl>
<dd>6 = 3 * 2</dd>
</dl>
<p>In the above expression, <i>a</i> is called the <b>dividend</b>, <i>b</i> the <b>divisor</b> and <i>c</i> the <b><a href="/wiki/Quotient" title="Quotient">quotient</a></b>.</p>
<p>Conceptually, division describes two distinct but related settings. <i>Partitioning</i> involves taking a set of size <i>a</i> and forming <i>b</i> groups that are equal in size. The size of each group formed, <i>c</i>, is the quotient of <i>a</i> and <i>b</i>. <i>Quotative</i> division involves taking a set of size <i>a</i> and forming groups of size <i>b</i>. The number of groups of this size that can be formed, <i>c</i>, is the quotient of <i>a</i> and <i>b</i>.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup></p>
<p>Teaching division usually leads to the concept of <a href="/wiki/Fraction_(mathematics)" title="Fraction (mathematics)">fractions</a> being introduced to students. Unlike <a href="/wiki/Addition" title="Addition">addition</a>, <a href="/wiki/Subtraction" title="Subtraction">subtraction</a>, and <a href="/wiki/Multiplication" title="Multiplication">multiplication</a>, the set of all <a href="/wiki/Integer" title="Integer">integers</a> is not <a href="/wiki/Closure_(mathematics)" title="Closure (mathematics)">closed</a> under division. Dividing two integers may result in a <a href="/wiki/Remainder" title="Remainder">remainder</a>. To complete the division of the remainder, the <a href="/wiki/Number_system" title="Number system" class="mw-redirect">number system</a> is extended to include fractions or <a href="/wiki/Rational_number" title="Rational number">rational numbers</a> as they are more generally called.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Notation"><span class="tocnumber">1</span> <span class="toctext">Notation</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Computing_division"><span class="tocnumber">2</span> <span class="toctext">Computing division</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Manual_methods_for_humans_to_perform_division"><span class="tocnumber">2.1</span> <span class="toctext">Manual methods for humans to perform division</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Division_by_computer_or_with_computer_assistance"><span class="tocnumber">2.2</span> <span class="toctext">Division by computer or with computer assistance</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Division_algorithm"><span class="tocnumber">3</span> <span class="toctext">Division algorithm</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Division_of_integers"><span class="tocnumber">4</span> <span class="toctext">Division of integers</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Division_of_rational_numbers"><span class="tocnumber">5</span> <span class="toctext">Division of rational numbers</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#Division_of_real_numbers"><span class="tocnumber">6</span> <span class="toctext">Division of real numbers</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#Division_by_zero"><span class="tocnumber">7</span> <span class="toctext">Division by zero</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#Division_of_complex_numbers"><span class="tocnumber">8</span> <span class="toctext">Division of complex numbers</span></a></li>
<li class="toclevel-1 tocsection-11"><a href="#Division_of_polynomials"><span class="tocnumber">9</span> <span class="toctext">Division of polynomials</span></a></li>
<li class="toclevel-1 tocsection-12"><a href="#Division_of_matrices"><span class="tocnumber">10</span> <span class="toctext">Division of matrices</span></a>
<ul>
<li class="toclevel-2 tocsection-13"><a href="#Left_and_right_division"><span class="tocnumber">10.1</span> <span class="toctext">Left and right division</span></a></li>
<li class="toclevel-2 tocsection-14"><a href="#Matrix_division_and_pseudoinverse"><span class="tocnumber">10.2</span> <span class="toctext">Matrix division and pseudoinverse</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-15"><a href="#Division_in_abstract_algebra"><span class="tocnumber">11</span> <span class="toctext">Division in abstract algebra</span></a></li>
<li class="toclevel-1 tocsection-16"><a href="#Division_and_calculus"><span class="tocnumber">12</span> <span class="toctext">Division and calculus</span></a></li>
<li class="toclevel-1 tocsection-17"><a href="#See_also"><span class="tocnumber">13</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-18"><a href="#References"><span class="tocnumber">14</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-19"><a href="#External_links"><span class="tocnumber">15</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=1" title="Edit section: Notation"></a>]</span> <span class="mw-headline" id="Notation">Notation</span></h2>
<p>Division is often shown in algebra and science by placing the <i>dividend</i> over the <i>divisor</i> with a horizontal line, also called a <a href="/wiki/Vinculum_(symbol)" title="Vinculum (symbol)">vinculum</a> or <a href="/wiki/Fraction" title="Fraction">fraction bar</a>, between them. For example, <i>a</i> divided by <i>b</i> is written</p>
<dl>
<dd><img class="tex" alt="\frac ab" src="//upload.wikimedia.org/wikipedia/en/math/2/e/0/2e06f98701af77a75327d4da74c11479.png" /></dd>
</dl>
<p>This can be read out loud as "a divided by b", "a by b" or "a over b". A way to express division all on one line is to write the <i>dividend</i> (or numerator), then a <a href="/wiki/Slash_(punctuation)" title="Slash (punctuation)">slash</a>, then the <i>divisor</i> (or denominator), like this:</p>
<dl>
<dd><img class="tex" alt="a/b\," src="//upload.wikimedia.org/wikipedia/en/math/e/b/6/eb697ee25a19cdf9e9f2ab28e5e76698.png" /></dd>
</dl>
<p>This is the usual way to specify division in most computer <a href="/wiki/Programming_language" title="Programming language">programming languages</a> since it can easily be typed as a simple sequence of <a href="/wiki/ASCII" title="ASCII">ASCII</a> characters.</p>
<p>A typographical variation halfway between these two forms uses a <a href="/wiki/Solidus_(punctuation)" title="Solidus (punctuation)" class="mw-redirect">solidus</a> (fraction slash) but elevates the dividend, and lowers the divisor:</p>
<dl>
<dd><span class="frac nowrap"><sup><i>a</i></sup><sub><i>b</i></sub></span></dd>
</dl>
<p>Any of these forms can be used to display a <a href="/wiki/Fraction_(mathematics)" title="Fraction (mathematics)">fraction</a>. A fraction is a division expression where both dividend and divisor are <a href="/wiki/Integer" title="Integer">integers</a> (although typically called the <i>numerator</i> and <i>denominator</i>), and there is no implication that the division needs to be evaluated further. A second way to show division is to use the <a href="/wiki/Obelus" title="Obelus">obelus</a> (or division sign), common in arithmetic, in this manner:</p>
<dl>
<dd><img class="tex" alt="a \div b" src="//upload.wikimedia.org/wikipedia/en/math/2/1/c/21c3f494e6dcf99dd72fab560c4a8fee.png" /></dd>
</dl>
<p>This form is infrequent except in elementary arithmetic. The obelus is also used alone to represent the division operation itself, as for instance as a label on a key of a <a href="/wiki/Calculator" title="Calculator">calculator</a>.</p>
<p>In some non-<a href="/wiki/English_language" title="English language">English</a>-speaking cultures, "a divided by b" is written <i>a</i>&#160;: <i>b</i>. However, in English usage the <a href="/wiki/Colon_(punctuation)" title="Colon (punctuation)">colon</a> is restricted to expressing the related concept of <a href="/wiki/Ratio" title="Ratio">ratios</a> (then "a is to b").</p>
<p>In elementary mathematics the notation <img class="tex" alt="b)~a" src="//upload.wikimedia.org/wikipedia/en/math/f/2/4/f24b736dfff973420565930a40ef9b39.png" /> or <img class="tex" alt="b)\overline{~a~}" src="//upload.wikimedia.org/wikipedia/en/math/9/3/7/93700df1e4d9f9598f0343d698235973.png" /> is used to denote <i>a</i> divided by <i>b</i>. This notation was first introduced by <a href="/wiki/Michael_Stifel" title="Michael Stifel">Michael Stifel</a> in <i>Arithmetica integra</i>, published in 1544.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=2" title="Edit section: Computing division"></a>]</span> <span class="mw-headline" id="Computing_division">Computing division</span></h2>
<h3><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=3" title="Edit section: Manual methods for humans to perform division"></a>]</span> <span class="mw-headline" id="Manual_methods_for_humans_to_perform_division">Manual methods for humans to perform division</span></h3>
<p>Division is often introduced through the notion of "sharing out" a set of objects, for example a pile of sweets, into a number of equal portions. Distributing the objects several at a time in each round of sharing to each portion leads to the idea of "<a href="/wiki/Chunking_(division)" title="Chunking (division)">chunking</a>", i.e. division by repeated subtraction.</p>
<p>More systematic and more efficient (but also more formalised and more rule-based, and more removed from an overall holistic picture of what division is achieving), a person who knows the <a href="/wiki/Multiplication_tables" title="Multiplication tables" class="mw-redirect">multiplication tables</a> can divide two integers using pencil and paper using the method of <a href="/wiki/Short_division" title="Short division">short division</a>, if the divisor is simple. <a href="/wiki/Long_division" title="Long division">Long division</a> is used for larger integer divisors. If the dividend has a <a href="/wiki/Fraction_(mathematics)" title="Fraction (mathematics)">fractional</a> part (expressed as a <a href="/wiki/Decimal_fraction" title="Decimal fraction" class="mw-redirect">decimal fraction</a>), one can continue the algorithm past the ones place as far as desired. If the divisor has a fractional part, we can restate the problem by moving the decimal to the right in both numbers until the divisor has no fraction.</p>
<p>A person can calculate division with an <a href="/wiki/Abacus" title="Abacus">abacus</a> by repeatedly placing the dividend on the abacus, and then subtracting the divisor the offset of each digit in the result, counting the number of divisions possible at each offset.</p>
<p>A person can use <a href="/wiki/Logarithm_tables" title="Logarithm tables" class="mw-redirect">logarithm tables</a> to divide two numbers, by subtracting the two numbers' logarithms, then looking up the antilogarithm of the result.</p>
<p>A person can calculate division with a <a href="/wiki/Slide_rule" title="Slide rule">slide rule</a> by aligning the divisor on the C scale with the dividend on the D scale. The quotient can be found on the D scale where it is aligned with the left index on the C scale. The user is responsible, however, for mentally keeping track of the decimal point.</p>
<h3><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=4" title="Edit section: Division by computer or with computer assistance"></a>]</span> <span class="mw-headline" id="Division_by_computer_or_with_computer_assistance">Division by computer or with computer assistance</span></h3>
<p>Modern computers compute division by methods that are faster than long division: see <a href="/wiki/Division_(digital)" title="Division (digital)">Division (digital)</a>.</p>
<p>In <a href="/wiki/Modular_arithmetic" title="Modular arithmetic">modular arithmetic</a>, some numbers have a <a href="/wiki/Modular_multiplicative_inverse" title="Modular multiplicative inverse">multiplicative inverse</a> with respect to the modulus. We can calculate division by multiplication in such a case. This approach is useful in computers that do not have a fast division instruction.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=5" title="Edit section: Division algorithm"></a>]</span> <span class="mw-headline" id="Division_algorithm">Division algorithm</span></h2>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Division_algorithm" title="Division algorithm">Division algorithm</a></div>
<p>The <a href="/wiki/Division_algorithm" title="Division algorithm">division algorithm</a> is a mathematical theorem that precisely expresses the outcome of the usual process of division of integers. In particular, the theorem asserts that integers called the quotient <i>q</i> and remainder <i>r</i> always exist and that they are uniquely determined by the dividend <i>a</i> and divisor <i>d</i>, with <i>d</i>  0. Formally, the theorem is stated as follows: There exist <a href="/wiki/Uniqueness_quantification" title="Uniqueness quantification">unique</a> integers <i>q</i> and <i>r</i> such that <i>a</i> = <i>qd</i> + <i>r</i> and 0  <i>r</i> &lt; | <i>d</i> |, where | <i>d</i> | denotes the <a href="/wiki/Absolute_value" title="Absolute value">absolute value</a> of <i>d</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=6" title="Edit section: Division of integers"></a>]</span> <span class="mw-headline" id="Division_of_integers">Division of integers</span></h2>
<p>Division of integers is not <a href="/wiki/Closure_(mathematics)" title="Closure (mathematics)">closed</a>. Apart from division by zero being undefined, the quotient is not an integer unless the dividend is an integer multiple of the divisor. For example 26 cannot be divided by 10 to give an integer. Such a case uses one of four approaches:</p>
<ol>
<li>Say that 26 cannot be divided by 10; division becomes a <a href="/wiki/Partial_function" title="Partial function">partial function</a>.</li>
<li>Give the answer as a <a href="/wiki/Decimal_fraction" title="Decimal fraction" class="mw-redirect">decimal fraction</a> or a <a href="/wiki/Mixed_number" title="Mixed number" class="mw-redirect">mixed number</a>, so <img class="tex" alt="\tfrac{26}{10} = 2.6" src="//upload.wikimedia.org/wikipedia/en/math/4/1/f/41f4cae83366c8bac6d08cb8e0c512f3.png" /> or <img class="tex" alt="\tfrac{26}{10} = 2 \tfrac 35." src="//upload.wikimedia.org/wikipedia/en/math/6/0/2/6028f43d70e29b343e93495f9c9c9471.png" /> This is the approach usually taken in mathematics.</li>
<li>Give the answer as an integer <i><a href="/wiki/Quotient" title="Quotient">quotient</a></i> and a <i><a href="/wiki/Remainder" title="Remainder">remainder</a></i>, so <img class="tex" alt="\tfrac{26}{10} = 2 \mbox{ remainder } 6." src="//upload.wikimedia.org/wikipedia/en/math/6/8/6/68635d9cbb2f3bae7a51b226830ced15.png" /></li>
<li>Give the integer quotient as the answer, so <img class="tex" alt="\tfrac{26}{10} = 2." src="//upload.wikimedia.org/wikipedia/en/math/a/f/d/afd2f7b11312c6666b6c92ca374f814e.png" /> This is sometimes called <i>integer division</i>.</li>
</ol>
<p>One has to be careful when performing division of integers in a <a href="/wiki/Computer_program" title="Computer program">computer program</a>. Some <a href="/wiki/Programming_language" title="Programming language">programming languages</a>, such as <a href="/wiki/C_(programming_language)" title="C (programming language)">C</a>, treat integer division as in case 4 above, so the answer is an integer. Other languages, such as <a href="/wiki/MATLAB" title="MATLAB">MATLAB</a>, first convert integers to rational numbers, then return a rational number as the answer, as in case 2 above.</p>
<p>Names and symbols used for integer division include div, /, \, and&#160;%. Definitions vary regarding integer division when the dividend or the divisor is negative: rounding may be toward zero (so called T-division) or toward <a href="/wiki/Extended_real_number_line" title="Extended real number line"></a> (F-division); rarer styles can occur - see <a href="/wiki/Modulo_operation" title="Modulo operation">Modulo operation</a> for the details.</p>
<p><a href="/wiki/Divisibility_rule" title="Divisibility rule">Divisibility rules</a> can sometimes be used to quickly determine whether one integer divides exactly into another.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=7" title="Edit section: Division of rational numbers"></a>]</span> <span class="mw-headline" id="Division_of_rational_numbers">Division of rational numbers</span></h2>
<p>The result of dividing two <a href="/wiki/Rational_number" title="Rational number">rational numbers</a> is another rational number when the divisor is not 0. We may define division of two rational numbers <i>p</i>/<i>q</i> and <i>r</i>/<i>s</i> by</p>
<dl>
<dd><img class="tex" alt="{p/q \over r/s} = {p \over q} \times {s \over r} = {ps \over qr}." src="//upload.wikimedia.org/wikipedia/en/math/7/2/5/7257d434e5e5b3cee9a6859f8a46c096.png" /></dd>
</dl>
<p>All four quantities are integers, and only <i>p</i> may be 0. This definition ensures that division is the inverse operation of <a href="/wiki/Multiplication" title="Multiplication">multiplication</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=8" title="Edit section: Division of real numbers"></a>]</span> <span class="mw-headline" id="Division_of_real_numbers">Division of real numbers</span></h2>
<p>Division of two <a href="/wiki/Real_number" title="Real number">real numbers</a> results in another real number when the divisor is not 0. It is defined such <i>a</i>/<i>b</i> = <i>c</i> if and only if <i>a</i> = <i>cb</i> and <i>b</i>  0.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=9" title="Edit section: Division by zero"></a>]</span> <span class="mw-headline" id="Division_by_zero">Division by zero</span></h2>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Division_by_zero" title="Division by zero">Division by zero</a></div>
<p>Division of any number by <a href="/wiki/Zero" title="Zero" class="mw-redirect">zero</a> (where the divisor is zero) is undefined. This is because zero multiplied by any finite number always results in a <a href="/wiki/Multiplication" title="Multiplication">product</a> of zero. Entry of such an expression into most <a href="/wiki/Calculator" title="Calculator">calculators</a> produces an error message.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=10" title="Edit section: Division of complex numbers"></a>]</span> <span class="mw-headline" id="Division_of_complex_numbers">Division of complex numbers</span></h2>
<p>Dividing two <a href="/wiki/Complex_number" title="Complex number">complex numbers</a> results in another complex number when the divisor is not 0, defined thus:</p>
<dl>
<dd><img class="tex" alt="{p + iq \over r + is} = {p r + q s \over r^2 + s^2} + i{q r - p s \over r^2 + s^2}." src="//upload.wikimedia.org/wikipedia/en/math/6/4/b/64b35087f7fad12d194a579e67803732.png" /></dd>
</dl>
<p>All four quantities are real numbers. <i>r</i> and <i>s</i> may not both be 0.</p>
<p>Division for complex numbers expressed in polar form is simpler than the definition above:</p>
<dl>
<dd><img class="tex" alt="{p e^{iq} \over r e^{is}} = {p \over r}e^{i(q - s)}." src="//upload.wikimedia.org/wikipedia/en/math/b/0/8/b0840f5a5ae658e81c037f07e66862ea.png" /></dd>
</dl>
<p>Again all four quantities are real numbers. <i>r</i> may not be 0.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=11" title="Edit section: Division of polynomials"></a>]</span> <span class="mw-headline" id="Division_of_polynomials">Division of polynomials</span></h2>
<p>One can define the division operation for <a href="/wiki/Polynomial" title="Polynomial">polynomials</a>. Then, as in the case of integers, one has a remainder. See <a href="/wiki/Polynomial_long_division" title="Polynomial long division">polynomial long division</a> or <a href="/wiki/Synthetic_division" title="Synthetic division">synthetic division</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=12" title="Edit section: Division of matrices"></a>]</span> <span class="mw-headline" id="Division_of_matrices">Division of matrices</span></h2>
<p>One can define a division operation for matrices. The usual way to do this is to define <span class="nowrap"><i>A</i> / <i>B</i> = <i>AB</i><sup>1</sup></span>, where <span class="nowrap"><i>B</i><sup>1</sup></span> denotes the <a href="/wiki/Inverse_matrix" title="Inverse matrix" class="mw-redirect">inverse</a> of <i>B</i>, but it is far more common to write out <span class="nowrap"><i>AB</i><sup>1</sup></span> explicitly to avoid confusion.</p>
<h3><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=13" title="Edit section: Left and right division"></a>]</span> <span class="mw-headline" id="Left_and_right_division">Left and right division</span></h3>
<p>Because <a href="/wiki/Matrix_multiplication" title="Matrix multiplication">matrix multiplication</a> is not <a href="/wiki/Commutative" title="Commutative" class="mw-redirect">commutative</a>, one can also define a <a href="/wiki/Left_division" title="Left division" class="mw-redirect">left division</a> or so-called <i>backslash-division</i> as <span class="nowrap"><i>A</i> \ <i>B</i> = <i>A</i><sup>1</sup><i>B</i></span>. For this to be well defined, <span class="nowrap"><i>B</i><sup>1</sup></span> need not exist, however <span class="nowrap"><i>A</i><sup>1</sup></span> does need to exist. To avoid confusion, division as defined by <span class="nowrap"><i>A</i> / <i>B</i> = <i>AB</i><sup>1</sup></span> is sometimes called <i>right division</i> or <i>slash-division</i> in this context.</p>
<p>Note that with left and right division defined this way, <span class="nowrap"><i>A</i>/(<i>BC</i>)</span> is in general not the same as <span class="nowrap">(<i>A</i>/<i>B</i>)/<i>C</i></span> and nor is <span class="nowrap">(<i>AB</i>)\<i>C</i></span> the same as <span class="nowrap"><i>A</i>\(<i>B</i>\<i>C</i>)</span>, but <span class="nowrap"><i>A</i>/(<i>BC</i>) = (<i>A</i>/<i>C</i>)/<i>B</i></span> and <span class="nowrap">(<i>AB</i>)\<i>C</i> = <i>B</i>\(<i>A</i>\<i>C</i>)</span>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=14" title="Edit section: Matrix division and pseudoinverse"></a>]</span> <span class="mw-headline" id="Matrix_division_and_pseudoinverse">Matrix division and pseudoinverse</span></h3>
<p>To avoid problems when <span class="nowrap"><i>A</i><sup>1</sup></span> and/or <span class="nowrap"><i>B</i><sup>1</sup></span> do not exist, division can also be defined as multiplication with the <a href="/wiki/Moore%E2%80%93Penrose_pseudoinverse" title="MoorePenrose pseudoinverse">pseudoinverse</a>, i.e., <span class="nowrap"><i>A</i> / <i>B</i> = <i>AB</i><sup>+</sup></span> and <span class="nowrap"><i>A</i> \ <i>B</i> = <i>A</i><sup>+</sup><i>B</i></span>, where <span class="nowrap"><i>A</i><sup>+</sup></span> and <span class="nowrap"><i>B</i><sup>+</sup></span> denote the pseudoinverse of <i>A</i> and <i>B</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=15" title="Edit section: Division in abstract algebra"></a>]</span> <span class="mw-headline" id="Division_in_abstract_algebra">Division in abstract algebra</span></h2>
<p>In <a href="/wiki/Abstract_algebra" title="Abstract algebra">abstract algebras</a> such as <a href="/wiki/Matrix_(mathematics)" title="Matrix (mathematics)">matrix</a> algebras and <a href="/wiki/Quaternion" title="Quaternion">quaternion</a> algebras, fractions such as <img class="tex" alt="{a \over b}" src="//upload.wikimedia.org/wikipedia/en/math/f/2/8/f28931973df5c03bd73635fb4e84d136.png" /> are typically defined as <img class="tex" alt="a \cdot {1 \over b}" src="//upload.wikimedia.org/wikipedia/en/math/f/9/1/f910bb957d79b5413442a29f84949831.png" /> or <img class="tex" alt="a \cdot b^{-1}" src="//upload.wikimedia.org/wikipedia/en/math/5/9/5/595766b69dfeec23bdce04d42e55f670.png" /> where <img class="tex" alt="b" src="//upload.wikimedia.org/wikipedia/en/math/9/2/e/92eb5ffee6ae2fec3ad71c777531578f.png" /> is presumed to be an invertible element (i.e. there exists a <a href="/wiki/Multiplicative_inverse" title="Multiplicative inverse">multiplicative inverse</a> <img class="tex" alt="b^{-1}" src="//upload.wikimedia.org/wikipedia/en/math/8/3/a/83a2628e12eb8f3c30e16c050313498a.png" /> such that <img class="tex" alt="bb^{-1} = b^{-1}b = 1" src="//upload.wikimedia.org/wikipedia/en/math/7/4/5/745a13569684fd379a81fe449b506916.png" /> where <img class="tex" alt="1" src="//upload.wikimedia.org/wikipedia/en/math/c/4/c/c4ca4238a0b923820dcc509a6f75849b.png" /> is the multiplicative identity). In an <a href="/wiki/Integral_domain" title="Integral domain">integral domain</a> where such elements may not exist, <i>division</i> can still be performed on equations of the form <img class="tex" alt="ab = ac" src="//upload.wikimedia.org/wikipedia/en/math/0/8/b/08bb7c60b53f64f1d83c80c82adf30c5.png" /> or <img class="tex" alt="ba = ca" src="//upload.wikimedia.org/wikipedia/en/math/2/7/0/270685ebf67c066453025d58c5aeb4be.png" /> by left or right cancellation, respectively. More generally "division" in the sense of "cancellation" can be done in any <a href="/wiki/Ring_(mathematics)" title="Ring (mathematics)">ring</a> with the aforementioned cancellation properties. If such a ring is finite, then by an application of the <a href="/wiki/Pigeonhole_principle" title="Pigeonhole principle">pigeonhole principle</a>, every nonzero element of the ring is invertible, so <i>division</i> by any nonzero element is possible in such a ring. To learn about when <i>algebras</i> (in the technical sense) have a division operation, refer to the page on <a href="/wiki/Division_algebra" title="Division algebra">division algebras</a>. In particular <a href="/wiki/Bott_periodicity" title="Bott periodicity" class="mw-redirect">Bott periodicity</a> can be used to show that any <a href="/wiki/Real_number" title="Real number">real</a> <a href="/wiki/Normed_division_algebra" title="Normed division algebra">normed division algebra</a> must be <a href="/wiki/Isomorphic" title="Isomorphic" class="mw-redirect">isomorphic</a> to either the real numbers <b>R</b>, the <a href="/wiki/Complex_number" title="Complex number">complex numbers</a> <b>C</b>, the <a href="/wiki/Quaternion" title="Quaternion">quaternions</a> <b>H</b>, or the <a href="/wiki/Octonion" title="Octonion">octonions</a> <b>O</b>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=16" title="Edit section: Division and calculus"></a>]</span> <span class="mw-headline" id="Division_and_calculus">Division and calculus</span></h2>
<p>The <a href="/wiki/Derivative" title="Derivative">derivative</a> of the quotient of two functions is given by the <a href="/wiki/Quotient_rule" title="Quotient rule">quotient rule</a>:</p>
<dl>
<dd><img class="tex" alt="{\left(\frac fg\right)}' = \frac{f'g - fg'}{g^2}." src="//upload.wikimedia.org/wikipedia/en/math/3/d/5/3d540e1ae51531dcacaafb3b81a4d464.png" /></dd>
</dl>
<p>There is no general method to <a href="/wiki/Integral" title="Integral">integrate</a> the quotient of two functions.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=17" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Rod_calculus#Division" title="Rod calculus">400AD Sunzi division algorithm</a></li>
<li><a href="/wiki/Galley_division" title="Galley division">Galley division</a></li>
<li><a href="/wiki/Division_by_two" title="Division by two">Division by two</a></li>
<li><a href="/wiki/Field_(mathematics)" title="Field (mathematics)">Field</a></li>
<li><a href="/wiki/Group_(mathematics)" title="Group (mathematics)">Group</a></li>
<li><a href="/wiki/Inverse_element" title="Inverse element">Inverse element</a></li>
<li><a href="/wiki/Order_of_operations" title="Order of operations">Order of operations</a></li>
<li><a href="/wiki/Quasigroup" title="Quasigroup">Quasigroup</a> (left division)</li>
<li><a href="/wiki/Repeating_decimal" title="Repeating decimal">Repeating decimal</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=18" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">Fosnot and Dolk 2001. <i>Young Mathematicians at Work: Constructing Multiplication and Division</i>. Portsmouth, NH: Heinemann.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text"><a rel="nofollow" class="external text" href="http://jeff560.tripod.com/operation.html">Earliest Uses of Symbols of Operation</a>, Jeff MIller</span></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Division_(mathematics)&amp;action=&amp;section=19" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<table class="metadata mbox-small plainlinks" style="border:1px solid #aaa; background-color:#f9f9f9;">
<tr>
<td class="mbox-image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/4/4a/Commons-logo.svg/30px-Commons-logo.svg.png" width="30" height="40" /></td>
<td class="mbox-text" style="">Wikimedia Commons has media related to: <i><b><a class="external text" href="//commons.wikimedia.org/wiki/Category:Division_(mathematics)">Division (mathematics)</a></b></i></td>
</tr>
</table>
<ul>
<li><a rel="nofollow" class="external text" href="http://planetmath.org/?op=getobj&amp;from=objects&amp;id=6148">Division</a>, <a href="/wiki/PlanetMath" title="PlanetMath">PlanetMath.org</a>.</li>
<li><a rel="nofollow" class="external text" href="http://webhome.idirect.com/~totton/abacus/pages.htm#Division1">Division on a Japanese abacus</a> selected from <a rel="nofollow" class="external text" href="http://webhome.idirect.com/~totton/abacus/">Abacus: Mystery of the Bead</a></li>
<li><a rel="nofollow" class="external text" href="http://webhome.idirect.com/~totton/suanpan/sh_div/">Chinese Short Division Techniques on a Suan Pan</a></li>
<li><a rel="nofollow" class="external text" href="http://www.math.wichita.edu/history/topics/arithmetic.html#div">Rules of divisibility</a></li>
</ul>
<table class="metadata plainlinks ambox ambox-style ambox-More_footnotes" style="">
<tr>
<td class="mbox-image">
<div style="width: 52px;"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Text_document_with_red_question_mark.svg/40px-Text_document_with_red_question_mark.svg.png" width="40" height="40" /></div>
</td>
<td class="mbox-text" style="">This article includes a <a href="/wiki/Wikipedia:Citing_sources" title="Wikipedia:Citing sources">list of references</a>, but <b>its sources remain unclear because it has insufficient <a href="/wiki/Wikipedia:INCITE" title="Wikipedia:INCITE" class="mw-redirect">inline citations</a></b>. Please help to <a href="/wiki/Wikipedia:WikiProject_Fact_and_Reference_Check" title="Wikipedia:WikiProject Fact and Reference Check">improve</a> this article by <a href="/wiki/Wikipedia:When_to_cite" title="Wikipedia:When to cite">introducing</a> more precise citations. <small><i>(February 2008)</i></small></td>
</tr>
</table>
<table cellspacing="0" class="navbox" style="border-spacing:0;;">
<tr>
<td style="padding:2px;">
<table cellspacing="0" class="nowraplinks collapsible autocollapse navbox-inner" style="border-spacing:0;background:transparent;color:inherit;;">
<tr>
<th scope="col" style=";" class="navbox-title" colspan="2">
<div class="noprint plainlinks hlist navbar mini" style="">
<ul>
<li class="nv-view"><a href="/wiki/Template:Elementary_arithmetic" title="Template:Elementary arithmetic"><span title="View this template" style=";;background:none transparent;border:none;">v</span></a></li>
<li class="nv-talk"><a href="/wiki/Template_talk:Elementary_arithmetic" title="Template talk:Elementary arithmetic"><span title="Discuss this template" style=";;background:none transparent;border:none;">t</span></a></li>
<li class="nv-"><a class="external text" href="//en.wikipedia.org/w/index.php?title=Template:Elementary_arithmetic&amp;action="><span title="Edit this template" style=";;background:none transparent;border:none;">e</span></a></li>
</ul>
</div>
<div class="" style="font-size:110%;"><a href="/wiki/Elementary_arithmetic" title="Elementary arithmetic">Elementary arithmetic</a></div>
</th>
</tr>
<tr style="height:2px;">
<td></td>
</tr>
<tr>
<td colspan="2" style="width:100%;padding:0px;;;background:transparent;color:inherit;" class="navbox-list navbox-odd">
<div style="padding:0px;">
<table cellspacing="0" class="navbox-columns-table" style="text-align:left;width:auto; margin-left:auto; margin-right:auto;">
<tr style="vertical-align:top;">
<td style="width:5em;">&#160;&#160;&#160;</td>
<td style="padding:0px;text-align: center;;;width:25%;">
<div>
<p><a href="/wiki/Addition" title="Addition"><img alt="Symbol support vote.svg" src="//upload.wikimedia.org/wikipedia/en/thumb/9/94/Symbol_support_vote.svg/50px-Symbol_support_vote.svg.png" width="50" height="51" /></a><br />
<a href="/wiki/Addition" title="Addition">Addition</a><br />
<a href="/wiki/Addition" title="Addition"><font size="5">+</font></a></p>
</div>
</td>
<td style="border-left:2px solid #fdfdfd;padding:0px;text-align: center;;;width:25%;">
<div>
<p><a href="/wiki/Subtraction" title="Subtraction"><img alt="Symbol oppose vote.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Symbol_oppose_vote.svg/50px-Symbol_oppose_vote.svg.png" width="50" height="51" /></a><br />
<a href="/wiki/Subtraction" title="Subtraction">Subtraction</a><br />
<a href="/wiki/Subtraction" title="Subtraction"><font size="5"></font></a></p>
</div>
</td>
<td style="border-left:2px solid #fdfdfd;padding:0px;text-align: center;;;width:25%;">
<div>
<p><a href="/wiki/Multiplication" title="Multiplication"><img alt="Symbol multiplication vote.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Symbol_multiplication_vote.svg/50px-Symbol_multiplication_vote.svg.png" width="50" height="51" /></a><br />
<a href="/wiki/Multiplication" title="Multiplication">Multiplication</a><br />
<a href="/wiki/Multiplication" title="Multiplication"><font size="5"></font></a></p>
</div>
</td>
<td style="border-left:2px solid #fdfdfd;padding:0px;text-align: center;;;width:25%;">
<div>
<p><a href="/wiki/Division_(mathematics)" title="Division (mathematics)"><img alt="Symbol divide vote.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Symbol_divide_vote.svg/50px-Symbol_divide_vote.svg.png" width="50" height="51" /></a><br />
<strong class="selflink">Division</strong><br />
<font size="5"><strong class="selflink"></strong></font></p>
</div>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>


<!-- 
NewPP limit report
Preprocessor node count: 1756/1000000
Post-expand include size: 21618/2048000 bytes
Template argument size: 7413/2048000 bytes
Expensive parser function count: 2/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:53696-0!0!0!!en!4!* and timestamp 20120414192200 -->
