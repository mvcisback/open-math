<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, <b>division by <a href="/wiki/Two" title="Two" class="mw-redirect">two</a></b> or <b>halving</b> has also been called <b>mediation</b> or <b>dimidiation</b>.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup> The treatment of this as a different operation from multiplication and division by other numbers goes back to the ancient Egyptians, whose <a href="/wiki/Ancient_Egyptian_multiplication" title="Ancient Egyptian multiplication">multiplication algorithm</a> used division by two as one of its fundamental steps.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup> Some mathematicians as late as the sixteenth century continued to view halving as a separate operation,<sup id="cite_ref-2" class="reference"><a href="#cite_note-2"><span>[</span>3<span>]</span></a></sup><sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>4<span>]</span></a></sup> and it often continues to be treated separately in modern <a href="/wiki/Computer_programming" title="Computer programming">computer programming</a>.<sup id="cite_ref-WC00_4-0" class="reference"><a href="#cite_note-WC00-4"><span>[</span>5<span>]</span></a></sup> Performing this operation is simple in <a href="/wiki/Decimal_arithmetic" title="Decimal arithmetic" class="mw-redirect">decimal arithmetic</a>, in the <a href="/wiki/Binary_numeral_system" title="Binary numeral system">binary numeral system</a> used in computer programming, and in other even-numbered <a href="/wiki/Numeral_system" title="Numeral system">bases</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Binary"><span class="tocnumber">1</span> <span class="toctext">Binary</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Binary_floating_point"><span class="tocnumber">2</span> <span class="toctext">Binary floating point</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Decimal"><span class="tocnumber">3</span> <span class="toctext">Decimal</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#See_also"><span class="tocnumber">4</span> <span class="toctext">See also</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Division_by_two&amp;action=&amp;section=1" title="Edit section: Binary"></a>]</span> <span class="mw-headline" id="Binary">Binary</span></h2>
<p>In binary arithmetic, division by two can be performed by a <a href="/wiki/Bit_shift" title="Bit shift" class="mw-redirect">bit shift</a> operation that shifts the number one place to the right. This is a form of <a href="/wiki/Strength_reduction" title="Strength reduction">strength reduction</a> optimization. For example, 1101001 in binary (the decimal number 105), shifted one place to the right, is 110100 (the decimal number 52): the lowest order bit, a 1, is removed. Similarly, division by any <a href="/wiki/Power_of_two" title="Power of two">power of two</a> 2<sup><i>k</i></sup> may be performed by right-shifting <i>k</i> positions. Because bit shifts are often much faster operations than division, replacing a division by a shift in this way can be a helpful step in <a href="/wiki/Program_optimization" title="Program optimization">program optimization</a>.<sup id="cite_ref-WC00_4-1" class="reference"><a href="#cite_note-WC00-4"><span>[</span>5<span>]</span></a></sup> However, for the sake of <a href="/wiki/Software_portability" title="Software portability">software portability</a> and readability, it is often best to write programs using the division operation and trust in the <a href="/wiki/Compiler" title="Compiler">compiler</a> to perform this replacement.<sup id="cite_ref-5" class="reference"><a href="#cite_note-5"><span>[</span>6<span>]</span></a></sup> An example from <a href="/wiki/Common_Lisp" title="Common Lisp">Common Lisp</a>:</p>
<div dir="ltr" class="mw-geshi mw-content-ltr">
<div class="lisp source-lisp">
<pre class="de1">
 <span class="br0">(</span><span class="kw1">setq</span> number #b1101001<span class="br0">)</span>   <span class="co1">; #b1101001    105</span>
 <span class="br0">(</span>ash number -<span class="nu0">1</span><span class="br0">)</span>           <span class="co1">; #b0110100    105 &gt;&gt; 1  52</span>
 <span class="br0">(</span>ash number -<span class="nu0">4</span><span class="br0">)</span>           <span class="co1">; #b0000110    105 &gt;&gt; 4  105 / 2  6</span>
</pre></div>
</div>
<p>The above statements, however, are not always true when dealing with dividing <a href="/wiki/Signed_number_representations" title="Signed number representations">signed</a> binary numbers. Shifting right by 1 bit will divide by two, always rounding down. However, in some languages, division of signed binary numbers round towards 0 (which, if the result is negative, means it rounds up). For example, <a href="/wiki/Java_(programming_language)" title="Java (programming language)">Java</a> is one such language: in Java, <code>-3 / 2</code> evaluates to <code>-1</code>, whereas <code>-3 &gt;&gt; 1</code> evaluates to <code>-2</code>. So in this case, the compiler <i>cannot</i> optimize division by two by replacing it by a bit shift, when the dividend could possibly be negative.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_by_two&amp;action=&amp;section=2" title="Edit section: Binary floating point"></a>]</span> <span class="mw-headline" id="Binary_floating_point">Binary floating point</span></h2>
<p>In binary <a href="/wiki/Floating-point_arithmetic" title="Floating-point arithmetic" class="mw-redirect">floating-point arithmetic</a>, division by two can be performed by decreasing the exponent by one (as long as the result is not a <a href="/wiki/Subnormal_number" title="Subnormal number" class="mw-redirect">subnormal number</a>). Many programming languages provide functions that can be used to divide a floating point number by a power of two. For example, the <a href="/wiki/Java_(programming_language)" title="Java (programming language)">Java programming language</a> provides the method <code>java.lang.Math.scalb</code> for scaling by a power of two,<sup id="cite_ref-6" class="reference"><a href="#cite_note-6"><span>[</span>7<span>]</span></a></sup> and the <a href="/wiki/C_(programming_language)" title="C (programming language)">C programming language</a> provides the function <code>ldexp</code> for the same purpose.<sup id="cite_ref-7" class="reference"><a href="#cite_note-7"><span>[</span>8<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Division_by_two&amp;action=&amp;section=3" title="Edit section: Decimal"></a>]</span> <span class="mw-headline" id="Decimal">Decimal</span></h2>
<p>The following <a href="/wiki/Algorithm" title="Algorithm">algorithm</a> is for decimal. However, it can be used as a model to construct an algorithm for taking half of any number <i>N</i> in any <a href="/wiki/Even" title="Even">even</a> base.</p>
<ul>
<li>Write out <i>N</i>, putting a zero to its left.</li>
<li>Go through the digits of <i>N</i> in overlapping pairs, writing down digits of the result from the following table.</li>
</ul>
<table border="1" cellspacing="0" cellpadding="2">
<tr>
<th>If first digit is</th>
<td>Even</td>
<td>Even</td>
<td>Even</td>
<td>Even</td>
<td>Even</td>
<td>Odd</td>
<td>Odd</td>
<td>Odd</td>
<td>Odd</td>
<td>Odd</td>
</tr>
<tr>
<th>And second digit is</th>
<td>0 or 1</td>
<td>2 or 3</td>
<td>4 or 5</td>
<td>6 or 7</td>
<td>8 or 9</td>
<td>0 or 1</td>
<td>2 or 3</td>
<td>4 or 5</td>
<td>6 or 7</td>
<td>8 or 9</td>
</tr>
<tr>
<th>Write</th>
<td>0</td>
<td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
</tr>
</table>
<p>Example: 1738/2=?</p>
<p>Write 01738. We will now work on finding the result.</p>
<ul>
<li>01: even digit followed by 1, write 0.</li>
<li>17: odd digit followed by 7, write 8.</li>
<li>73: odd digit followed by 3, write 6.</li>
<li>38: odd digit followed by 8, write 9.</li>
</ul>
<p>Result: 0869.</p>
<p>From the example one can see that <a href="/wiki/0_is_even" title="0 is even" class="mw-redirect">0 is even</a>.</p>
<p>If the last digit of <i>N</i> is <a href="/wiki/Even_and_odd_numbers" title="Even and odd numbers" class="mw-redirect">odd</a> digit one should add 0.5 to the result.</p>
<h2><span class="section">[<a href="/w/index.php?title=Division_by_two&amp;action=&amp;section=4" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/One_half" title="One half">One half</a></li>
<li><a href="/wiki/Median" title="Median">Median</a>, a value that splits a set of data values into two equal subsets</li>
<li><a href="/wiki/Bisection" title="Bisection">Bisection</a>, the partition of a geometric object into two equal halves</li>
<li><a href="/wiki/Dimidiation" title="Dimidiation">Dimidiation</a>, a heraldic method of joining two coats of arms by splitting their designs into halves</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Division_by_two&amp;action=&amp;section=5" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFSteele1922">Steele, Robert (1922), <i>The Earliest arithmetics in English</i>, Early English Text Society, <b>118</b>, Oxford University Press, p.&#160;82</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=The+Earliest+arithmetics+in+English&amp;rft.aulast=Steele&amp;rft.aufirst=Robert&amp;rft.au=Steele%2C%26%2332%3BRobert&amp;rft.date=1922&amp;rft.series=Early+English+Text+Society&amp;rft.volume=118&amp;rft.pages=p.%26nbsp%3B82&amp;rft.pub=Oxford+University+Press&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFChabertBarbin1999">Chabert, Jean-Luc; Barbin, velyne (1999), <i>A history of algorithms: from the pebble to the microchip</i>, Springer-Verlag, p.&#160;16, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9783540633693" title="Special:BookSources/9783540633693">9783540633693</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=A+history+of+algorithms%3A+from+the+pebble+to+the+microchip&amp;rft.aulast=Chabert&amp;rft.aufirst=Jean-Luc&amp;rft.au=Chabert%2C%26%2332%3BJean-Luc&amp;rft.au=Barbin%2C%26%2332%3B%C3%89velyne&amp;rft.date=1999&amp;rft.pages=p.%26nbsp%3B16&amp;rft.pub=Springer-Verlag&amp;rft.isbn=9783540633693&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-2"><b><a href="#cite_ref-2">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFJackson1906">Jackson, Lambert Lincoln (1906), <i>The educational significance of sixteenth century arithmetic from the point of view of the present time</i>, Contributions to education, <b>8</b>, Columbia University, p.&#160;76</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=The+educational+significance+of+sixteenth+century+arithmetic+from+the+point+of+view+of+the+present+time&amp;rft.aulast=Jackson&amp;rft.aufirst=Lambert+Lincoln&amp;rft.au=Jackson%2C%26%2332%3BLambert+Lincoln&amp;rft.date=1906&amp;rft.series=Contributions+to+education&amp;rft.volume=8&amp;rft.pages=p.%26nbsp%3B76&amp;rft.pub=Columbia+University&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-3"><b><a href="#cite_ref-3">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFWaters1929">Waters, E. G. R. (1929), "A Fifteenth Century French Algorism from Lige", <i>Isis</i> <b>12</b> (2): 194236, <a href="/wiki/JSTOR" title="JSTOR">JSTOR</a>&#160;<a rel="nofollow" class="external text" href="http://www.jstor.org/stable/224785">224785</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=A+Fifteenth+Century+French+Algorism+from+Li%C3%A9ge&amp;rft.jtitle=Isis&amp;rft.aulast=Waters&amp;rft.aufirst=E.+G.+R.&amp;rft.au=Waters%2C%26%2332%3BE.+G.+R.&amp;rft.date=1929&amp;rft.volume=12&amp;rft.issue=2&amp;rft.pages=194%E2%80%93236&amp;rft.jstor=224785&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-WC00-4">^ <a href="#cite_ref-WC00_4-0"><sup><i><b>a</b></i></sup></a> <a href="#cite_ref-WC00_4-1"><sup><i><b>b</b></i></sup></a> <span class="reference-text"><span class="citation" id="CITEREFWadleighCrawford2000">Wadleigh, Kevin R.; Crawford, Isom L. (2000), <i>Software optimization for high-performance computing</i>, Prentice Hall, p.&#160;92, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9780130170088" title="Special:BookSources/9780130170088">9780130170088</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Software+optimization+for+high-performance+computing&amp;rft.aulast=Wadleigh&amp;rft.aufirst=Kevin+R.&amp;rft.au=Wadleigh%2C%26%2332%3BKevin+R.&amp;rft.au=Crawford%2C%26%2332%3BIsom+L.&amp;rft.date=2000&amp;rft.pages=p.%26nbsp%3B92&amp;rft.pub=Prentice+Hall&amp;rft.isbn=9780130170088&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-5"><b><a href="#cite_ref-5">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFHook2005">Hook, Brian (2005), <i>Write portable code: an introduction to developing software for multiple platforms</i>, No Starch Press, p.&#160;133, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9781593270568" title="Special:BookSources/9781593270568">9781593270568</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Write+portable+code%3A+an+introduction+to+developing+software+for+multiple+platforms&amp;rft.aulast=Hook&amp;rft.aufirst=Brian&amp;rft.au=Hook%2C%26%2332%3BBrian&amp;rft.date=2005&amp;rft.pages=p.%26nbsp%3B133&amp;rft.pub=No+Starch+Press&amp;rft.isbn=9781593270568&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-6"><b><a href="#cite_ref-6">^</a></b> <span class="reference-text"><span class="citation web"><a rel="nofollow" class="external text" href="http://java.sun.com/javase/6/docs/api/java/lang/Math.html#scalb(double,%20int)">"Math.scalb"</a>. <i>Java Platform Standard Ed. 6</i><span class="printonly">. <a rel="nofollow" class="external free" href="http://java.sun.com/javase/6/docs/api/java/lang/Math.html#scalb(double,%20int)">http://java.sun.com/javase/6/docs/api/java/lang/Math.html#scalb(double,%20int)</a></span><span class="reference-accessdate">. Retrieved 2009-10-11</span>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=bookitem&amp;rft.btitle=Math.scalb&amp;rft.atitle=Java+Platform+Standard+Ed.+6&amp;rft_id=http%3A%2F%2Fjava.sun.com%2Fjavase%2F6%2Fdocs%2Fapi%2Fjava%2Flang%2FMath.html%23scalb%28double%2C%2520int%29&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span></span></li>
<li id="cite_note-7"><b><a href="#cite_ref-7">^</a></b> <span class="reference-text"><span class="citation"><i>Programming languages  C, International Standard ISO/IEC 9899:1999</i></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Programming+languages+%E2%80%94+C%2C+International+Standard+ISO%2FIEC+9899%3A1999&amp;rfr_id=info:sid/en.wikipedia.org:Division_by_two"><span style="display: none;">&#160;</span></span>, Section 7.12.6.6.</span></li>
</ol>
</div>


<!-- 
NewPP limit report
Preprocessor node count: 5672/1000000
Post-expand include size: 34524/2048000 bytes
Template argument size: 7170/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:80754-0!*!0!!en!*!* and timestamp 20120407110439 -->
