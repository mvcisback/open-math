<p>A <b>unit fraction</b> is a <a href="/wiki/Rational_number" title="Rational number">rational number</a> written as a <a href="/wiki/Fraction_(mathematics)" title="Fraction (mathematics)">fraction</a> where the <a href="/wiki/Numerator" title="Numerator" class="mw-redirect">numerator</a> is <a href="/wiki/1_(number)" title="1 (number)">one</a> and the <a href="/wiki/Denominator" title="Denominator" class="mw-redirect">denominator</a> is a positive <a href="/wiki/Integer" title="Integer">integer</a>. A unit fraction is therefore the <a href="/wiki/Multiplicative_inverse" title="Multiplicative inverse">reciprocal</a> of a positive integer, 1/<i>n</i>. Examples are 1/1, 1/2, 1/3, 1/4 etc.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Elementary_arithmetic"><span class="tocnumber">1</span> <span class="toctext">Elementary arithmetic</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Modular_arithmetic"><span class="tocnumber">2</span> <span class="toctext">Modular arithmetic</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Finite_sums_of_unit_fractions"><span class="tocnumber">3</span> <span class="toctext">Finite sums of unit fractions</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Series_of_unit_fractions"><span class="tocnumber">4</span> <span class="toctext">Series of unit fractions</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Matrices_of_unit_fractions"><span class="tocnumber">5</span> <span class="toctext">Matrices of unit fractions</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Adjacent_fractions"><span class="tocnumber">6</span> <span class="toctext">Adjacent fractions</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Unit_fractions_in_probability_and_statistics"><span class="tocnumber">7</span> <span class="toctext">Unit fractions in probability and statistics</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#Unit_fractions_in_physics"><span class="tocnumber">8</span> <span class="toctext">Unit fractions in physics</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#References"><span class="tocnumber">9</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#External_links"><span class="tocnumber">10</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=1" title="Edit section: Elementary arithmetic"></a>]</span> <span class="mw-headline" id="Elementary_arithmetic">Elementary arithmetic</span></h2>
<p><a href="/wiki/Multiplication" title="Multiplication">Multiplying</a> any two unit fractions results in a product that is another unit fraction:</p>
<dl>
<dd><img class="tex" alt="\frac1x \times \frac1y = \frac1{xy}." src="//upload.wikimedia.org/wikipedia/en/math/b/1/3/b1396effd4e72d89f61b67900de84d31.png" /></dd>
</dl>
<p>However, <a href="/wiki/Addition" title="Addition">adding</a>, <a href="/wiki/Subtraction" title="Subtraction">subtracting</a>, or <a href="/wiki/Division_(mathematics)" title="Division (mathematics)">dividing</a> two unit fractions produces a result that is generally not a unit fraction:</p>
<dl>
<dd><img class="tex" alt="\frac1x + \frac1y = \frac{x+y}{xy}" src="//upload.wikimedia.org/wikipedia/en/math/3/7/4/3749a47e41b342f6a926da616f4b2f10.png" /></dd>
</dl>
<dl>
<dd><img class="tex" alt="\frac1x - \frac1y = \frac{y-x}{xy}" src="//upload.wikimedia.org/wikipedia/en/math/1/3/f/13fc82c055ae3d5b460f208e8e96a0c0.png" /></dd>
</dl>
<dl>
<dd><img class="tex" alt="\frac1x \div \frac1y = \frac{y}{x}." src="//upload.wikimedia.org/wikipedia/en/math/0/a/f/0affcf2e98393c02482a3ea44f0851c8.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=2" title="Edit section: Modular arithmetic"></a>]</span> <span class="mw-headline" id="Modular_arithmetic">Modular arithmetic</span></h2>
<p>Unit fractions play an important role in <a href="/wiki/Modular_arithmetic" title="Modular arithmetic">modular arithmetic</a>, as they may be used to reduce modular division to the calculation of greatest common divisors. Specifically, suppose that we wish to perform divisions by a value <i>x</i>, modulo <i>y</i>. In order for division by <i>x</i> to be well defined modulo <i>y</i>, <i>x</i> and <i>y</i> must be <a href="/wiki/Relatively_prime" title="Relatively prime" class="mw-redirect">relatively prime</a>. Then, by using the <a href="/wiki/Extended_Euclidean_algorithm" title="Extended Euclidean algorithm">extended Euclidean algorithm</a> for <a href="/wiki/Greatest_common_divisor" title="Greatest common divisor">greatest common divisors</a> we may find <i>a</i> and <i>b</i> such that</p>
<dl>
<dd><img class="tex" alt="\displaystyle ax + by = 1," src="//upload.wikimedia.org/wikipedia/en/math/7/a/3/7a3bb4a9815f2c448d4eebf545ed5c59.png" /></dd>
</dl>
<p>from which it follows that</p>
<dl>
<dd><img class="tex" alt="\displaystyle ax \equiv 1 \pmod y," src="//upload.wikimedia.org/wikipedia/en/math/5/9/4/594c536fe55f387a00e4b4298a87a31b.png" /></dd>
</dl>
<p>or equivalently</p>
<dl>
<dd><img class="tex" alt="a \equiv \frac1x \pmod y." src="//upload.wikimedia.org/wikipedia/en/math/6/6/9/6694e9b8c0982fcde23656938e45bc4c.png" /></dd>
</dl>
<p>Thus, to divide by <i>x</i> (modulo <i>y</i>) we need merely instead multiply by <i>a</i>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=3" title="Edit section: Finite sums of unit fractions"></a>]</span> <span class="mw-headline" id="Finite_sums_of_unit_fractions">Finite sums of unit fractions</span></h2>
<p>Any positive rational number can be written as the sum of unit fractions, in multiple ways. For example,</p>
<dl>
<dd><img class="tex" alt="\frac45=\frac12+\frac14+\frac1{20}=\frac13+\frac15+\frac16+\frac1{10}." src="//upload.wikimedia.org/wikipedia/en/math/3/8/b/38ba3eb3ad087d473d3274d82e255049.png" /></dd>
</dl>
<p>The ancient Egyptians used sums of distinct unit fractions in their notation for more general <a href="/wiki/Rational_number" title="Rational number">rational numbers</a>, and so such sums are often called <a href="/wiki/Egyptian_fractions" title="Egyptian fractions" class="mw-redirect">Egyptian fractions</a>. There is still interest today in analyzing the methods used by the ancients to choose among the possible representations for a fractional number, and to calculate with such representations.<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup> The topic of Egyptian fractions has also seen interest in modern <a href="/wiki/Number_theory" title="Number theory">number theory</a>; for instance, the <a href="/wiki/Erd%C5%91s%E2%80%93Graham_conjecture" title="ErdsGraham conjecture" class="mw-redirect">ErdsGraham conjecture</a> and the <a href="/wiki/Erd%C5%91s%E2%80%93Straus_conjecture" title="ErdsStraus conjecture">ErdsStraus conjecture</a> concern sums of unit fractions, as does the definition of <a href="/wiki/Ore%27s_harmonic_number" title="Ore's harmonic number" class="mw-redirect">Ore's harmonic numbers</a>.</p>
<p>In <a href="/wiki/Geometric_group_theory" title="Geometric group theory">geometric group theory</a>, <a href="/wiki/Triangle_group" title="Triangle group">triangle groups</a> are classified into Euclidean, spherical, and hyperbolic cases according to whether an associated sum of unit fractions is equal to one, greater than one, or less than one respectively.</p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=4" title="Edit section: Series of unit fractions"></a>]</span> <span class="mw-headline" id="Series_of_unit_fractions">Series of unit fractions</span></h2>
<p>Many well-known <a href="/wiki/Series_(mathematics)" title="Series (mathematics)">infinite series</a> have terms that are unit fractions. These include:</p>
<ul>
<li>The <a href="/wiki/Harmonic_series_(mathematics)" title="Harmonic series (mathematics)">harmonic series</a>, the sum of all positive unit fractions. This sum diverges, and its partial sums</li>
</ul>
<dl>
<dd>
<dl>
<dd><img class="tex" alt="\frac11+\frac12+\frac13+\cdots+\frac1n" src="//upload.wikimedia.org/wikipedia/en/math/2/4/6/2468554919b728d144e5f86dfcfb17f1.png" /></dd>
</dl>
</dd>
<dd>closely approximate <a href="/wiki/Natural_logarithm" title="Natural logarithm">ln</a>&#160;<i>n</i>&#160;+&#160;<a href="/wiki/Euler-Mascheroni_constant" title="Euler-Mascheroni constant" class="mw-redirect"></a> as <i>n</i> increases.</dd>
</dl>
<ul>
<li>The <a href="/wiki/Basel_problem" title="Basel problem">Basel problem</a> concerns the sum of the square unit fractions, which converges to <a href="/wiki/Pi" title="Pi"></a><sup>2</sup>/6</li>
</ul>
<ul>
<li><a href="/wiki/Ap%C3%A9ry%27s_constant" title="Apry's constant">Apry's constant</a> is the sum of the cubed unit fractions.</li>
</ul>
<ul>
<li>The binary <a href="/wiki/Geometric_series" title="Geometric series">geometric series</a>, which adds to 2, and the <a href="/wiki/Reciprocal_Fibonacci_constant" title="Reciprocal Fibonacci constant">reciprocal Fibonacci constant</a> are additional examples of a series composed of unit fractions.</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=5" title="Edit section: Matrices of unit fractions"></a>]</span> <span class="mw-headline" id="Matrices_of_unit_fractions">Matrices of unit fractions</span></h2>
<p>The <a href="/wiki/Hilbert_matrix" title="Hilbert matrix">Hilbert matrix</a> is the matrix with elements</p>
<dl>
<dd><img class="tex" alt="B_{i,j} = \frac1{i+j-1}." src="//upload.wikimedia.org/wikipedia/en/math/7/d/3/7d3154e032607a8631014c399c538486.png" /></dd>
</dl>
<p>It has the unusual property that all elements in its <a href="/wiki/Matrix_inverse" title="Matrix inverse" class="mw-redirect">inverse matrix</a> are integers.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup> Similarly, <a href="#CITEREFRichardson2001">Richardson (2001)</a> defined a matrix with elements</p>
<dl>
<dd><img class="tex" alt="C_{i,j} = \frac1{F_{i+j-1}}," src="//upload.wikimedia.org/wikipedia/en/math/1/3/1/131971aa6f2752ba2e37daa4ee93c6b1.png" /></dd>
</dl>
<p>where <i>F</i><sub>i</sub> denotes the <i>i</i>th <a href="/wiki/Fibonacci_number" title="Fibonacci number">Fibonacci number</a>. He calls this matrix the Filbert matrix and it has the same property of having an integer inverse.<sup id="cite_ref-2" class="reference"><a href="#cite_note-2"><span>[</span>3<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=6" title="Edit section: Adjacent fractions"></a>]</span> <span class="mw-headline" id="Adjacent_fractions">Adjacent fractions</span></h2>
<p>Two fractions are called <b>adjacent</b> if their difference is a unit fraction.<sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>4<span>]</span></a></sup><sup id="cite_ref-4" class="reference"><a href="#cite_note-4"><span>[</span>5<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=7" title="Edit section: Unit fractions in probability and statistics"></a>]</span> <span class="mw-headline" id="Unit_fractions_in_probability_and_statistics">Unit fractions in probability and statistics</span></h2>
<p>In a <a href="/wiki/Uniform_distribution_(discrete)" title="Uniform distribution (discrete)">uniform distribution on a discrete space</a>, all probabilities are equal unit fractions. Due to the <a href="/wiki/Principle_of_indifference" title="Principle of indifference">principle of indifference</a>, probabilities of this form arise frequently in statistical calculations.<sup id="cite_ref-5" class="reference"><a href="#cite_note-5"><span>[</span>6<span>]</span></a></sup> Additionally, <a href="/wiki/Zipf%27s_law" title="Zipf's law">Zipf's law</a> states that, for many observed phenomena involving the selection of items from an ordered sequence, the probability that the <i>n</i>th item is selected is proportional to the unit fraction 1/<i>n</i>.<sup id="cite_ref-6" class="reference"><a href="#cite_note-6"><span>[</span>7<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=8" title="Edit section: Unit fractions in physics"></a>]</span> <span class="mw-headline" id="Unit_fractions_in_physics">Unit fractions in physics</span></h2>
<p>The energy levels of <a href="/wiki/Photon" title="Photon">photons</a> that can be absorbed or emitted by a hydrogen atom are, according to the <a href="/wiki/Rydberg_formula" title="Rydberg formula">Rydberg formula</a>, proportional to the differences of two unit fractions. An explanation for this phenomenon is provided by the <a href="/wiki/Bohr_model" title="Bohr model">Bohr model</a>, according to which the energy levels of <a href="/wiki/Atomic_orbital" title="Atomic orbital">electron orbitals</a> in a <a href="/wiki/Hydrogen_atom" title="Hydrogen atom">hydrogen atom</a> are inversely proportional to square unit fractions, and the energy of a photon is <a href="/wiki/Quantization_(physics)" title="Quantization (physics)">quantized</a> to the difference between two levels.<sup id="cite_ref-7" class="reference"><a href="#cite_note-7"><span>[</span>8<span>]</span></a></sup></p>
<p><a href="/wiki/Arthur_Eddington" title="Arthur Eddington">Arthur Eddington</a> argued that the <a href="/wiki/Fine_structure_constant" title="Fine structure constant" class="mw-redirect">fine structure constant</a> was a unit fraction, first 1/136 then 1/137. This contention has been falsified, given that current estimates of the fine structure constant are (to 6 significant digits) 1/137.036.<sup id="cite_ref-8" class="reference"><a href="#cite_note-8"><span>[</span>9<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=9" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFGuy2004"><a href="/wiki/Richard_K._Guy" title="Richard K. Guy">Guy, Richard K.</a> (2004), "D11. Egyptian Fractions", <i>Unsolved problems in number theory</i> (3rd ed.), Springer-Verlag, p.&#160;252262, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/978-0387208602" title="Special:BookSources/978-0387208602">978-0387208602</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=bookitem&amp;rft.btitle=D11.+Egyptian+Fractions&amp;rft.atitle=Unsolved+problems+in+number+theory&amp;rft.aulast=Guy&amp;rft.aufirst=Richard+K.&amp;rft.au=Guy%2C%26%2332%3BRichard+K.&amp;rft.date=2004&amp;rft.pages=p.%26nbsp%3B252%E2%80%93262&amp;rft.ion=3rd&amp;rft.pub=Springer-Verlag&amp;rft.isbn=978-0387208602&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFChoi1983">Choi, Man Duen (1983), "Tricks or treats with the Hilbert matrix", <i>The American Mathematical Monthly</i> <b>90</b> (5): 301312, <a href="/wiki/Digital_object_identifier" title="Digital object identifier">doi</a>:<a rel="nofollow" class="external text" href="http://dx.doi.org/10.2307%2F2975779">10.2307/2975779</a>, <a href="/wiki/Mathematical_Reviews" title="Mathematical Reviews">MR</a><a rel="nofollow" class="external text" href="http://www.ams.org/mathscinet-getitem?mr=701570">701570</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=Tricks+or+treats+with+the+Hilbert+matrix&amp;rft.jtitle=The+American+Mathematical+Monthly&amp;rft.aulast=Choi&amp;rft.aufirst=Man+Duen&amp;rft.au=Choi%2C%26%2332%3BMan+Duen&amp;rft.date=1983&amp;rft.volume=90&amp;rft.issue=5&amp;rft.pages=301%E2%80%93312&amp;rft_id=info:doi/10.2307%2F2975779&amp;rft.mr=701570&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-2"><b><a href="#cite_ref-2">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFRichardson2001">Richardson, Thomas M. (2001), <a rel="nofollow" class="external text" href="http://www.fq.math.ca/Scanned/39-3/richardson.pdf">"The Filbert matrix"</a>, <i><a href="/wiki/Fibonacci_Quarterly" title="Fibonacci Quarterly">Fibonacci Quarterly</a></i> <b>39</b> (3): 268275, <a href="/wiki/ArXiv" title="ArXiv">arXiv</a>:<a rel="nofollow" class="external text" href="http://arxiv.org/abs/math.RA/9905079">math.RA/9905079</a>, <a href="/wiki/Bibcode" title="Bibcode">Bibcode</a> <a rel="nofollow" class="external text" href="http://adsabs.harvard.edu/abs/1999math......5079R">1999math......5079R</a><span class="printonly">, <a rel="nofollow" class="external free" href="http://www.fq.math.ca/Scanned/39-3/richardson.pdf">http://www.fq.math.ca/Scanned/39-3/richardson.pdf</a></span></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=The+Filbert+matrix&amp;rft.jtitle=%5B%5BFibonacci+Quarterly%5D%5D&amp;rft.aulast=Richardson&amp;rft.aufirst=Thomas+M.&amp;rft.au=Richardson%2C%26%2332%3BThomas+M.&amp;rft.date=2001&amp;rft.volume=39&amp;rft.issue=3&amp;rft.pages=268%E2%80%93275&amp;rft_id=info:arxiv/math.RA%2F9905079&amp;rft_id=info:bibcode/1999math......5079R&amp;rft_id=http%3A%2F%2Fwww.fq.math.ca%2FScanned%2F39-3%2Frichardson.pdf&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span></span></li>
<li id="cite_note-3"><b><a href="#cite_ref-3">^</a></b> <span class="reference-text"><a rel="nofollow" class="external text" href="http://planetmath.org/encyclopedia/AdjacentFraction.html">Adjacent Fraction</a> at <a href="/wiki/PlanetMath" title="PlanetMath">PlanetMath</a></span></li>
<li id="cite_note-4"><b><a href="#cite_ref-4">^</a></b> <span class="reference-text"><span class="citation mathworld" id="Reference-Mathworld-Adjacent_Fraction"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/AdjacentFraction.html">Adjacent Fraction</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></span></li>
<li id="cite_note-5"><b><a href="#cite_ref-5">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFWelsh1996">Welsh, Alan H. (1996), <i>Aspects of statistical inference</i>, Wiley Series in Probability and Statistics, <b>246</b>, John Wiley and Sons, p.&#160;66, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9780471115915" title="Special:BookSources/9780471115915">9780471115915</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Aspects+of+statistical+inference&amp;rft.aulast=Welsh&amp;rft.aufirst=Alan+H.&amp;rft.au=Welsh%2C%26%2332%3BAlan+H.&amp;rft.date=1996&amp;rft.series=Wiley+Series+in+Probability+and+Statistics&amp;rft.volume=246&amp;rft.pages=p.%26nbsp%3B66&amp;rft.pub=John+Wiley+and+Sons&amp;rft.isbn=9780471115915&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-6"><b><a href="#cite_ref-6">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFSaichevMalevergneSornette2009">Saichev, Alexander; Malevergne, Yannick; Sornette, Didier (2009), <i>Theory of Zipf's Law and Beyond</i>, Lecture Notes in Economics and Mathematical Systems, <b>632</b>, Springer-Verlag, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9783642029455" title="Special:BookSources/9783642029455">9783642029455</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Theory+of+Zipf%27s+Law+and+Beyond&amp;rft.aulast=Saichev&amp;rft.aufirst=Alexander&amp;rft.au=Saichev%2C%26%2332%3BAlexander&amp;rft.au=Malevergne%2C%26%2332%3BYannick&amp;rft.au=Sornette%2C%26%2332%3BDidier&amp;rft.date=2009&amp;rft.series=Lecture+Notes+in+Economics+and+Mathematical+Systems&amp;rft.volume=632&amp;rft.pub=Springer-Verlag&amp;rft.isbn=9783642029455&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-7"><b><a href="#cite_ref-7">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFYangHamilton2009">Yang, Fujia; Hamilton, Joseph H. (2009), <i>Modern Atomic and Nuclear Physics</i>, World Scientific, pp.&#160;8186, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9789812836786" title="Special:BookSources/9789812836786">9789812836786</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Modern+Atomic+and+Nuclear+Physics&amp;rft.aulast=Yang&amp;rft.aufirst=Fujia&amp;rft.au=Yang%2C%26%2332%3BFujia&amp;rft.au=Hamilton%2C%26%2332%3BJoseph+H.&amp;rft.date=2009&amp;rft.pages=pp.%26nbsp%3B81%E2%80%9386&amp;rft.pub=World+Scientific&amp;rft.isbn=9789812836786&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
<li id="cite_note-8"><b><a href="#cite_ref-8">^</a></b> <span class="reference-text"><span class="citation" id="CITEREFKilmister1994">Kilmister, Clive William (1994), <i>Eddington's search for a fundamental theory: a key to the universe</i>, Cambridge University Press, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/9780521371650" title="Special:BookSources/9780521371650">9780521371650</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Eddington%27s+search+for+a+fundamental+theory%3A+a+key+to+the+universe&amp;rft.aulast=Kilmister&amp;rft.aufirst=Clive+William&amp;rft.au=Kilmister%2C%26%2332%3BClive+William&amp;rft.date=1994&amp;rft.pub=Cambridge+University+Press&amp;rft.isbn=9780521371650&amp;rfr_id=info:sid/en.wikipedia.org:Unit_fraction"><span style="display: none;">&#160;</span></span>.</span></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Unit_fraction&amp;action=&amp;section=10" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><span class="citation mathworld" id="Reference-Mathworld-Unit_Fraction"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/UnitFraction.html">Unit Fraction</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 5678/1000000
Post-expand include size: 39200/2048000 bytes
Template argument size: 8024/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:491316-0!0!0!!en!*!* and timestamp 20120414023734 -->
