<div class="floatright"><a href="/wiki/File:Annulus_area.svg" class="image" title="An annulus"><img alt="An annulus" src="//upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Annulus_area.svg/150px-Annulus_area.svg.png" width="150" height="150" /></a></div>
<p>In <a href="/wiki/Mathematics" title="Mathematics">mathematics</a>, an <b>annulus</b> (the <a href="/wiki/Latin" title="Latin">Latin</a> word for "little ring", with plural <i>annuli</i>) is a ring-shaped geometric figure, or more generally, a term used to name a ring-shaped object. Or, it is the area between two concentric circles. The adjectival form is <b>annular</b> (for example, an <a href="/wiki/Annular_eclipse" title="Annular eclipse" class="mw-redirect">annular eclipse</a>).</p>
<p>The open annulus is <a href="/wiki/Homeomorphism" title="Homeomorphism">topologically equivalent</a> to both the open <a href="/wiki/Cylinder_(geometry)" title="Cylinder (geometry)">cylinder</a> <span class="texhtml"><i>S</i><sup>1</sup>  (0,1)</span> and the <a href="/wiki/Punctured_plane" title="Punctured plane" class="mw-redirect">punctured plane</a>.</p>
<p>The area of an annulus is the difference in the areas of the larger <a href="/wiki/Circle" title="Circle">circle</a> of radius <span class="texhtml"><i>R</i></span> and the smaller one of radius <span class="texhtml"><i>r</i></span>:</p>
<dl>
<dd><img class="tex" alt="A = \pi R^2 - \pi r^2 = \pi(R^2 - r^2)\,." src="//upload.wikimedia.org/wikipedia/en/math/6/2/5/625a63303c4fd802feacd8828c84c799.png" /></dd>
</dl>
<p>The area of an annulus can be obtained from the length of the longest interval that can lie completely inside the annulus, 2*<i>d</i> in the accompanying diagram. This can be proven by the <a href="/wiki/Pythagorean_theorem" title="Pythagorean theorem">Pythagorean theorem</a>; the length of the longest interval that can lie completely inside the annulus will be <a href="/wiki/Tangent" title="Tangent">tangent</a> to the smaller circle and form a right angle with its radius at that point. Therefore <i>d</i> and <i>r</i> are the sides of a right angled triangle with hypotenuse <i>R</i> and the area is given by:</p>
<dl>
<dd><img class="tex" alt="A = \pi (R^2-r^2) = \pi d^2 \,." src="//upload.wikimedia.org/wikipedia/en/math/d/7/4/d74b5da38fe25b8324113066591b6250.png" /></dd>
</dl>
<p>The area can also be obtained via <a href="/wiki/Calculus" title="Calculus">calculus</a> by dividing the annulus up into an infinite number of annuli of <a href="/wiki/Infinitesimal" title="Infinitesimal">infinitesimal</a> width <span class="texhtml"><i>d</i></span> and area <span class="texhtml">2<i> d</i></span> ( ) and then <a href="/wiki/Integral" title="Integral">integrating</a> from <i></i> = <i>r</i> to <i></i> = <i>R</i>:</p>
<dl>
<dd><img class="tex" alt="A = \int_r^R 2\pi\rho\, d\rho = \pi(R^2-r^2)." src="//upload.wikimedia.org/wikipedia/en/math/d/c/3/dc3012756e9c58173505bdab6522d6c3.png" /></dd>
</dl>
<p>The area of an annulus segment of angle <span class="texhtml"><i></i></span>, with <span class="texhtml"><i></i></span> measured in radians, is given by:</p>
<dl>
<dd><img class="tex" alt=" A = \frac{\theta}{2} (R^2 - r^2) " src="//upload.wikimedia.org/wikipedia/en/math/0/c/0/0c0e57a2a3a0a46596e1ce4aa75d551b.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Annulus_(mathematics)&amp;action=&amp;section=1" title="Edit section: Complex structure"></a>]</span> <span class="mw-headline" id="Complex_structure">Complex structure</span></h2>
<p>In <a href="/wiki/Complex_analysis" title="Complex analysis">complex analysis</a> an <b>annulus</b> <span class="texhtml">ann<i>(a; r, R)</i></span> in the <a href="/wiki/Complex_plane" title="Complex plane">complex plane</a> is an <a href="/wiki/Open_region" title="Open region" class="mw-redirect">open region</a> defined by:</p>
<dl>
<dd><img class="tex" alt=" r &lt; |z-a| &lt; R.\," src="//upload.wikimedia.org/wikipedia/en/math/8/1/5/8154f9370e3506fab480d0ea62635325.png" /></dd>
</dl>
<p>If <span class="texhtml"><i>r</i></span> is <span class="texhtml">0</span>, the region is known as the <b>punctured disk</b> of radius <span class="texhtml"><i>R</i></span> around the point <span class="texhtml"><i>a</i></span>.</p>
<p>As a subset of the complex <a href="/wiki/Plane_(mathematics)" title="Plane (mathematics)" class="mw-redirect">plane</a>, an annulus can be considered as a <a href="/wiki/Riemann_surface" title="Riemann surface">Riemann surface</a>. The complex structure of an annulus depends only on the ratio <span class="texhtml"><i>r</i>/<i>R</i></span>. Each annulus <span class="texhtml">ann<i>(a; r, R)</i></span> can be <a href="/wiki/Holomorphic_function" title="Holomorphic function">holomorphically</a> mapped to a standard one centered at the origin and with outer radius <span class="texhtml">1</span> by the map</p>
<dl>
<dd><img class="tex" alt="z \mapsto \frac{z-a}{R}." src="//upload.wikimedia.org/wikipedia/en/math/e/9/4/e9495886efe9b36b0082091ae67a34cf.png" /></dd>
</dl>
<p>The inner radius is then <span class="texhtml"><i>r</i>/<i>R</i> &lt; 1</span>.</p>
<p>The <a href="/wiki/Hadamard_three-circle_theorem" title="Hadamard three-circle theorem">Hadamard three-circle theorem</a> is a statement about the maximum value a holomorphic function may take inside an annulus.</p>
<h2><span class="section">[<a href="/w/index.php?title=Annulus_(mathematics)&amp;action=&amp;section=2" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Annulus_theorem" title="Annulus theorem">Annulus theorem</a> (or conjecture)</li>
<li><a href="/wiki/Spherical_shell" title="Spherical shell">Spherical shell</a></li>
<li><a href="/wiki/Torus" title="Torus">Torus</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Annulus_(mathematics)&amp;action=&amp;section=3" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/annulus.html">Annulus definition and properties</a> With interactive animation</li>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/annulusarea.html">Area of an annulus, formula</a> With interactive animation</li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 303/1000000
Post-expand include size: 679/2048000 bytes
Template argument size: 169/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:356158-0!0!0!!*!4!* and timestamp 20120331115208 -->
