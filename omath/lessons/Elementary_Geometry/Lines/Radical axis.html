<div class="dablink">This article is about the radical axis used in geometry.  For the animation studio, see <a href="/wiki/Radical_Axis_(studio)" title="Radical Axis (studio)">Radical Axis (studio)</a>.</div>
<div class="thumb tright">
<div class="thumbinner" style="width:352px;"><a href="/wiki/File:Radical_axis_orthogonal_circles.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/05/Radical_axis_orthogonal_circles.svg/350px-Radical_axis_orthogonal_circles.svg.png" width="350" height="329" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Radical_axis_orthogonal_circles.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Figure 1. Illustration of the radical axis (red line) of two given circles (solid black). For any point <b>P</b> (blue) on the radical axis, a unique circle (dashed) can be drawn that is centered on that point and intersects both given circles at right angles, i.e., orthogonally. The point <b>P</b> has an equal <a href="/wiki/Power_of_a_point" title="Power of a point">power</a> with respect to both given circles, because the tangents from <b>P</b> (blue lines) are radii of the orthogonal circle and thus have equal length.</div>
</div>
</div>
<p>The <b>radical axis</b> (or <b>power line</b>) of two <a href="/wiki/Circles" title="Circles" class="mw-redirect">circles</a> is the <a href="/wiki/Locus_(mathematics)" title="Locus (mathematics)">locus</a> of points at which tangents drawn to both circles have the same length. For any point <b>P</b> on the radical axis, there is a unique circle centered on <b>P</b> that intersects both circles at right angles (orthogonally); conversely, the center of any circle that cuts both circles orthogonally must lie on the radical axis. In technical language, each point <b>P</b> on the radical axis has the same <a href="/wiki/Power_of_a_point" title="Power of a point">power</a> with respect to both circles<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt="
R^{2} = d_{1}^{2} - r_{1}^{2} = d_{2}^{2} - r_{2}^{2}
" src="//upload.wikimedia.org/wikipedia/en/math/3/5/6/356543cd46d205a97e99f765ea0c0e2a.png" /></dd>
</dl>
<p>where <i>r</i><sub>1</sub> and <i>r</i><sub>2</sub> are the radii of the two circles, <i>d</i><sub>1</sub> and <i>d</i><sub>2</sub> are distances from <b>P</b> to the centers of the two circles, and <i>R</i> is the radius of the unique orthogonal circle centered on <b>P</b>.</p>
<p>The radical axis is always a straight line and always <a href="/wiki/Perpendicular" title="Perpendicular">perpendicular</a> to the line connecting the centers of the circles, albeit closer to the circumference of the larger circle. If the circles intersect, the radical axis is the line passing through the intersection points; similarly, if the circles are <a href="/wiki/Tangent" title="Tangent">tangent</a>, the radical axis is simply the common tangent. In general, two disjoint, non-concentric circles can be aligned with the circles of <a href="/wiki/Bipolar_coordinates" title="Bipolar coordinates">bipolar coordinates</a>; in that case, the radical axis is simply the <i>y</i>-axis; every circle on that axis that passes through the two foci intersect the two circles orthogonally. Thus, two radii of such a circle are tangent to both circles, satisfying the definition of the radical axis. The collection of all circles with the same radical axis and with centers on the same line is known as a <a href="/wiki/Pencil_(mathematics)" title="Pencil (mathematics)">pencil</a> of <a href="/wiki/Coaxal_circles" title="Coaxal circles" class="mw-redirect">coaxal circles</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Definition_and_general_properties"><span class="tocnumber">1</span> <span class="toctext">Definition and general properties</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Radical_center_of_three_circles"><span class="tocnumber">2</span> <span class="toctext">Radical center of three circles</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Geometric_construction"><span class="tocnumber">3</span> <span class="toctext">Geometric construction</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Algebraic_construction"><span class="tocnumber">4</span> <span class="toctext">Algebraic construction</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Determinant_calculation"><span class="tocnumber">5</span> <span class="toctext">Determinant calculation</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#References"><span class="tocnumber">6</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Bibliography"><span class="tocnumber">7</span> <span class="toctext">Bibliography</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#Further_reading"><span class="tocnumber">8</span> <span class="toctext">Further reading</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#External_links"><span class="tocnumber">9</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=1" title="Edit section: Definition and general properties"></a>]</span> <span class="mw-headline" id="Definition_and_general_properties">Definition and general properties</span></h2>
<div class="thumb tleft">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:Radical_axis_intersecting_circles.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/9/94/Radical_axis_intersecting_circles.svg/200px-Radical_axis_intersecting_circles.svg.png" width="200" height="200" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Radical_axis_intersecting_circles.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
If the two circles intersect, the radical axis is the <a href="/wiki/Secant_line" title="Secant line">secant line</a> corresponding to their common <a href="/wiki/Chord_(geometry)" title="Chord (geometry)">chord</a>.</div>
</div>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=2" title="Edit section: Radical center of three circles"></a>]</span> <span class="mw-headline" id="Radical_center_of_three_circles">Radical center of three circles</span></h2>
<div class="thumb tright">
<div class="thumbinner" style="width:252px;"><a href="/wiki/File:Radical_center.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Radical_center.svg/250px-Radical_center.svg.png" width="250" height="266" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Radical_center.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The radical center (orange point) is the center of the unique circle (also orange) that intersects three given circles at right angles.</div>
</div>
</div>
<p>Consider three circles <b>A</b>, <b>B</b> and <b>C</b>, no two of which are concentric. The <b>radical axis theorem</b> states that the three radical axes (for each pair of circles) intersect in one point called the <a href="/wiki/Power_center_(geometry)" title="Power center (geometry)">radical center</a>, or are parallel.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup> In technical language, the three radical axes are <i>concurrent</i> (share a common point); if they are parallel, they concur at a point of infinity.</p>
<p>A simple proof is as follows.<sup id="cite_ref-2" class="reference"><a href="#cite_note-2"><span>[</span>3<span>]</span></a></sup> The radical axis of circles <b>A</b> and <b>B</b> is defined as the line along which the tangents to those circles are equal in length <i>a</i>=<i>b</i>. Similarly, the tangents to circles B and C must be equal in length on their radical axis. By the <a href="/wiki/Transitive_relation" title="Transitive relation">transitivity</a> of <a href="/wiki/Equality_(mathematics)" title="Equality (mathematics)">equality</a>, all three tangents are equal <i>a</i>=<i>b</i>=<i>c</i> at the intersection point <b>r</b> of those two radical axes. Hence, the radical axis for circles A and C must pass through the same point <b>r</b>, since <i>a</i>=<i>c</i> there. This common intersection point <b>r</b> is the <b>radical center</b>.</p>
<p>At the radical center, there is a unique circle that is orthogonal to all three circles. This follows because each radical axis is the locus of centers of circles that cut each pair of given circles orthogonally.</p>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=3" title="Edit section: Geometric construction"></a>]</span> <span class="mw-headline" id="Geometric_construction">Geometric construction</span></h2>
<div class="thumb tleft">
<div class="thumbinner" style="width:182px;"><a href="/wiki/File:Radical_axis_construction.svg" class="image"><img alt="Radical axis construction.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Radical_axis_construction.svg/180px-Radical_axis_construction.svg.png" width="180" height="197" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Radical_axis_construction.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
</div>
</div>
</div>
<p>The radical axis of two circles <b>A</b> and <b>B</b> can be constructed by drawing a line through any two of its points. Such a point can be found by drawing a circle <b>C</b> that intersects both circles <b>A</b> and <b>B</b> in two points. The two lines passing through each pair of intersection points are the radical axes of <b>A</b> and <b>C</b> and of <b>B</b> and <b>C</b>. These two lines intersect in a point <b>J</b> that is the radical center of all three circles, as described <a href="#Radical_center_of_three_circles">above</a>; therefore, this point also lies on the radical axis of <b>A</b> and <b>B</b>. Repeating this process with another such circle <b>D</b> provides a second point <b>K</b>. The radical axis is the line passing through both <b>J</b> and <b>K</b>.</p>
<p>A special case of this approach is carried out with antihomologous points from an internal or external center of similarity. Consider two rays emanating from an external homothetic center <b>E</b>. Let the anithomologous pairs of intersections points of these rays with the two given circles be denoted as <b>P</b> and <b>Q</b>, and <b>S</b> and <b>T</b>, respectively. These four points lie on a common circle that intersects the two given circles in two points each.<sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>4<span>]</span></a></sup> Hence, the two lines joining <i>P</i> and <b>S</b>, and <b>Q</b> and <b>T</b> intersect at the radical center of the three circles, which lies on the radical axis of the given circles.<sup id="cite_ref-Johnson_1960.2C_p._41_4-0" class="reference"><a href="#cite_note-Johnson_1960.2C_p._41-4"><span>[</span>5<span>]</span></a></sup> Similarly, the line joining two antihomologous points on separate circles and their tangents form an isoceles triangle, with both tangents being of equal length.<sup id="cite_ref-5" class="reference"><a href="#cite_note-5"><span>[</span>6<span>]</span></a></sup> Therefore, such tangents meet on the radical axis.<sup id="cite_ref-Johnson_1960.2C_p._41_4-1" class="reference"><a href="#cite_note-Johnson_1960.2C_p._41-4"><span>[</span>5<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=4" title="Edit section: Algebraic construction"></a>]</span> <span class="mw-headline" id="Algebraic_construction">Algebraic construction</span></h2>
<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Radical_axis_algebraic.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/8/84/Radical_axis_algebraic.svg/300px-Radical_axis_algebraic.svg.png" width="300" height="220" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Radical_axis_algebraic.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Figure 3: Finding the radical axis algebraically. <i>L</i> is the distance from <b>J</b> to <b>K</b>, whereas <i>x</i><sub>1</sub> and <i>x</i><sub>2</sub> are the distances from <b>K</b> to <b>B</b> and from <b>K</b> to <b>V</b>, respectively. The variables <i>d</i><sub>1</sub> and <i>d</i><sub>2</sub> represent the distances from <b>J</b> to <b>B</b> and from <b>J</b> to <b>V</b>, respectively.</div>
</div>
</div>
<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Two_circles_antihomothetic_points.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Two_circles_antihomothetic_points.svg/300px-Two_circles_antihomothetic_points.svg.png" width="300" height="287" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Two_circles_antihomothetic_points.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Figure 4: Lines through corresponding antihomologous points intersect on the radical axis of the two given circles (green and blue, centered on points <b>C</b> and <b>D</b>, respectively). The points <b>P</b> and <b>Q</b> are antihomologous, as are <b>S</b> and <b>T</b>. These four points lie on a circle that intersects the two given circles.</div>
</div>
</div>
<p>Referring to Figure 3, the radical axis (red) is perpendicular to the blue line segment joining the centers <b>B</b> and <b>V</b> of the two given circles, intersecting that line segment at a point <b>K</b> between the two circles. Therefore, it suffices to find the distance <i>x</i><sub>1</sub> or <i>x</i><sub>2</sub> from <b>K</b> to <b>B</b> or <b>V</b>, respectively, where <i>x</i><sub>1</sub>+<i>x</i><sub>2</sub> equals <i>D</i>, the distance between <b>B</b> and <b>V</b>.</p>
<p>Consider a point <b>J</b> on the radical axis, and let its distances to <b>B</b> and <b>V</b> be denoted as <i>d</i><sub>1</sub> and <i>d</i><sub>2</sub>, respectively. Since <b>J</b> must have the same <a href="/wiki/Power_of_a_point" title="Power of a point">power</a> with respect to both circles, it follows that</p>
<dl>
<dd><img class="tex" alt="
d_{1}^{2} - r_{1}^{2} = d_{2}^{2} - r_{2}^{2} 
" src="//upload.wikimedia.org/wikipedia/en/math/3/0/5/305fe1330e3f44143e0fb1ca04dd26d1.png" /></dd>
</dl>
<p>where <i>r</i><sub>1</sub> and <i>r</i><sub>2</sub> are the radii of the two given circles. By the <a href="/wiki/Pythagorean_theorem" title="Pythagorean theorem">Pythagorean theorem</a>, the distances <i>d</i><sub>1</sub> and <i>d</i><sub>2</sub> can be expressed in terms of <i>x</i><sub>1</sub>, <i>x</i><sub>2</sub> and <i>L</i>, the distance from <b>J</b> to <b>K</b></p>
<dl>
<dd><img class="tex" alt="
L^{2} + x_{1}^{2}  - r_{1}^{2} = L^{2} + x_{2}^{2}  - r_{2}^{2} 
" src="//upload.wikimedia.org/wikipedia/en/math/a/1/3/a138acb245c8d7cd0f5c12cd95edbe55.png" /></dd>
</dl>
<p>By cancelling <i>L</i> on both sides of the equation, the equation can be written</p>
<dl>
<dd><img class="tex" alt="
x_{1}^{2}  - x_{2}^{2} = r_{1}^{2}  - r_{2}^{2}
" src="//upload.wikimedia.org/wikipedia/en/math/e/b/d/ebdf1a37bd4e5fe14e24622cecf334e0.png" /></dd>
</dl>
<p>Dividing both sides by <i>D</i>&#160;=&#160;<i>x</i><sub>1</sub>+<i>x</i><sub>2</sub> yields the equation</p>
<dl>
<dd><img class="tex" alt="
x_{1}  - x_{2} = \frac{r_{1}^{2}  - r_{2}^{2}}{D} 
" src="//upload.wikimedia.org/wikipedia/en/math/8/c/e/8ce35766502e31bd1329b1605fd4c6d4.png" /></dd>
</dl>
<p>Adding this equation to <i>x</i><sub>1</sub>+<i>x</i><sub>2</sub>&#160;=&#160;<i>D</i> yields a formula for <i>x</i><sub>1</sub></p>
<dl>
<dd><img class="tex" alt="
2x_{1} = D + \frac{r_{1}^{2}  - r_{2}^{2}}{D}
" src="//upload.wikimedia.org/wikipedia/en/math/b/c/6/bc67670591b533eaeb43f2485ed50738.png" /></dd>
</dl>
<p>Subtracting the same equation yields the corresponding formula for <i>x</i><sub>2</sub></p>
<dl>
<dd><img class="tex" alt="
2x_{2} = D - \frac{r_{1}^{2}  - r_{2}^{2}}{D}
" src="//upload.wikimedia.org/wikipedia/en/math/c/c/d/ccdb00afa2c7b691737f2ac3b459f4cf.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=5" title="Edit section: Determinant calculation"></a>]</span> <span class="mw-headline" id="Determinant_calculation">Determinant calculation</span></h2>
<p>If the circles are represented in <a href="/wiki/Trilinear_coordinates" title="Trilinear coordinates">trilinear coordinates</a> in the usual way, then their radical center is conveniently given as a certain determinant. Specifically, let <i>X</i> = <i>x</i>&#160;:&#160;<i>y</i>&#160;:&#160;<i>z</i> denote a variable point in the plane of a triangle <i>ABC</i> with sidelengths <i>a</i> = |<i>BC</i>|, <i>b</i> = |<i>CA</i>|, <i>c</i> = |<i>AB</i>|, and represent the circles as follows:</p>
<dl>
<dd>(<i>dx + ey + fz</i>)(<i>ax + by + cz</i>) + <i>g</i>(<i>ayz + bzx + cxy</i>) = 0</dd>
</dl>
<dl>
<dd>(<i>hx + iy + jz</i>)(<i>ax + by + cz</i>) + <i>k</i>(<i>ayz + bzx + cxy</i>) = 0</dd>
</dl>
<dl>
<dd>(<i>lx + my + nz</i>)(<i>ax + by + cz</i>) + <i>p</i>(<i>ayz + bzx + cxy</i>) = 0</dd>
</dl>
<p>Then the radical center is the point</p>
<dl>
<dd><img class="tex" alt=" \det \begin{bmatrix}g&amp;k&amp;p\\
e&amp;i&amp;m\\f&amp;j&amp;n\end{bmatrix}&#160;: \det \begin{bmatrix}g&amp;k&amp;p\\
f&amp;j&amp;n\\d&amp;h&amp;l\end{bmatrix}&#160;: \det \begin{bmatrix}g&amp;k&amp;p\\
d&amp;h&amp;l\\e&amp;i&amp;m\end{bmatrix}." src="//upload.wikimedia.org/wikipedia/en/math/2/7/7/2779a332023e5881afac81e1360fba4c.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=6" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="reflist references-column-count references-column-count-1" style="-moz-column-count: 1; -webkit-column-count: 1; column-count: 1; list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text">Johnson (1960), pp. 3132.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text">Johnson (1960), pp. 3233.</span></li>
<li id="cite_note-2"><b><a href="#cite_ref-2">^</a></b> <span class="reference-text">Johnson (1960), p. 32.</span></li>
<li id="cite_note-3"><b><a href="#cite_ref-3">^</a></b> <span class="reference-text">Johnson (1960), pp. 2021.</span></li>
<li id="cite_note-Johnson_1960.2C_p._41-4">^ <a href="#cite_ref-Johnson_1960.2C_p._41_4-0"><sup><i><b>a</b></i></sup></a> <a href="#cite_ref-Johnson_1960.2C_p._41_4-1"><sup><i><b>b</b></i></sup></a> <span class="reference-text">Johnson (1960), p. 41.</span></li>
<li id="cite_note-5"><b><a href="#cite_ref-5">^</a></b> <span class="reference-text">Johnson (1960), p. 21.</span></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=7" title="Edit section: Bibliography"></a>]</span> <span class="mw-headline" id="Bibliography">Bibliography</span></h2>
<ul>
<li><span class="citation book">Johnson RA (1960). <i>Advanced Euclidean Geometry: An elementary treatise on the geometry of the triangle and the circle</i> (reprint of 1929 ion by Houghton Miflin ed.). New York: Dover Publications. pp.&#160;3143. <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/978-0486462370" title="Special:BookSources/978-0486462370">978-0486462370</a>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Advanced+Euclidean+Geometry%3A+An+elementary+treatise+on+the+geometry+of+the+triangle+and+the+circle&amp;rft.aulast=Johnson+RA&amp;rft.au=Johnson+RA&amp;rft.date=1960&amp;rft.pages=pp.%26nbsp%3B31%26ndash%3B43&amp;rft.ion=reprint+of+1929+ion+by+Houghton+Miflin&amp;rft.place=New+York&amp;rft.pub=Dover+Publications&amp;rft.isbn=978-0486462370&amp;rfr_id=info:sid/en.wikipedia.org:Radical_axis"><span style="display: none;">&#160;</span></span></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=8" title="Edit section: Further reading"></a>]</span> <span class="mw-headline" id="Further_reading">Further reading</span></h2>
<ul>
<li><span class="citation book">Ogilvy CS (1990). <i>Excursions in Geometry</i>. Dover. pp.&#160;1723. <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/0-486-26530-7" title="Special:BookSources/0-486-26530-7">0-486-26530-7</a>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Excursions+in+Geometry&amp;rft.aulast=Ogilvy+CS&amp;rft.au=Ogilvy+CS&amp;rft.date=1990&amp;rft.pages=pp.%26nbsp%3B17%26ndash%3B23&amp;rft.pub=Dover&amp;rft.isbn=0-486-26530-7&amp;rfr_id=info:sid/en.wikipedia.org:Radical_axis"><span style="display: none;">&#160;</span></span></li>
</ul>
<ul>
<li><span class="citation book"><a href="/wiki/Harold_Scott_MacDonald_Coxeter" title="Harold Scott MacDonald Coxeter">Coxeter HSM</a>, <a href="/wiki/S._L._Greitzer" title="S. L. Greitzer" class="mw-redirect">Greitzer SL</a> (1967). <i>Geometry Revisited</i>. <a href="/wiki/Washington,_D.C." title="Washington, D.C.">Washington</a>: <a href="/wiki/Mathematical_Association_of_America" title="Mathematical Association of America">MAA</a>. pp.&#160;3136, 160161. <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/978-0883856192" title="Special:BookSources/978-0883856192">978-0883856192</a>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Geometry+Revisited&amp;rft.aulast=%5B%5BHarold+Scott+MacDonald+Coxeter%7CCoxeter+HSM%5D%5D%2C+%5B%5BS.+L.+Greitzer%7CGreitzer+SL%5D%5D&amp;rft.au=%5B%5BHarold+Scott+MacDonald+Coxeter%7CCoxeter+HSM%5D%5D%2C+%5B%5BS.+L.+Greitzer%7CGreitzer+SL%5D%5D&amp;rft.date=1967&amp;rft.pages=pp.%26nbsp%3B31%26ndash%3B36%2C+160%26ndash%3B161&amp;rft.place=%5B%5BWashington%2C+D.C.%7CWashington%5D%5D&amp;rft.pub=%5B%5BMathematical+Association+of+America%7CMAA%5D%5D&amp;rft.isbn=978-0883856192&amp;rfr_id=info:sid/en.wikipedia.org:Radical_axis"><span style="display: none;">&#160;</span></span></li>
</ul>
<ul>
<li>Clark Kimberling, "Triangle Centers and Central Triangles," <i>Congressus Numerantium</i> 129 (1998) ixxv, 1295.</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Radical_axis&amp;action=&amp;section=9" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<table class="metadata mbox-small plainlinks" style="border:1px solid #aaa; background-color:#f9f9f9;">
<tr>
<td class="mbox-image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/4/4a/Commons-logo.svg/30px-Commons-logo.svg.png" width="30" height="40" /></td>
<td class="mbox-text" style="">Wikimedia Commons has media related to: <i><b><a class="external text" href="//commons.wikimedia.org/wiki/Category:Radical_axes">Radical axes</a></b></i></td>
</tr>
</table>
<ul>
<li><span class="citation mathworld" id="Reference-Mathworld-Radical_line"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/RadicalLine.html">Radical line</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
<li><span class="citation mathworld" id="Reference-Mathworld-Chordal_theorem"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/ChordalTheorem.html">Chordal theorem</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
<li><a rel="nofollow" class="external text" href="http://www.cut-the-knot.org/Curriculum/Geometry/IncircleInSegment1.shtml">Animation</a> at <a href="/wiki/Cut-the-knot" title="Cut-the-knot" class="mw-redirect">Cut-the-knot</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 2527/1000000
Post-expand include size: 15547/2048000 bytes
Template argument size: 5000/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:2093844-0!0!0!!en!4!* and timestamp 20120412195542 -->
