<div class="dablink">For the bisection theorem in measure theory, see <a href="/wiki/Ham_sandwich_theorem" title="Ham sandwich theorem">ham sandwich theorem</a>.  For other uses, see <a href="/wiki/Bisect" title="Bisect">bisect</a>.</div>
<p>In <a href="/wiki/Geometry" title="Geometry">geometry</a>, <b>bisection</b> is the division of something into two equal or <a href="/wiki/Congruence_(geometry)" title="Congruence (geometry)">congruent</a> parts, usually by a <a href="/wiki/Line_(mathematics)" title="Line (mathematics)" class="mw-redirect">line</a>, which is then called a <i>bisector</i>. The most often considered types of bisectors are the <i>segment bisector</i> (a line that passes through the midpoint of a given segment) and the <i>angle bisector</i> (a line that passes through the apex of an angle, that divides it into two equal angles).</p>
<p>In <a href="/wiki/Three_dimensional_space" title="Three dimensional space" class="mw-redirect">three dimensional space</a>, bisection is usually done by a plane, also called the <i>bisector</i> or <i>bisecting plane</i>.</p>
<div class="thumb tright">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:Perpendicular_bisector.gif" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/4/45/Perpendicular_bisector.gif" width="200" height="333" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Perpendicular_bisector.gif" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Bisection of a line segment using a compass and ruler</div>
</div>
</div>
<div class="thumb tright">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:Bisection_construction.gif" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/1/14/Bisection_construction.gif" width="200" height="200" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Bisection_construction.gif" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Bisection of an angle using a compass and ruler</div>
</div>
</div>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Line_segment_bisector"><span class="tocnumber">1</span> <span class="toctext">Line segment bisector</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Angle_bisector"><span class="tocnumber">2</span> <span class="toctext">Angle bisector</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Angle_bisectors_of_a_triangle"><span class="tocnumber">2.1</span> <span class="toctext">Angle bisectors of a triangle</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Angle_bisectors_of_a_rhombus"><span class="tocnumber">2.2</span> <span class="toctext">Angle bisectors of a rhombus</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Area_bisectors_and_area-perimeter_bisectors_of_a_triangle"><span class="tocnumber">3</span> <span class="toctext">Area bisectors and area-perimeter bisectors of a triangle</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Area_and_diagonal_bisectors_of_a_parallelogram"><span class="tocnumber">4</span> <span class="toctext">Area and diagonal bisectors of a parallelogram</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Angle_bisector_theorem"><span class="tocnumber">5</span> <span class="toctext">Angle bisector theorem</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#References"><span class="tocnumber">6</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#External_links"><span class="tocnumber">7</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=1" title="Edit section: Line segment bisector"></a>]</span> <span class="mw-headline" id="Line_segment_bisector">Line segment bisector</span></h2>
<div class="thumb tleft">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:Bisectors.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/5/57/Bisectors.svg/220px-Bisectors.svg.png" width="220" height="111" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Bisectors.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
Line DE bisects line AB at D, line EF is a perpendicular bisector of segment AD at C and the interior bisector of right angle AED</div>
</div>
</div>
<p>A <a href="/wiki/Line_segment" title="Line segment">line segment</a> bisector passes through the <a href="/wiki/Midpoint" title="Midpoint">midpoint</a> of the segment. Particularly important is the <a href="/wiki/Perpendicular" title="Perpendicular">perpendicular</a> bisector of a segment, which, according to its name, meets the segment at <a href="/wiki/Right_angle" title="Right angle">right angles</a>. The perpendicular bisector of a segment also has the property that each of its points is <a href="/wiki/Equidistant" title="Equidistant">equidistant</a> from the segment's endpoints. Therefore <a href="/wiki/Voronoi_diagram" title="Voronoi diagram">Voronoi diagram</a> boundaries consist of segments of such lines or planes.</p>
<p>In classical geometry, the bisection is a simple <a href="/wiki/Compass_and_straightedge" title="Compass and straightedge" class="mw-redirect">compass and straightedge</a>, whose possibility depends on the ability to draw <a href="/wiki/Circle" title="Circle">circles</a> of equal radii and different centers. The segment is bisected by drawing intersecting circles of equal radius, whose centers are the endpoints of the segment. The line determined by the points of intersection is the perpendicular bisector, and crosses our original segment at its center. This construction is in fact used when constructing a line perpendicular to a given line at a given point: drawing an arbitrary circle whose center is that point, it intersects the line in two more points, and the perpendicular to be constructed is the one bisecting the segment defined by these two points.</p>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=2" title="Edit section: Angle bisector"></a>]</span> <span class="mw-headline" id="Angle_bisector">Angle bisector</span></h2>
<p>An <a href="/wiki/Angle" title="Angle">angle</a> bisector divides the angle into two angles with <a href="/wiki/Equality_(mathematics)" title="Equality (mathematics)">equal</a> measures. An angle only has one bisector. Each point of an angle bisector is equidistant from the sides of the angle.</p>
<p>The interior bisector of an angle is the <a href="/wiki/Half-line" title="Half-line" class="mw-redirect">half-line</a> or line segment that divides an angle of less than 180 into two equal angles. The exterior bisector is the half-line that divides the opposite angle (of greater than 180) into two equal angles.</p>
<p>To bisect an angle with <a href="/wiki/Straightedge_and_compass" title="Straightedge and compass" class="mw-redirect">straightedge and compass</a>, one draws a circle whose center is the vertex. The circle meets the angle at two points: one on each leg. Using each of these points as a center, draw two circles of the same size. The intersection of the circles (two points) determines a line that is the angle bisector.</p>
<p>The proof of the correctness of these two constructions is fairly intuitive, relying on the symmetry of the problem. It is interesting to note that the <a href="/wiki/Trisecting_the_angle" title="Trisecting the angle" class="mw-redirect">trisection of an angle</a> (dividing it into three equal parts) cannot be achieved with the ruler and compass alone (this was first proved by <a href="/wiki/Pierre_Wantzel" title="Pierre Wantzel">Pierre Wantzel</a>).</p>
<h3><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=3" title="Edit section: Angle bisectors of a triangle"></a>]</span> <span class="mw-headline" id="Angle_bisectors_of_a_triangle">Angle bisectors of a triangle</span></h3>
<p>The angle bisectors of the angles of a <a href="/wiki/Triangle" title="Triangle">triangle</a> are concurrent in a point called the <a href="/wiki/Incenter" title="Incenter" class="mw-redirect">incenter</a> of the triangle.</p>
<p>If the side lengths of a triangle are <img class="tex" alt="a,b,c" src="//upload.wikimedia.org/wikipedia/en/math/a/4/4/a44c56c8177e32d3613988f4dba7962e.png" />, the semiperimeter (<img class="tex" alt="(a+b+c)/2)" src="//upload.wikimedia.org/wikipedia/en/math/d/e/6/de652c217411eff420cdca4e28f2c768.png" />) is <img class="tex" alt="s" src="//upload.wikimedia.org/wikipedia/en/math/0/3/c/03c7c0ace395d80182db07ae2c30f034.png" />, and A is the angle opposite side <img class="tex" alt="a" src="//upload.wikimedia.org/wikipedia/en/math/0/c/c/0cc175b9c0f1b6a831c399e269772661.png" />, the length of the internal bisector of angle A is<sup id="cite_ref-Johnson_0-0" class="reference"><a href="#cite_note-Johnson-0"><span>[</span>1<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt=" \frac{2 \sqrt{bcs(s-a)}}{b+c}" src="//upload.wikimedia.org/wikipedia/en/math/3/a/1/3a1b8993443c428e353df2ec66df6eac.png" />.</dd>
</dl>
<p>If the bisector of angle A in triangle ABC has length <img class="tex" alt="t_a" src="//upload.wikimedia.org/wikipedia/en/math/b/e/e/beeccaef733b5360da45c6c694b6a92f.png" /> and if this bisector divides the side opposite A into segments of lengths <i>m</i> and <i>n</i>, then<sup id="cite_ref-Johnson_0-1" class="reference"><a href="#cite_note-Johnson-0"><span>[</span>1<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt="t_a^2+mn = bc" src="//upload.wikimedia.org/wikipedia/en/math/3/2/4/324b10c28563295ed76314e6f7e7251d.png" /></dd>
</dl>
<p>where <i>b</i> and <i>c</i> are the side lengths opposite vertices B and C; and the side opposite A is divided in the proportion <i>b</i>:<i>c</i>.</p>
<p>If the bisectors of angles A, B, and C have lengths <img class="tex" alt="t_a, t_b," src="//upload.wikimedia.org/wikipedia/en/math/d/f/7/df730625793a2e95c782e4dd83177e95.png" /> and <img class="tex" alt="t_c" src="//upload.wikimedia.org/wikipedia/en/math/9/3/4/934955b1c3b5e1b2a513d22c4fb5d416.png" />, then<sup id="cite_ref-1" class="reference"><a href="#cite_note-1"><span>[</span>2<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt="\frac{(b+c)^2}{bc}t_a^2+ \frac{(c+a)^2}{ca}t_b^2+\frac{(a+b)^2}{ab}t_c^2 = (a+b+c)^2." src="//upload.wikimedia.org/wikipedia/en/math/1/0/6/10645affe2f32749e81e84d1b6aa36b9.png" /></dd>
</dl>
<p>The <a href="/wiki/Angle_bisector_theorem" title="Angle bisector theorem">angle bisector theorem</a> is concerned with the relative lengths of the two segments that a triangle's side is divided into by a line that bisects the opposite angle. It equates their relative lengths to the relative lengths of the other two sides of the triangle.</p>
<h3><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=4" title="Edit section: Angle bisectors of a rhombus"></a>]</span> <span class="mw-headline" id="Angle_bisectors_of_a_rhombus">Angle bisectors of a rhombus</span></h3>
<p>Each diagonal of a <a href="/wiki/Rhombus" title="Rhombus">rhombus</a> bisects opposite angles.</p>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=5" title="Edit section: Area bisectors and area-perimeter bisectors of a triangle"></a>]</span> <span class="mw-headline" id="Area_bisectors_and_area-perimeter_bisectors_of_a_triangle">Area bisectors and area-perimeter bisectors of a triangle</span></h2>
<p>There are an infinitude of lines that bisect the area of a <a href="/wiki/Triangle" title="Triangle">triangle</a>. Three of them are the <a href="/wiki/Median_(geometry)" title="Median (geometry)">medians</a> of the triangle (which connect the sides' midpoints with the opposite vertices), and these are <a href="/wiki/Concurrent_lines" title="Concurrent lines">concurrent</a> at the triangle's <a href="/wiki/Centroid" title="Centroid">centroid</a>; indeed, they are the only area bisectors that go through the centroid. Three other area bisectors are parallel to the triangle's sides; each of these intersects the other two sides so as to divide them into segments with the proportions <img class="tex" alt="\sqrt{2}+1:1" src="//upload.wikimedia.org/wikipedia/en/math/a/8/b/a8b84fc2578ffd2247f119bb1a7ff90b.png" />.<sup id="cite_ref-Dunn_2-0" class="reference"><a href="#cite_note-Dunn-2"><span>[</span>3<span>]</span></a></sup> These six lines are concurrent three at a time: in addition to the three medians being concurrent, any one median is concurrent with two of the side-parallel area bisectors.</p>
<p>The <a href="/wiki/Envelope_(mathematics)" title="Envelope (mathematics)">envelope</a> of the infinitude of area bisectors is a <a href="/wiki/Deltoid_curve" title="Deltoid curve">deltoid</a> (broadly defined as a figure with three vertices connected by curves that are concave to the exterior of the deltoid, making the interior points a non-convex set).<sup id="cite_ref-Dunn_2-1" class="reference"><a href="#cite_note-Dunn-2"><span>[</span>3<span>]</span></a></sup> The vertices of the deltoid are at the midpoints of the medians; all points inside the deltoid are on three different area bisectors, while all points outside it are on just one. <a rel="nofollow" class="external autonumber" href="http://www.btinternet.com/~se16/js/halfarea.htm">[1]</a> The sides of the deltoid are arcs of <a href="/wiki/Hyperbola" title="Hyperbola">hyperbolas</a> that are <a href="/wiki/Asymptote" title="Asymptote">asymptotic</a> to the extended sides of the triangle.<sup id="cite_ref-Dunn_2-2" class="reference"><a href="#cite_note-Dunn-2"><span>[</span>3<span>]</span></a></sup></p>
<p>Any line through a triangle that splits both the triangle's area and its perimeter in half goes through the triangle's incenter (the center of its <a href="/wiki/Incircle" title="Incircle" class="mw-redirect">incircle</a>). There are either one, two, or three of these for any given triangle. A line through the incenter bisects one of the area or perimeter if and only if it also bisects the other.<sup id="cite_ref-3" class="reference"><a href="#cite_note-3"><span>[</span>4<span>]</span></a></sup></p>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=6" title="Edit section: Area and diagonal bisectors of a parallelogram"></a>]</span> <span class="mw-headline" id="Area_and_diagonal_bisectors_of_a_parallelogram">Area and diagonal bisectors of a parallelogram</span></h2>
<p>Any line through the midpoint of a <a href="/wiki/Parallelogram" title="Parallelogram">parallelogram</a> bisects the area.<sup id="cite_ref-4" class="reference"><a href="#cite_note-4"><span>[</span>5<span>]</span></a></sup> Also, the <a href="/wiki/Diagonal" title="Diagonal">diagonals</a> of a parallelogram bisect each other.</p>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=7" title="Edit section: Angle bisector theorem"></a>]</span> <span class="mw-headline" id="Angle_bisector_theorem">Angle bisector theorem</span></h2>
<div class="rellink relarticle mainarticle">Main article: <a href="/wiki/Angle_bisector_theorem" title="Angle bisector theorem">Angle bisector theorem</a></div>
<div class="thumb tright">
<div class="thumbinner" style="width:242px;"><a href="/wiki/File:Triangle_ABC_with_bisector_AD.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Triangle_ABC_with_bisector_AD.svg/240px-Triangle_ABC_with_bisector_AD.svg.png" width="240" height="195" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Triangle_ABC_with_bisector_AD.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
In this diagram, BD:DC = AB:AC.</div>
</div>
</div>
<p>In <a href="/wiki/Geometry" title="Geometry">geometry</a>, the angle bisector theorem is concerned with the relative <a href="/wiki/Length" title="Length">lengths</a> of the two segments that a <a href="/wiki/Triangle" title="Triangle">triangle</a>'s side is divided into by a line that <strong class="selflink">bisects</strong> the opposite angle. It equates their relative lengths to the relative lengths of the other two sides of the triangle.</p>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=8" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ol class="references">
<li id="cite_note-Johnson-0">^ <a href="#cite_ref-Johnson_0-0"><sup><i><b>a</b></i></sup></a> <a href="#cite_ref-Johnson_0-1"><sup><i><b>b</b></i></sup></a> <span class="reference-text">Johnson, Roger A., <i>Advanced Euclidean Geometry</i>, Dover Publ., 2007 (orig. 1929), p. 70.</span></li>
<li id="cite_note-1"><b><a href="#cite_ref-1">^</a></b> <span class="reference-text">Simons, Stuart. <i>Mathematical Gazette</i> 93, March 2009, 115-116.</span></li>
<li id="cite_note-Dunn-2">^ <a href="#cite_ref-Dunn_2-0"><sup><i><b>a</b></i></sup></a> <a href="#cite_ref-Dunn_2-1"><sup><i><b>b</b></i></sup></a> <a href="#cite_ref-Dunn_2-2"><sup><i><b>c</b></i></sup></a> <span class="reference-text">Dunn, J. A., and Pretty, J. E., "Halving a triangle," <i><a href="/wiki/Mathematical_Gazette" title="Mathematical Gazette" class="mw-redirect">Mathematical Gazette</a></i> 56, May 1972, 105-108.</span></li>
<li id="cite_note-3"><b><a href="#cite_ref-3">^</a></b> <span class="reference-text">Kodokostas, Dimitrios, "Triangle Equalizers," <i><a href="/wiki/Mathematics_Magazine" title="Mathematics Magazine">Mathematics Magazine</a></i> 83, April 2010, pp. 141-146.</span></li>
<li id="cite_note-4"><b><a href="#cite_ref-4">^</a></b> <span class="reference-text">Dunn, J. A., and J. E. Pretty, "Halving a triangle", <i>Mathematical Gazette</i> 56, May 1972, p. 105.</span></li>
</ol>
<h2><span class="section">[<a href="/w/index.php?title=Bisection&amp;action=&amp;section=9" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.cut-the-knot.org/triangle/ABisector.shtml">The Angle Bisector</a> at <a href="/wiki/Cut-the-knot" title="Cut-the-knot" class="mw-redirect">cut-the-knot</a></li>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/bisectorangle.html">Angle Bisector definition. Math Open Reference</a> With interactive applet</li>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/bisectorline.html">Line Bisector definition. Math Open Reference</a> With interactive applet</li>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/bisectorperpendicular.html">Perpendicular Line Bisector.</a> With interactive applet</li>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/constbisectangle.html">Animated instructions for bisecting an angle</a> and <a rel="nofollow" class="external text" href="http://www.mathopenref.com/constbisectline.html">bisecting a line</a> Using a compass and straightedge</li>
<li><span class="citation mathworld" id="Reference-Mathworld-Line_Bisector"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/LineBisector.html">Line Bisector</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
</ul>
<p><i>This article incorporates material from <a href="http://planetmath.org/?op=getobj&amp;from=objects&amp;id=3623" class="extiw" title="planetmath:3623">Angle bisector</a> on <a href="/wiki/PlanetMath" title="PlanetMath">PlanetMath</a>, which is licensed under the <a href="/wiki/Wikipedia:CC-BY-SA" title="Wikipedia:CC-BY-SA" class="mw-redirect">Creative Commons Attribution/Share-Alike License</a>.</i></p>


<!-- 
NewPP limit report
Preprocessor node count: 446/1000000
Post-expand include size: 1767/2048000 bytes
Template argument size: 674/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:152547-0!0!0!!en!4!* and timestamp 20120414021657 -->
