<div class="thumb tright">
<div class="thumbinner" style="width:222px;"><a href="/wiki/File:Golden_Angle.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/0/06/Golden_Angle.svg/220px-Golden_Angle.svg.png" width="220" height="221" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Golden_Angle.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The golden angle is the angle subtended by the smaller (red) arc when two arcs that make up a circle are in the <a href="/wiki/Golden_ratio" title="Golden ratio">golden ratio</a></div>
</div>
</div>
<p>In <a href="/wiki/Geometry" title="Geometry">geometry</a>, the <b>golden angle</b> is the smaller of the two <a href="/wiki/Angle" title="Angle">angles</a> created by sectioning the circumference of a circle according to the <a href="/wiki/Golden_section" title="Golden section" class="mw-redirect">golden section</a>; that is, into two <a href="/wiki/Arc_(geometry)" title="Arc (geometry)">arcs</a> such that the ratio of the length of the larger arc to the length of the smaller arc is the same as the ratio of the full circumference to the length of the larger arc.</p>
<p>Algebraically, let <i>a+b</i> be the circumference of a <a href="/wiki/Circle" title="Circle">circle</a>, divided into a longer arc of length <i>a</i> and a smaller arc of length <i>b</i> such that</p>
<dl>
<dd><img class="tex" alt=" \frac{a + b}{a} = \frac{a}{b}. " src="//upload.wikimedia.org/wikipedia/en/math/5/e/2/5e234bb792ef7052cc6e50d6994c7647.png" /></dd>
</dl>
<p>The golden angle is then the angle <a href="/wiki/Subtend" title="Subtend" class="mw-redirect">subtended</a> by the smaller arc of length <i>b</i>. It measures approximately 137.508, or about 2.39996 <a href="/wiki/Radian" title="Radian">radians</a>.</p>
<p>The name comes from the golden angle's connection to the <a href="/wiki/Golden_ratio" title="Golden ratio">golden ratio</a> <i></i>; the exact value of the golden angle is</p>
<dl>
<dd><img class="tex" alt="360\left(1 - \frac{1}{\varphi}\right) = 360(2 - \varphi) = \frac{360}{\varphi^2} = 180(3 - \sqrt{5})\text{ degrees}" src="//upload.wikimedia.org/wikipedia/en/math/1/b/3/1b343ec1838139a948e1a3d7480ab8da.png" /></dd>
</dl>
<p>or</p>
<dl>
<dd><img class="tex" alt=" 2\pi \left( 1 - \frac{1}{\varphi}\right) = 2\pi(2 - \varphi) = \frac{2\pi}{\varphi^2} = \pi(3 - \sqrt{5})\text{ radians}," src="//upload.wikimedia.org/wikipedia/en/math/8/e/2/8e2010cc7a6ecadf275a81f63c88f869.png" /></dd>
</dl>
<p>where the equivalences follow from well-known algebraic properties of the golden ratio.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Derivation"><span class="tocnumber">1</span> <span class="toctext">Derivation</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Golden_angle_in_nature"><span class="tocnumber">2</span> <span class="toctext">Golden angle in nature</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#References"><span class="tocnumber">3</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#See_also"><span class="tocnumber">4</span> <span class="toctext">See also</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Golden_angle&amp;action=&amp;section=1" title="Edit section: Derivation"></a>]</span> <span class="mw-headline" id="Derivation">Derivation</span></h2>
<p>The golden ratio is equal to <i></i>&#160;=&#160;<i>a</i>/<i>b</i> given the conditions above.</p>
<p>Let <i></i> be the fraction of the circumference subtended by the golden angle, or equivalently, the golden angle divided by the angular measurement of the circle.</p>
<dl>
<dd><img class="tex" alt=" f = \frac{b}{a+b} = \frac{1}{1+\varphi}." src="//upload.wikimedia.org/wikipedia/en/math/1/9/f/19fd3ab16d1fa8370698b24020dab2e8.png" /></dd>
</dl>
<p>But since</p>
<dl>
<dd><img class="tex" alt="{1+\varphi} = \varphi^2," src="//upload.wikimedia.org/wikipedia/en/math/8/3/0/83009f737b1bb9c963d09da1e5d1fe70.png" /></dd>
</dl>
<p>it follows that</p>
<dl>
<dd><img class="tex" alt=" f = \frac{1}{\varphi^2} " src="//upload.wikimedia.org/wikipedia/en/math/b/4/f/b4f2a000e7f3a2ab80611b907af018ba.png" /></dd>
</dl>
<p>This is equivalent to saying that <i></i><sup>&#160;2</sup> golden angles can fit in a circle.</p>
<p>The fraction of a circle occupied by the golden angle is therefore:</p>
<dl>
<dd><img class="tex" alt="f \approx 0.381966 \," src="//upload.wikimedia.org/wikipedia/en/math/4/2/f/42f5806c4014b16f9c60a6feb7b2c6fb.png" /></dd>
</dl>
<p>The golden angle <i>g</i> can therefore be numerically approximated in <a href="/wiki/Degree_(angle)" title="Degree (angle)">degrees</a> as:</p>
<dl>
<dd><img class="tex" alt="g \approx 360 \times 0.381966 \approx 137.508^\circ,\," src="//upload.wikimedia.org/wikipedia/en/math/5/b/3/5b3674519322cf346e0486e411fd164e.png" /></dd>
</dl>
<p>or in radians as:</p>
<dl>
<dd><img class="tex" alt=" g \approx 2\pi \times 0.381966 \approx 2.39996. \," src="//upload.wikimedia.org/wikipedia/en/math/d/f/7/df762aa9895f3102f4b2c4a795e3935a.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Golden_angle&amp;action=&amp;section=2" title="Edit section: Golden angle in nature"></a>]</span> <span class="mw-headline" id="Golden_angle_in_nature">Golden angle in nature</span></h2>
<div class="thumb tright">
<div class="thumbinner" style="width:258px;"><a href="/wiki/File:Goldener_Schnitt_Blattstand.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/d/db/Goldener_Schnitt_Blattstand.png" width="256" height="256" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Goldener_Schnitt_Blattstand.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The angle between successive florets in some flowers is the golden angle.</div>
</div>
</div>
<p>The golden angle plays a significant role in the theory of <a href="/wiki/Phyllotaxis" title="Phyllotaxis">phyllotaxis</a>. Perhaps most notably, the golden angle is the angle separating the <a href="/wiki/Floret" title="Floret" class="mw-redirect">florets</a> on a <a href="/wiki/Sunflower" title="Sunflower">sunflower</a>.</p>
<h2><span class="section">[<a href="/w/index.php?title=Golden_angle&amp;action=&amp;section=3" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<div class="refbegin" style="">
<ul>
<li><span class="citation Journal">Vogel, H (1979). "A better way to construct the sunflower head". <i>Mathematical Biosciences</i> <b>44</b> (44): 179189. <a href="/wiki/Digital_object_identifier" title="Digital object identifier">doi</a>:<a rel="nofollow" class="external text" href="http://dx.doi.org/10.1016%2F0025-5564%2879%2990080-4">10.1016/0025-5564(79)90080-4</a>.</span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=A+better+way+to+construct+the+sunflower+head&amp;rft.jtitle=Mathematical+Biosciences&amp;rft.aulast=Vogel&amp;rft.aufirst=H&amp;rft.au=Vogel%2C%26%2332%3BH&amp;rft.date=1979&amp;rft.volume=44&amp;rft.issue=44&amp;rft.pages=179%E2%80%93189&amp;rft_id=info:doi/10.1016%2F0025-5564%2879%2990080-4&amp;rfr_id=info:sid/en.wikipedia.org:Golden_angle"><span style="display: none;">&#160;</span></span></li>
<li><span class="citation book"><a href="/wiki/Przemyslaw_Prusinkiewicz" title="Przemyslaw Prusinkiewicz" class="mw-redirect">Prusinkiewicz, Przemyslaw</a>; <a href="/wiki/Aristid_Lindenmayer" title="Aristid Lindenmayer">Lindenmayer, Aristid</a> (1990). <a rel="nofollow" class="external text" href="http://algorithmicbotany.org/papers/#webdocs"></a><a href="/wiki/The_Algorithmic_Beauty_of_Plants" title="The Algorithmic Beauty of Plants">The Algorithmic Beauty of Plants</a></span>. Springer-Verlag. pp.&#160;101107. <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/978-0387972978" title="Special:BookSources/978-0387972978">978-0387972978</a><span class="printonly">. <a rel="nofollow" class="external free" href="http://algorithmicbotany.org/papers/#webdocs">http://algorithmicbotany.org/papers/#webdocs</a></span>.<span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=%5B%5BThe+Algorithmic+Beauty+of+Plants%5D%5D&amp;rft.aulast=Prusinkiewicz&amp;rft.aufirst=Przemyslaw&amp;rft.au=Prusinkiewicz%2C%26%2332%3BPrzemyslaw&amp;rft.date=1990&amp;rft.pages=pp.%26nbsp%3B101%26ndash%3B107&amp;rft.pub=Springer-Verlag&amp;rft.isbn=978-0387972978&amp;rft_id=http%3A%2F%2Falgorithmicbotany.org%2Fpapers%2F%23webdocs&amp;rfr_id=info:sid/en.wikipedia.org:Golden_angle"><span style="display: none;">&#160;</span></span></li>
</ul>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Golden_angle&amp;action=&amp;section=4" title="Edit section: See also"></a>]</span> <span class="mw-headline" id="See_also">See also</span></h2>
<ul>
<li><a href="/wiki/Fermat%27s_spiral" title="Fermat's spiral">Fermat's spiral</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 1513/1000000
Post-expand include size: 9213/2048000 bytes
Template argument size: 2614/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:315659-0!0!0!!en!4!* and timestamp 20120412191455 -->
