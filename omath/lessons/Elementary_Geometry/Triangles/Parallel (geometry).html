<div class="dablink">"parallel lines" redirects here. For other uses, see <a href="/wiki/Parallel_lines_(disambiguation)" title="Parallel lines (disambiguation)">parallel lines (disambiguation)</a>.</div>
<p><b>Parallelism</b> is a term in <a href="/wiki/Geometry" title="Geometry">geometry</a> and in everyday life that refers to a property in <a href="/wiki/Euclidean_space" title="Euclidean space">Euclidean space</a> of two or more <a href="/wiki/Line_(mathematics)" title="Line (mathematics)" class="mw-redirect">lines</a> or <a href="/wiki/Plane_(mathematics)" title="Plane (mathematics)" class="mw-redirect">planes</a>, or a combination of these. The assumed existence and properties of <b>parallel lines</b> are the basis of <a href="/wiki/Euclid" title="Euclid">Euclid</a>'s <a href="/wiki/Parallel_postulate" title="Parallel postulate">parallel postulate</a>. Two lines in a plane that do not intersect or touch at a point are called parallel lines. Likewise, a line and a plane, or two planes, in three-dimensional Euclidean space that do not share a point are said to be parallel.</p>
<p>In a <a href="/wiki/Non-Euclidean_geometry" title="Non-Euclidean geometry">non-Euclidean space</a>, parallel lines are those that intersect only in the limit at infinity.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Symbol"><span class="tocnumber">1</span> <span class="toctext">Symbol</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Euclidean_parallelism"><span class="tocnumber">2</span> <span class="toctext">Euclidean parallelism</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Construction"><span class="tocnumber">2.1</span> <span class="toctext">Construction</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Distance_between_two_parallel_lines"><span class="tocnumber">2.2</span> <span class="toctext">Distance between two parallel lines</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Extension_to_non-Euclidean_geometry"><span class="tocnumber">3</span> <span class="toctext">Extension to non-Euclidean geometry</span></a>
<ul>
<li class="toclevel-2 tocsection-6"><a href="#Spherical"><span class="tocnumber">3.1</span> <span class="toctext">Spherical</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#Hyperbolic"><span class="tocnumber">3.2</span> <span class="toctext">Hyperbolic</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-8"><a href="#See_Also"><span class="tocnumber">4</span> <span class="toctext">See Also</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#External_links"><span class="tocnumber">5</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=1" title="Edit section: Symbol"></a>]</span> <span class="mw-headline" id="Symbol">Symbol</span></h2>
<p>The parallel symbol is <img class="tex" alt="\parallel" src="//upload.wikimedia.org/wikipedia/en/math/6/d/1/6d1776b6521615edcdd32490a027fd75.png" /> . For example, <img class="tex" alt="AB \parallel CD" src="//upload.wikimedia.org/wikipedia/en/math/f/e/1/fe1121cd0a5d244250f0f1df8c8f47e2.png" /> indicates that line <i>AB</i> is parallel to line <i>CD</i>.</p>
<p>In the <a href="/wiki/Unicode" title="Unicode">Unicode</a> character set, the 'parallel' and 'not parallel' signs have codepoints U+2225 () and U+2226 () respectively.</p>
<h2><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=2" title="Edit section: Euclidean parallelism"></a>]</span> <span class="mw-headline" id="Euclidean_parallelism">Euclidean parallelism</span></h2>
<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Parallel_transversal.svg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/32/Parallel_transversal.svg/300px-Parallel_transversal.svg.png" width="300" height="257" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Parallel_transversal.svg" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
As shown by the tick marks, lines <i>a</i> and <i>b</i> are parallel. This can be proved because the transversal <i>t</i> produces congruent angles.</div>
</div>
</div>
<p>Given straight lines <i>l</i> and <i>m</i>, the following descriptions of line <i>m</i> equivalently define it as parallel to line <i>l</i> in <a href="/wiki/Euclidean_space" title="Euclidean space">Euclidean space</a>:</p>
<ol>
<li>Every point on line <i>m</i> is located at exactly the same minimum distance from line <i>l</i> (<i>equidistant lines</i>).</li>
<li>Line <i>m</i> is on the same plane as line <i>l</i> but does not intersect <i>l</i> (even assuming that lines extend to <a href="/wiki/Infinity" title="Infinity">infinity</a> in either direction).</li>
<li>Lines <i>m</i> and <i>l</i> are both intersected by a third straight line (a <a href="/wiki/Transversal_(geometry)" title="Transversal (geometry)">transversal</a>) in the same plane, and the corresponding angles of intersection with the transversal are equal. (This is equivalent to <a href="/wiki/Euclid" title="Euclid">Euclid</a>'s <a href="/wiki/Parallel_postulate" title="Parallel postulate">parallel postulate</a>.)</li>
</ol>
<p>In other words, parallel lines must be located in the same plane, and parallel planes must be located in the same three-dimensional space. A parallel combination of a line and a plane may be located in the same three-dimensional space. Lines parallel to each other have the same gradient. Compare to <a href="/wiki/Perpendicular" title="Perpendicular">perpendicular</a>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=3" title="Edit section: Construction"></a>]</span> <span class="mw-headline" id="Construction">Construction</span></h3>
<p>The three definitions above lead to three different methods of construction of parallel lines.<br style="clear: left" /></p>
<div class="thumb tleft">
<div class="thumbinner" style="width:252px;"><a href="/wiki/File:Par-prob.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Par-prob.png/250px-Par-prob.png" width="250" height="115" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Par-prob.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
The problem: Draw a line through <i>a</i> parallel to <i>l</i>.</div>
</div>
</div>
<p><br style="clear: left" /></p>
<ul class="gallery">
<li class="gallerybox" style="width: 235px">
<div style="width: 235px">
<div class="thumb" style="width: 230px;">
<div style="margin:29px auto;"><a href="/wiki/File:Par-equi.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/9/95/Par-equi.png/200px-Par-equi.png" width="200" height="92" /></a></div>
</div>
<div class="gallerytext">
<p>Definition 1: Line <i>m</i> has everywhere the same distance to line <i>l</i>.</p>
</div>
</div>
</li>
<li class="gallerybox" style="width: 235px">
<div style="width: 235px">
<div class="thumb" style="width: 230px;">
<div style="margin:23.5px auto;"><a href="/wiki/File:Par-para.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/2/20/Par-para.png/200px-Par-para.png" width="200" height="103" /></a></div>
</div>
<div class="gallerytext">
<p>Definition 2: Take a random line through <i>a</i> that intersects <i>l</i> in <i>x</i>. Move point <i>x</i> to infinity.</p>
</div>
</div>
</li>
<li class="gallerybox" style="width: 235px">
<div style="width: 235px">
<div class="thumb" style="width: 230px;">
<div style="margin:23px auto;"><a href="/wiki/File:Par-perp.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Par-perp.png/200px-Par-perp.png" width="200" height="104" /></a></div>
</div>
<div class="gallerytext">
<p>Definition 3: Both <i>l</i> and <i>m</i> share a transversal line through <i>a</i> that intersect them at 90.</p>
</div>
</div>
</li>
</ul>
<p>Another definition of parallel line that's often used is that two lines are parallel if they do not intersect, though this definition applies only in the 2-dimensional plane. Another easy way is to remember that a parallel line is a line that has an equal distance with the opposite line.</p>
<h3><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=4" title="Edit section: Distance between two parallel lines"></a>]</span> <span class="mw-headline" id="Distance_between_two_parallel_lines">Distance between two parallel lines</span></h3>
<p>Because a parallel line is a line that has an equal distance with the opposite line, there is a unique distance between the two parallel lines. Given the equations of two non-vertical parallel lines</p>
<dl>
<dd><img class="tex" alt="y = mx+b_1\," src="//upload.wikimedia.org/wikipedia/en/math/f/a/e/fae095b3c945270e0185747871022453.png" /></dd>
<dd><img class="tex" alt="y = mx+b_2\,," src="//upload.wikimedia.org/wikipedia/en/math/9/e/6/9e68de13df80642dbd00894eee967f7a.png" /></dd>
</dl>
<p>the distance between the two lines can be found by solving the linear systems</p>
<dl>
<dd><img class="tex" alt="\begin{cases}
y = mx+b_1 \\
y = -x/m
\end{cases}" src="//upload.wikimedia.org/wikipedia/en/math/5/5/b/55b6bf7aecc212c1f265b99d452e4a90.png" /></dd>
</dl>
<p>and</p>
<dl>
<dd><img class="tex" alt="\begin{cases}
y = mx+b_2 \\
y = -x/m
\end{cases}" src="//upload.wikimedia.org/wikipedia/en/math/e/f/e/efe70c2bf7343741302103889208814f.png" /></dd>
</dl>
<p>to get the coordinates of the points. The solutions to the linear systems are the points</p>
<dl>
<dd><img class="tex" alt="\left( x_1,y_1 \right)\ = \left( \frac{-b_1m}{m^2+1},\frac{b_1}{m^2+1} \right)\," src="//upload.wikimedia.org/wikipedia/en/math/0/7/d/07d57b40176db0b80515bba0610f3c31.png" /></dd>
</dl>
<p>and</p>
<dl>
<dd><img class="tex" alt="\left( x_2,y_2 \right)\ = \left( \frac{-b_2m}{m^2+1},\frac{b_2}{m^2+1} \right).\," src="//upload.wikimedia.org/wikipedia/en/math/e/4/0/e4014a145fe4ed7dff2172d13a20200a.png" /></dd>
</dl>
<p>The distance between the points is</p>
<dl>
<dd><img class="tex" alt="d = \sqrt{\left(\frac{b_1m-b_2m}{m^2+1}\right)^2 + \left(\frac{b_2-b_1}{m^2+1}\right)^2}\,," src="//upload.wikimedia.org/wikipedia/en/math/6/c/6/6c6a1dbac3a2e2d9caed03966b848e0d.png" /></dd>
</dl>
<p>which reduces to</p>
<dl>
<dd><img class="tex" alt="d = \frac{|b_2-b_1|}{\sqrt{m^2+1}}\,." src="//upload.wikimedia.org/wikipedia/en/math/8/2/e/82e31b7e7ad9ad174a33e35c4bb1b3a9.png" /></dd>
</dl>
<p>When the lines are given by</p>
<dl>
<dd><img class="tex" alt="ax+by+c_1=0\," src="//upload.wikimedia.org/wikipedia/en/math/3/9/c/39c8028d5798b55bfa138baa161432ac.png" /></dd>
<dd><img class="tex" alt="ax+by+c_2=0,\," src="//upload.wikimedia.org/wikipedia/en/math/a/4/1/a410732819825a84af049ad179ca72b4.png" /></dd>
</dl>
<p>their distance can be expressed as</p>
<dl>
<dd><img class="tex" alt="d = \frac{|c_2-c_1|}{\sqrt {a^2+b^2}}." src="//upload.wikimedia.org/wikipedia/en/math/8/6/7/86777de5102ce3efabbf05e3122d8113.png" /></dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=5" title="Edit section: Extension to non-Euclidean geometry"></a>]</span> <span class="mw-headline" id="Extension_to_non-Euclidean_geometry">Extension to non-Euclidean geometry</span></h2>
<p>In <a href="/wiki/Non-Euclidean_geometry" title="Non-Euclidean geometry">non-Euclidean geometry</a> it is more common to talk about <a href="/wiki/Geodesic" title="Geodesic">geodesics</a> than (straight) lines. A geodesic is the path that a particle follows if no force is applied to it. In non-Euclidean geometry (<a href="/wiki/Elliptic_geometry" title="Elliptic geometry">spherical</a> or <a href="/wiki/Hyperbolic_geometry" title="Hyperbolic geometry">hyperbolic</a>) the three Euclidean definitions are not equivalent: only the second one is useful in other non-Euclidean geometries. In general, <a href="/wiki/Equidistant" title="Equidistant">equidistant</a> lines are not geodesics so the equidistant definition cannot be used. In the Euclidean plane, when two geodesics (straight lines) are intersected with the same angles by a transversal geodesic (see image), every (non-parallel) geodesic intersects them with the same angles. In both the hyperbolic and spherical plane, this is not the case. For example, geodesics sharing a common perpendicular only do so at one point (hyperbolic space) or at two (antipodal) points (spherical space).</p>
<p>In general geometry it is useful to distinguish the three definitions above as three different types of lines, respectively <b>equidistant lines</b>, <b>parallel geodesics</b> and <b>geodesics sharing a common perpendicular</b>.</p>
<p>While in Euclidean geometry two geodesics can either intersect or be parallel, in general and in hyperbolic space in particular there are three possibilities. Two geodesics can be either:</p>
<ol>
<li><b>intersecting</b>: they intersect in a common point in the plane</li>
<li><b>parallel</b>: they do not intersect in the plane, but do in the limit to infinity</li>
<li><b>ultra parallel</b>: they do not even intersect in the limit to infinity</li>
</ol>
<p>In the literature <i>ultra parallel</i> geodesics are often called <i>parallel</i>. <i>Geodesics intersecting at infinity</i> are then called <i>limit geodesics</i>.</p>
<h3><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=6" title="Edit section: Spherical"></a>]</span> <span class="mw-headline" id="Spherical">Spherical</span></h3>
<div class="thumb tright">
<div class="thumbinner" style="width:202px;"><a href="/wiki/File:SphereParallel.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/7/70/SphereParallel.png/200px-SphereParallel.png" width="200" height="202" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:SphereParallel.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
On the <a href="/wiki/Spherical_plane" title="Spherical plane" class="mw-redirect">spherical plane</a> there is no such thing as a parallel line. Line <i>a</i> is a <a href="/wiki/Great_circle" title="Great circle">great circle</a>, the equivalent of a straight line in the spherical plane. Line <i>c</i> is equidistant to line <i>a</i> but is not a great circle. It is a parallel of latitude. Line <i>b</i> is another geodesic which intersects <i>a</i> in two antipodal points. They share two common perpendiculars (one shown in blue).</div>
</div>
</div>
<p>In the <a href="/wiki/Spherical_geometry" title="Spherical geometry">spherical plane</a>, all geodesics are <a href="/wiki/Great_circles" title="Great circles" class="mw-redirect">great circles</a>. Great circles divide the sphere in two equal <a href="/wiki/Sphere" title="Sphere">hemispheres</a> and all great circles intersect each other. By the above definitions, there are no parallel geodesics to a given geodesic, all geodesics intersect. Equidistant lines on the sphere are called <b>parallels of latitude</b> in analog to <a href="/wiki/Latitude" title="Latitude">latitude</a> lines on a globe. Parallel lines in Euclidean space are straight lines; equidistant lines are not geodesics and therefore are not directly analogous to straight lines in the Euclidean space. An object traveling along such a line has to <a href="/wiki/Accelerate" title="Accelerate" class="mw-redirect">accelerate</a> away from the geodesic to which it is equidistant to avoid intersecting with it. When embedded in Euclidean space a <a href="/wiki/Dimension" title="Dimension">dimension</a> higher, parallels of latitude can be generated by the intersection of the sphere with a plane parallel to a plane through the center.</p>
<h3><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=7" title="Edit section: Hyperbolic"></a>]</span> <span class="mw-headline" id="Hyperbolic">Hyperbolic</span></h3>
<p>In the <a href="/wiki/Hyperbolic_geometry" title="Hyperbolic geometry">hyperbolic plane</a>, there are two lines through a given point that intersect a given line in the limit to infinity. While in Euclidean geometry a geodesic intersects its parallels in both directions in the limit to infinity, in hyperbolic geometry both directions have their own line of parallelism. When visualized on a plane a geodesic is said to have a <b>left-handed parallel</b> and a <b>right-handed parallel</b> through a given point. The angle the parallel lines make with the perpendicular from that point to the given line is called the <a href="/wiki/Angle_of_parallelism" title="Angle of parallelism">angle of parallelism</a>. The angle of parallelism depends on the distance of the point to the line with respect to the <a href="/wiki/Curvature" title="Curvature">curvature</a> of the space. The angle is also present in the Euclidean case, there it is always 90 so the left and right-handed parallels <a href="/wiki/Coincident" title="Coincident">coincide</a>. The parallel lines divide the set of geodesics through the point in two sets: <b>intersecting geodesics</b> that intersect the given line in the hyperbolic plane, and <b>ultra parallel geodesics</b> that do not intersect even in the limit to infinity (in either direction). In the Euclidean limit the latter set is empty.</p>
<div class="thumb tleft">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:HyperParallel.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/en/thumb/4/44/HyperParallel.png/300px-HyperParallel.png" width="300" height="155" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:HyperParallel.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
<b>Intersecting</b>, <b>parallel</b> and <b>ultra parallel</b> lines through <i>a</i> with respect to <i>l</i> in the hyperbolic plane. The parallel lines appear to intersect <i>l</i> just off the image. This is an artifact of the visualisation. It is not possible to isometrically embed the hyperbolic plane in three dimensions. In a real hyperbolic space the lines will get closer to each other and 'touch' in infinity.</div>
</div>
</div>
<p><br style="clear:both;" /></p>
<h2><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=8" title="Edit section: See Also"></a>]</span> <span class="mw-headline" id="See_Also">See Also</span></h2>
<ul>
<li><a href="/wiki/Limiting_parallel" title="Limiting parallel">Limiting parallel</a></li>
<li><a href="/wiki/Ultraparallel_theorem" title="Ultraparallel theorem">Ultraparallel theorem</a></li>
<li><a href="/wiki/Clifford_parallel" title="Clifford parallel">Clifford parallel</a></li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Parallel_(geometry)&amp;action=&amp;section=9" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.mathopenref.com/constparallel.html">Constructing a parallel line through a given point with compass and straightedge</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 203/1000000
Post-expand include size: 303/2048000 bytes
Template argument size: 145/2048000 bytes
Expensive parser function count: 1/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:664497-0!0!0!!en!4!* and timestamp 20120325145303 -->
