<div class="thumb tright">
<div class="thumbinner" style="width:302px;"><a href="/wiki/File:Nested_hyperboloids.png" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Nested_hyperboloids.png/300px-Nested_hyperboloids.png" width="300" height="225" class="thumbimage" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/File:Nested_hyperboloids.png" class="internal" title="Enlarge"><img src="//bits.wikimedia.org/skins-1.19/common/images/magnify-clip.png" width="15" height="11" alt="" /></a></div>
A <a href="/wiki/Fiber_bundle" title="Fiber bundle">fibration</a> of <a href="/wiki/Projective_space" title="Projective space">projective space</a> by skew lines on nested <a href="/wiki/Hyperboloid" title="Hyperboloid">hyperboloids</a>.</div>
</div>
</div>
<p>In <a href="/wiki/Solid_geometry" title="Solid geometry">solid geometry</a>, <b>skew lines</b> are two lines that do not intersect and are not <a href="/wiki/Parallel_(geometry)" title="Parallel (geometry)">parallel</a>. Equivalently, they are lines that are not <a href="/wiki/Coplanar" title="Coplanar" class="mw-redirect">coplanar</a>. A simple example of a pair of skew lines is the pair of lines through opposite edges of a <a href="/wiki/Regular_tetrahedron" title="Regular tetrahedron" class="mw-redirect">regular tetrahedron</a>. Lines that are coplanar either intersect or are parallel, so skew lines exist only in three or more <a href="/wiki/Dimension" title="Dimension">dimensions</a>.</p>
<table id="toc" class="toc">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Explanation"><span class="tocnumber">1</span> <span class="toctext">Explanation</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Configurations_of_multiple_skew_lines"><span class="tocnumber">2</span> <span class="toctext">Configurations of multiple skew lines</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Skew_lines_and_ruled_surfaces"><span class="tocnumber">3</span> <span class="toctext">Skew lines and ruled surfaces</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Distance_between_two_skew_lines"><span class="tocnumber">4</span> <span class="toctext">Distance between two skew lines</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Skew_flats_in_higher_dimensions"><span class="tocnumber">5</span> <span class="toctext">Skew flats in higher dimensions</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Notes"><span class="tocnumber">6</span> <span class="toctext">Notes</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#References"><span class="tocnumber">7</span> <span class="toctext">References</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#External_links"><span class="tocnumber">8</span> <span class="toctext">External links</span></a></li>
</ul>
</td>
</tr>
</table>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=1" title="Edit section: Explanation"></a>]</span> <span class="mw-headline" id="Explanation">Explanation</span></h2>
<p>If each line in a pair of skew lines is defined by two <a href="/wiki/Point_(geometry)" title="Point (geometry)">points</a>, then these four points must not be coplanar, so they must be the vertices of a <a href="/wiki/Tetrahedron" title="Tetrahedron">tetrahedron</a> of nonzero <a href="/wiki/Volume" title="Volume">volume</a>; conversely, any two pairs of points defining a tetrahedron of nonzero volume also define a pair of skew lines. Therefore, a test of whether two pairs of points <span class="texhtml">(<b>a</b>,<b>b</b>)</span> and <span class="texhtml">(<b>c</b>,<b>d</b>)</span> define skew lines is to apply the formula for the volume of a tetrahedron, <span class="texhtml"><i>V</i> = |det(<b>a</b><b>b</b>, <b>b</b><b>c</b>, <b>c</b><b>d</b>)|/6</span>, and testing whether the result is nonzero.</p>
<p>If four points are chosen at random within a unit <a href="/wiki/Cube" title="Cube">cube</a>, they will <a href="/wiki/Almost_surely" title="Almost surely">almost surely</a> define a pair of skew lines, because (after the first three points have been chosen) the fourth point will define a non-skew line if, and only if, it is coplanar with the first three points, and the plane through the first three points forms a subset of measure zero of the cube. Similarly, in 3D space a very small perturbation of two parallel or intersecting lines will almost certainly turn them into skew lines. In this sense, skew lines are the "usual" case, and parallel or intersecting lines are special cases.</p>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=2" title="Edit section: Configurations of multiple skew lines"></a>]</span> <span class="mw-headline" id="Configurations_of_multiple_skew_lines">Configurations of multiple skew lines</span></h2>
<p>A <i>configuration</i> of skew lines is a set of lines in which all pairs are skew. Two configurations are said to be <i>isotopic</i> if it is possible to continuously transform one configuration into the other, maintaining throughout the transformation the invariant that all pairs of lines remain skew. Any two configurations of two lines are easily seen to be isotopic, and configurations of the same number of lines in dimensions higher than three are always isotopic, but there exist multiple non-isotopic configurations of three or more lines in three dimensions (<a href="#CITEREFViroViro1990">Viro &amp; Viro 1990</a>). The number of nonisotopic configurations of <i>n</i> lines in <b>R</b><sup>3</sup>, starting at <i>n</i> = 1, is</p>
<dl>
<dd>1, 1, 2, 3, 7, 19, 74, ... (sequence <span class="nowrap"><a href="//oeis.org/A110887" class="extiw" title="oeis:A110887">A110887</a></span> in <a href="/wiki/On-Line_Encyclopedia_of_Integer_Sequences" title="On-Line Encyclopedia of Integer Sequences">OEIS</a>).</dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=3" title="Edit section: Skew lines and ruled surfaces"></a>]</span> <span class="mw-headline" id="Skew_lines_and_ruled_surfaces">Skew lines and ruled surfaces</span></h2>
<p>If one rotates a line L around another line L' skew but not perpendicular to it, the <a href="/wiki/Surface_of_revolution" title="Surface of revolution">surface of revolution</a> swept out by L is a <a href="/wiki/Hyperboloid_of_one_sheet" title="Hyperboloid of one sheet" class="mw-redirect">hyperboloid of one sheet</a>. For instance, the three hyperboloids visible in the illustration can be formed in this way by rotating a line L around the central white vertical line L'. The copies of L within this surface make it a <a href="/wiki/Ruled_surface" title="Ruled surface">ruled surface</a>; it also contains a second family of lines that are also skew to L' at the same distance as L from it but with the opposite angle. An <a href="/wiki/Affine_transformation" title="Affine transformation">affine transformation</a> of this ruled surface produces a surface which in general has an elliptical cross-section rather than the circular cross-section produced by rotating L around L'; such surfaces are also called hyperboloids of one sheet, and again are ruled by two families of mutually skew lines. A third type of ruled surface is the <a href="/wiki/Hyperbolic_paraboloid" title="Hyperbolic paraboloid" class="mw-redirect">hyperbolic paraboloid</a>. Like the hyperboloid of one sheet, the hyperbolic paraboloid has two families of skew lines; in each of the two families the lines are parallel to a common plane although not to each other. Any three skew lines in <b>R</b><sup>3</sup> lie on exactly one ruled surface of one of these types (<a href="#CITEREFHilbertCohn-Vossen1952">Hilbert &amp; Cohn-Vossen 1952</a>).</p>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=4" title="Edit section: Distance between two skew lines"></a>]</span> <span class="mw-headline" id="Distance_between_two_skew_lines">Distance between two skew lines</span></h2>
<p>To calculate the distance between two skew lines the lines are expressed using vectors,</p>
<dl>
<dd><img class="tex" alt=" \mathbf{x} = \mathbf{a} + \lambda \mathbf{b}" src="//upload.wikimedia.org/wikipedia/en/math/7/a/4/7a4bcc48c67328f138664d66f411a2ca.png" /></dd>
<dd><img class="tex" alt=" \mathbf{y} = \mathbf{c} + \mu \mathbf{d}" src="//upload.wikimedia.org/wikipedia/en/math/f/6/d/f6d5d14ca496937656c5320dceafca26.png" />.</dd>
</dl>
<p>The cross product of <b>b</b> and <b>d</b> is perpendicular to the lines, as is the unit vector</p>
<dl>
<dd><img class="tex" alt=" \mathbf{n} = \frac{\mathbf{b} \times \mathbf{d}}{|\mathbf{b} \times \mathbf{d}|} " src="//upload.wikimedia.org/wikipedia/en/math/5/e/c/5ec0d628d2a2fffec91b1b82b2c2f459.png" /></dd>
</dl>
<p>(if |<b>b</b>  <b>d</b>| is zero the lines are parallel and this method cannot be used). The distance between the lines is then<sup id="cite_ref-0" class="reference"><a href="#cite_note-0"><span>[</span>1<span>]</span></a></sup></p>
<dl>
<dd><img class="tex" alt=" d = |\mathbf{n} \cdot (\mathbf{c} - \mathbf{a})|" src="//upload.wikimedia.org/wikipedia/en/math/2/0/0/200bd9cd2767229f72f13ee7aa318161.png" />.</dd>
</dl>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=5" title="Edit section: Skew flats in higher dimensions"></a>]</span> <span class="mw-headline" id="Skew_flats_in_higher_dimensions">Skew flats in higher dimensions</span></h2>
<p>In <i>d</i>-dimensional space, an <a href="/wiki/Flat_(geometry)" title="Flat (geometry)"><i>i</i>-flat</a> and a <i>j</i>-flat may be <b>skew</b> if <span class="texhtml"><i>i</i> + <i>j</i> &lt; <i>d</i></span>. As in the plane, skew flats are those that are neither parallel nor intersect.</p>
<p>In <a href="/wiki/Affine_geometry" title="Affine geometry">affine <i>d</i>-space</a>, two flats of any dimension may be parallel. However, in <a href="/wiki/Projective_geometry" title="Projective geometry">projective space</a>, parallelism does not exist; two flats must either intersect or be skew. Let <span class="texhtml"><i>I</i></span> be the set of points on an <i>i</i>-flat, and let <span class="texhtml"><i>J</i></span> be the set of points on a <i>j</i>-flat. In projective <i>d</i>-space, if <span class="texhtml"><i>i</i> + <i>j</i>  <i>d</i></span> then the intersection of <span class="texhtml"><i>I</i></span> and <span class="texhtml"><i>J</i></span> must contain a (<i>i</i>+<i>j</i><i>d</i>)-flat.</p>
<p>In either geometry, if <span class="texhtml"><i>I</i></span> and <span class="texhtml"><i>J</i></span> intersect at a <i>k</i>-flat, for <span class="texhtml"><i>k</i>  0</span>, then the points of <span class="texhtml"><i>I</i>  <i>J</i></span> determine a (<i>i</i>+<i>j</i><i>k</i>)-flat.</p>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=6" title="Edit section: Notes"></a>]</span> <span class="mw-headline" id="Notes">Notes</span></h2>
<div class="reflist" style="list-style-type: decimal;">
<ol class="references">
<li id="cite_note-0"><b><a href="#cite_ref-0">^</a></b> <span class="reference-text"><span class="citation mathworld" id="Reference-Mathworld-Line-Line_Distance"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/Line-LineDistance.html">Line-Line Distance</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></span></li>
</ol>
</div>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=7" title="Edit section: References"></a>]</span> <span class="mw-headline" id="References">References</span></h2>
<ul>
<li><span class="citation" id="CITEREFHilbertCohn-Vossen1952"><a href="/wiki/David_Hilbert" title="David Hilbert">Hilbert, David</a>; <a href="/wiki/Stephan_Cohn-Vossen" title="Stephan Cohn-Vossen">Cohn-Vossen, Stephan</a> (1952), <i>Geometry and the Imagination</i> (2nd ed.), Chelsea, pp.&#160;1317, <a href="/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a>&#160;<a href="/wiki/Special:BookSources/0-8284-1087-9" title="Special:BookSources/0-8284-1087-9">0-8284-1087-9</a></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Geometry+and+the+Imagination&amp;rft.aulast=Hilbert&amp;rft.aufirst=David&amp;rft.au=Hilbert%2C%26%2332%3BDavid&amp;rft.au=Cohn-Vossen%2C%26%2332%3BStephan&amp;rft.date=1952&amp;rft.pages=pp.%26nbsp%3B13%E2%80%9317&amp;rft.ion=2nd&amp;rft.pub=Chelsea&amp;rft.isbn=0-8284-1087-9&amp;rfr_id=info:sid/en.wikipedia.org:Skew_lines"><span style="display: none;">&#160;</span></span>.</li>
<li><span class="citation" id="CITEREFViroViro1990">Viro, Julia Drobotukhina; Viro, Oleg (1990), <a rel="nofollow" class="external text" href="http://www.math.uu.se/~julia/SplSkrPr.pdf">"Configurations of skew lines"</a> (in Russian), <i>Leningrad Math. J.</i> <b>1</b> (4): 10271050<span class="printonly">, <a rel="nofollow" class="external free" href="http://www.math.uu.se/~julia/SplSkrPr.pdf">http://www.math.uu.se/~julia/SplSkrPr.pdf</a></span></span><span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;rft.atitle=Configurations+of+skew+lines&amp;rft.jtitle=Leningrad+Math.+J.&amp;rft.aulast=Viro&amp;rft.aufirst=Julia+Drobotukhina&amp;rft.au=Viro%2C%26%2332%3BJulia+Drobotukhina&amp;rft.au=Viro%2C%26%2332%3BOleg&amp;rft.date=1990&amp;rft.volume=1&amp;rft.issue=4&amp;rft.pages=1027%E2%80%931050&amp;rft_id=http%3A%2F%2Fwww.math.uu.se%2F%7Ejulia%2FSplSkrPr.pdf&amp;rfr_id=info:sid/en.wikipedia.org:Skew_lines"><span style="display: none;">&#160;</span></span>. Revised version in English: <a href="/wiki/ArXiv" title="ArXiv">arXiv</a>:<a href="http://arxiv.org/abs/math.GT/0611374" class="extiw" title="arxiv:math.GT/0611374">math.GT/0611374</a>.</li>
</ul>
<h2><span class="section">[<a href="/w/index.php?title=Skew_lines&amp;action=&amp;section=8" title="Edit section: External links"></a>]</span> <span class="mw-headline" id="External_links">External links</span></h2>
<ul>
<li><span class="citation mathworld" id="Reference-Mathworld-Skew_Lines"><a href="/wiki/Eric_W._Weisstein" title="Eric W. Weisstein">Weisstein, Eric W.</a>, "<a rel="nofollow" class="external text" href="http://mathworld.wolfram.com/SkewLines.html">Skew Lines</a>" from <a href="/wiki/MathWorld" title="MathWorld">MathWorld</a>.</span></li>
<li><a rel="nofollow" class="external text" href="http://www.netcomuk.co.uk/~jenolive/skew.html">Finding the shortest distance between two skew lines</a></li>
</ul>


<!-- 
NewPP limit report
Preprocessor node count: 1993/1000000
Post-expand include size: 12422/2048000 bytes
Template argument size: 2569/2048000 bytes
Expensive parser function count: 0/500
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:1975821-0!0!0!!en!4!* and timestamp 20120322140236 -->
